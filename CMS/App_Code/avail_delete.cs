﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.Scheduler;
using CMS.EventLog;
using CMS.DataEngine;
using CMS.CustomTables;
using CMS;
using CMS.Helpers;


/// <summary>
/// Summary description for avail_delete
/// </summary>
/// 

[assembly: RegisterCustomClass("Custom.availDelete", typeof(MyTask.AvailDelete))]
namespace MyTask
{
    public class AvailDelete : ITask
    {

        public string Execute(TaskInfo task)
        {
            // Prepares the code name (class name) of the custom table
            string customTableClassName = "customtable.Availability";
            // Gets the custom table
            DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
            if (customTable != null)
            {
                // Gets the first custom table record whose value in the 'UserID' field is equal to LoggedIn User UserID
                var availableUser = CustomTableItemProvider.GetItems(customTableClassName);

                foreach (CustomTableItem item in availableUser)
                {
                    DateTime rowCreated = item.GetDateTimeValue("ItemCreatedWhen", DateTime.Now);

                    if (rowCreated < DateTime.Now.AddDays(-3))
                    {
                       item.Delete(); // delete data records
                    }

                }

            }
            return null;
        }
    }

}