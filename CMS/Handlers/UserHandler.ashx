﻿<%@ WebHandler Language="C#" Class="UserHandler" %>

using System;
using CMS.DataEngine;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using CustomUserInfo;
using CMS.CustomTables;

namespace CustomUserInfo {
    public class UserData
    {
        public string JobPosition { get; set; }
        public string Venue { get; set; }
    }
}
public class UserHandler : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        string JsonResponse = string.Empty;
        var JsSerialize = new JavaScriptSerializer();
        List<UserData> lstUsers = new List<UserData>();

        string sUserId = string.Empty;
        if (context.Request.QueryString["userid"] != null)
        {
            sUserId = context.Request.QueryString["userid"];

        }

        if (sUserId != string.Empty)
        {
            string jobExperienceCustomTable = "customtable.MemberDirJobExp";
            DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(jobExperienceCustomTable);
            if (customTable != null)
            {
                // gonna get the rw from custom
                var useritems = CustomTableItemProvider.GetItems(jobExperienceCustomTable).WhereEquals("UserID", sUserId);

                foreach (var useritem in useritems)
                {
                        string JobPosition = useritem.GetValue("JobPosition").ToString();
                    string Venue = useritem.GetValue("Venue").ToString();
                    lstUsers.Add(new UserData { JobPosition = JobPosition, Venue = Venue });
                }

                

            }
        }

        JsonResponse = JsSerialize.Serialize(lstUsers);
        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonResponse);
        context.Response.End();

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}
