<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_Membership_Registration_RegistrationForm" CodeFile="~/CMSWebParts/Membership/Registration/RegistrationForm.ascx.cs" %>
<%@ Register Src="~/CMSFormControls/Captcha/SecurityCode.ascx" TagName="SecurityCode" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Membership/FormControls/Passwords/PasswordStrength.ascx" TagName="PasswordStrength" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Inputs/EmailInput.ascx" TagName="EmailInput" TagPrefix="cms" %>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(document).ready(function () {
        $(".start-datepicker").datepicker();
        $('.end-datepicker').datepicker();
    });
</script>

<style>
    .form-horizontal .form-group {
        margin: 0;
    }

    .form-control {
        width: auto;
    }

    .registration {
        background-color: #fff;
    }

    .inline {
        display: inline-block;
        vertical-align: top;
    }

    .modal input[type=checkbox], .modal input[type=radio] {
        height: auto;
    }

    a {
        color: #337ab7;
    }

        a:hover {
            color: #337ab7;
            text-decoration: underline;
        }

    input[type=text], input[type=password] {
        border: 1px solid #000;
    }

    .item {
        display: inline-block;
        border: 1px solid #000;
        padding: 15px;
    }

    input[type=image] {
        height: auto;
        padding: 0;
        position: absolute;
        right: -12px;
        top: -12px;
    }

    .delete-btn-container {
        position: relative;
    }

    .form-section {
        padding: 20px 0;
    }

    .tab {
        width: 200px;
        background-color: #fff;
        color: #204d74;
    }

    .screen1 {
        border-radius: 50px 0px 0px 50px;
        border-right: none;
    }

    .screen2 {
        border-radius: 0;
    }

    .screen3 {
        border-radius: 0 50px 50px 0;
        border-left: none;
    }

    .nav-bar {
        padding-top: 20px;
        font-size: 0;
    }

    .active-tab {
        background-color: #204d74;
        color: #fff;
    }

    .error-labels {
        padding-bottom: 20px;
    }

    .job-modal-error, .info-modal-error {
        color: red;
    }
</style>
<!-- Job Modal -->
<div class="modal fade" id="jobSkillsModal" tabindex="-1" role="dialog" aria-labelledby="jobSkillsModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="jobSkillsModalLabel">Job Skills</h4>
            </div>
            <div class="modal-body">
                <div>
                    <asp:CheckBoxList ID="ccblSkills" runat="server" DataTextField="Text" DataValueField="Value"></asp:CheckBoxList>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="cbSaveJobSkills" runat="server" Text="Save" OnClick="cbSaveJobSkills_Click" CssClass="save-skill-btn" />
                <a href="#a" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>


<!-- Licenses Modal -->
<div class="modal fade" id="licensesModal" tabindex="-1" role="dialog" aria-labelledby="licensesModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="licensesModalLabel">Licenses & Certifications</h4>
            </div>
            <div class="modal-body">
                <div>
                    <asp:CheckBoxList ID="ccblLicenses" runat="server" DataTextField="Text" DataValueField="Value">
                    </asp:CheckBoxList>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="cbSaveLicenses" runat="server" Text="Save" OnClick="cbSaveLicenses_Click" CssClass="save-license-btn" />
                <a href="#a" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>


<!-- Job Experience Modal -->
<div class="modal fade" id="jobExpModal" tabindex="-1" role="dialog" aria-labelledby="jobExpModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <asp:UpdatePanel ID="cupJobExpModal" runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="jobExpModalLabel">Job Experience</h4>
                        <asp:HiddenField ID="chfJobExp" runat="server" />
                    </div>
                    <div class="modal-body">
                        <div>
                            <label>Venue Type: *</label><br />
                            <asp:RadioButtonList ID="crblVenueTypes" CssClass="rbl-venue-types" runat="server" DataTextField="Text" DataValueField="Value" RepeatDirection="Horizontal">
                            </asp:RadioButtonList>
                            <asp:Label ID="clVenueTypes" runat="server" CssClass="field-error"></asp:Label>
                        </div>
                        <div>
                            <div>
                                <label>Job Position: *</label>
                            </div>
                            <asp:Panel ID="cPanelJobPositions1" runat="server">
                                <div class="inline">
                                    <asp:DropDownList ID="cddlJobPositions" runat="server" CssClass="position-dropdown" DataTextField="Text" DataValueField="Value">
                                    </asp:DropDownList>
                                    <asp:Label ID="clJobPosition" runat="server" CssClass="field-error"></asp:Label>
                                </div>
                                <div class="inline">
                                    <div class="small">
                                        Don't See Your Position?
                                    </div>
                                    <div>
                                        <asp:LinkButton ID="clbAddJobPosition" runat="server" CssClass="add-job-position" OnClick="clbAddJobPosition_Click" Text="Add A New Position"></asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="cPanelJobPositions2" runat="server" Visible="false">
                                <div class="inline">
                                    <asp:TextBox ID="ctbAddJobPosition" runat="server"></asp:TextBox>
                                </div>
                                <div class="inline">
                                    <asp:Button ID="cbAddCustomPosition" runat="server" Text="Save" OnClick="cbAddCustomPosition_Click" />
                                    <asp:Button ID="cbCancelCustomPosition" runat="server" Text="Cancel" OnClick="cbCancelCustomPosition_Click" />
                                </div>
                            </asp:Panel>
                        </div>
                        <div>
                            <div>
                                <label>Venue: *</label>
                                <asp:Panel ID="cPanelVenue1" runat="server">
                                    <div class="inline">
                                        <asp:DropDownList ID="cddlJobVenues" runat="server" CssClass="venue-dropdown" DataTextField="Text" DataValueField="Value">
                                        </asp:DropDownList>
                                        <asp:Label ID="clVenue" runat="server" CssClass="field-error"></asp:Label>
                                    </div>
                                    <div class="inline">
                                        <div class="small">
                                            Don't See Your Venue?
                                        </div>
                                        <div>
                                            <asp:LinkButton ID="clbAddJobVenue" runat="server" CssClass="add-venue" OnClick="clbAddJobVenue_Click" Text="Add A New Venue"></asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="cPanelVenue2" runat="server" Visible="false">
                                    <div class="inline">
                                        <asp:TextBox ID="ctbAddJobVenue" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="inline">
                                        <asp:Button ID="cbAddCustomVenue" runat="server" Text="Save" OnClick="cbAddCustomVenue_Click" />
                                        <asp:Button ID="cbCancelCustomVenue" runat="server" Text="Cancel" OnClick="cbCancelCustomVenue_Click" />
                                    </div>
                                </asp:Panel>
                            </div>                            
                        </div>
                        <div>
                            <div class="inline">
                                <label>Start Date: *</label><br />
                                <asp:TextBox ID="ctbJobStartDate" runat="server" CssClass="start-datepicker"></asp:TextBox>
                                <asp:Label ID="clJobStateDate" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>End Date:</label><br />
                                <div>
                                    <asp:TextBox ID="ctbJobEndDate" runat="server" CssClass="end-datepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="inline">
                                <asp:CheckBox ID="ccbCurrentPosition" runat="server" CssClass="current-position" Text="Current Position" />
                            </div>
                        </div>
                        <div>
                            <div>
                                Job Description:
                            </div>
                            <div>
                                <asp:TextBox ID="ctbJobDescription" runat="server" TextMode="MultiLine" Height="140"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <asp:Button ID="cbSaveJob" runat="server" Text="Save Job" OnClick="cbSaveJob_Click" CssClass="save-job-btn" />
                        <a href="#a" data-dismiss="modal">Cancel</a>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="crepJobsInitial" EventName="ItemCommand" />
                    <asp:AsyncPostBackTrigger ControlID="clbAddJobInitial" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="clbAddJobFinal" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

<!-- Personal Info Modal -->
<div class="modal fade" id="editInfoModal" tabindex="-1" role="dialog" aria-labelledby="editInfoModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <asp:UpdatePanel ID="cupEditInfo" runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="editInfoModalLabel">Edit Personal Information</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label>Name: *</label><br />
                            <div class="inline">
                                <asp:TextBox ID="ctbFirstNameModal" runat="server" CssClass="first-name-modal"></asp:TextBox><br />
                                <label>First Name</label>
                                <asp:Label ID="clFirstNameModal" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <asp:TextBox ID="ctbLastNameModal" runat="server" CssClass="last-name-modal"></asp:TextBox><br />
                                <label>Last Name</label>
                                <asp:Label ID="clLastNameModal" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                        </div>
                        <div>
                            <label>Union Card Number: *</label><br />
                            <asp:TextBox ID="ctbCardNumberModal" runat="server" CssClass="card-number-modal"></asp:TextBox>
                            <asp:Label ID="Label1" runat="server" CssClass="field-error"></asp:Label>
                        </div>
                        <div>
                            <label>Street Address: *</label><br />
                            <asp:TextBox ID="ctbAddressModal" runat="server" CssClass="address-modal"></asp:TextBox>
                            <asp:Label ID="clAddressModal" runat="server" CssClass="field-error"></asp:Label>
                        </div>
                        <div>
                            <div class="inline">
                                <label>City: *</label><br />
                                <asp:TextBox ID="ctbCityModal" runat="server" CssClass="city-modal"></asp:TextBox>
                                <asp:Label ID="clCityModal" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>State: *</label><br />
                                <asp:TextBox ID="ctbStateModal" runat="server" CssClass="state-modal"></asp:TextBox>
                                <asp:Label ID="clStateModal" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>ZIP Code: *</label><br />
                                <asp:TextBox ID="ctbZipCodeModal" runat="server" CssClass="zip-modal"></asp:TextBox>
                                <asp:Label ID="clZipCodeModal" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                        </div>
                        <div>
                            <div class="inline">
                                <label>Cell Number: *</label><br />
                                <asp:TextBox ID="ctbCellNumberModal" runat="server" CssClass="cell-modal"></asp:TextBox>
                                <asp:Label ID="clCellNumberModal" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>Home Number: optional</label><br />
                                <asp:TextBox ID="ctbHomeNumberModal" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="cbSaveInfoModal" runat="server" Text="Save" OnClick="cbSaveInfoModal_Click" CssClass="save-info-btn" />
                        <a href="#a" data-dismiss="modal">Cancel</a>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="cbSaveProfile" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="cbScreen3" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

<asp:UpdatePanel ID="cupRegistration" runat="server">
    <ContentTemplate>
        <asp:Panel ID="cPanelButtons" runat="server" Visible="false">
            <div class="nav-bar">
                <cms:CMSButton ID="cbScreen1" runat="server" Text="Create Password" OnClientClick="return false;" CssClass="screen1 tab" />
                <cms:CMSButton ID="cbScreen2" runat="server" Text="Create Profile" OnClientClick="return false;" CssClass="screen2 tab" />
                <cms:CMSButton ID="cbScreen3" runat="server" Text="Confirm Your Information" OnClientClick="return false;" CssClass="screen3 tab" />
            </div>
        </asp:Panel>


        <asp:Panel ID="cPanelRegister" runat="server" Visible="true">
            <div id="register">
                <p>Please enter your email and Union Card Number. After successfully logging in, you will proceed to registration.</p>
                <div class="form">
                    <div class="form-section">
                        <div class="inline">
                            <%--<asp:Label CssClass="control-label" ID="lblEmail" runat="server" AssociatedControlID="ctbEmail" EnableViewState="false" />--%>
                            <label>Email: *</label>
                            <cms:EmailInput ID="ctbEmail" runat="server" CssClass="ctbEmail" />
                            <p class="bottom-label">Your email address will be your username.</p>
                            <asp:Label ID="clEmail1Error" runat="server" CssClass="field-error" Visible="true"></asp:Label>
                            <%--<cms:CMSRequiredFieldValidator ID="rfvEmail" runat="server" CssClass="garbage" ControlToValidate="ctbEmail" Display="Dynamic" EnableViewState="false" ValidationGroup="email" />--%>
                        </div>
                        <div class="inline">
                            <label>Confirm Email Address: *</label>
                            <cms:EmailInput ID="ctbEmail2" runat="server" CssClass="ctbEmail2" />
                            <asp:Label ID="clEmail2Error" runat="server" CssClass="field-error" Visible="true"></asp:Label>
                            <%--<cms:CMSRequiredFieldValidator ID="rfvEmail2" runat="server" ControlToValidate="ctbEmail2" Display="Dynamic" EnableViewState="false" ValidationGroup="email" />--%>
                        </div>
                        <div>
                            <label>Union Card Number: *</label><br />
                            <asp:TextBox ID="ctbCardNumber" runat="server" MaxLength="100" CssClass="ctbCardNumber" /><br />
                            <asp:Label ID="clCardNumberError" runat="server" CssClass="field-error" Visible="true"></asp:Label>
                            <%--<asp:RequiredFieldValidator ID="rfvCardNumber" runat="server" ValidationGroup="email" ControlToValidate="ctbCardNumber" CssClass="garbage"></asp:RequiredFieldValidator>--%>
                            <%--<cms:CMSRequiredFieldValidator ID="rfvCardNumber" runat="server" ControlToValidate="ctbCardNumber" Display="Dynamic" EnableViewState="false" />--%>
                        </div>
                        <div>
                            <cms:CMSButton ID="cbRegister" runat="server" CssClass="register-btn" Text="Login & Proceed to Registration" OnClick="cbRegister_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="cPanelCreatePassword" runat="server" Visible="false">
            <div id="create-password">
                <div class="form">
                    <div class="form-section">
                        <h3>Create your password</h3>
                        <p>Please create a password to begin creating your account.</p>
                    </div>
                    <div class="form-section">
                        <div class="inline">
                            <label>Password: *</label><br />
                            <asp:TextBox ID="ctbPassword" runat="server" TextMode="Password" CssClass="ctbPassword"></asp:TextBox>
                            <asp:Label ID="clPasswordError" runat="server" CssClass="field-error" Visible="true"></asp:Label>
                        </div>
                        <div class="inline">
                            <label>Confirm Password: *</label><br />
                            <asp:TextBox ID="ctbConfirmPassword" runat="server" TextMode="Password" CssClass="ctbPassword2"></asp:TextBox>
                            <asp:Label ID="clPassword2" runat="server" CssClass="field-error" Visible="true"></asp:Label>
                        </div>
                        <div>
                            <cms:CMSButton ID="cbCreatePassword" runat="server" CssClass="create-password-btn" Text="Save & Continue" OnClick="cbCreatePassword_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="cPanelProfile" runat="server" Visible="false">
            <div id="create-profile">
                <div class="form">
                    <div class="form-section">
                        <h3>Create your profile</h3>
                        <p>Please enter your personal information about job skills, licenses, certifications or jobs.</p>
                    </div>
                    <div class="form-section">
                        <h3>Personal Information</h3>
                        <div>
                            <label>Name: *</label><br />
                            <div class="inline">
                                <cms:CMSTextBox ID="ctbFirstName" EnableEncoding="true" runat="server" MaxLength="100" CssClass="ctbFirstName" />
                                <label>First</label>
                                <asp:Label ID="clFirstName" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <cms:CMSTextBox ID="ctbLastName" EnableEncoding="true" runat="server" MaxLength="100" CssClass="ctbLastName" />
                                <label>Last</label>
                                <asp:Label ID="clLastName" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                        </div>
                        <div>
                            <label>Street Address: *</label><br />
                            <cms:CMSTextBox ID="ctbAddress" runat="server" CssClass="ctbAddress"></cms:CMSTextBox>
                            <asp:Label ID="clAddress" runat="server" CssClass="field-error"></asp:Label>
                        </div>
                        <div>
                            <div class="inline">
                                <label>City: *</label><br />
                                <cms:CMSTextBox ID="ctbCity" runat="server" CssClass="ctbCity"></cms:CMSTextBox>
                                <asp:Label ID="clCity" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>State: *</label><br />
                                <cms:CMSTextBox ID="ctbState" runat="server" CssClass="ctbState"></cms:CMSTextBox>
                                <asp:Label ID="clState" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>Zip Code: *</label><br />
                                <cms:CMSTextBox ID="ctbZipCode" runat="server" CssClass="ctbZipCode"></cms:CMSTextBox>
                                <asp:Label ID="clZipCode" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                        </div>
                        <div>
                            <div class="inline">
                                <label>Cell Number: *</label><br />
                                <cms:CMSTextBox ID="ctbCellNumber" runat="server" CssClass="ctbCellNumber"></cms:CMSTextBox>
                                <asp:Label ID="clCellNumber" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>Home Number:</label>
                                <br />
                                <cms:CMSTextBox ID="ctbHomeNumber" runat="server"></cms:CMSTextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-section">
                        <h3>Job Skills:</h3>
                        <asp:Repeater ID="crepJobSkillsInitial" runat="server">
                            <ItemTemplate>
                                <p><%# Eval("Text") %></p>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:LinkButton ID="clbAddSkillsInitial" runat="server" Text="Add Job Skills" CssClass="add-skills-btn"></asp:LinkButton>
                    </div>
                    <div class="form-section">
                        <h3>Licenses & Certifications:</h3>
                        <asp:Repeater ID="crepLicensesInitial" runat="server">
                            <ItemTemplate>
                                <p><%# Eval("Text") %></p>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:LinkButton ID="clbAddLicensesInitial" runat="server" Text="Add Licenses & Certifications" CssClass="add-licenses-btn"></asp:LinkButton>
                    </div>
                    <div class="form-section">
                        <h3>Job Experience:</h3>
                        <asp:Repeater ID="crepJobsInitial" runat="server" OnItemCommand="crepJobsInitial_ItemCommand">
                            <HeaderTemplate>
                                <div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="item">
                                    <div class="delete-btn-container">
                                        <asp:ImageButton ID="cibDeleteJobInitial" runat="server" ImageUrl="~/EcommerceSite/images/fancy_close.png" CommandName="delete" CommandArgument='<%# Container.ItemIndex %>' OnClientClick="return confirm('Are you sure you want to delete this job?');" />
                                    </div>
                                    <p><%# Eval("Venue") %></p>
                                    <p><%# Eval("VenueType") %></p>
                                    <p><%# Eval("Position") %></p>
                                    <p><%# Eval("StartDate") %> - <%# Eval("Status")%></p>
                                    <asp:LinkButton ID="clbEditJobInitial" runat="server" CommandName="edit" CommandArgument='<%# Container.ItemIndex %>'>Edit</asp:LinkButton>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <%--<input type="button" id="add-job-btn" value="+ Add a Job" class="btn" data-toggle="modal" data-target="#jobExpModal" />--%>
                        <asp:LinkButton ID="clbAddJobInitial" runat="server" Text="Add a Job" CssClass="add-job-btn" OnClick="clbAddJobInitial_Click"></asp:LinkButton>
                    </div>
                    <div>
                        <cms:CMSButton ID="cbSaveProfile" runat="server" Text="Save & Continue" OnClick="cbSaveProfile_Click" CssClass="save-profile-btn" />
                    </div>
                </div>
            </div>

        </asp:Panel>
        <asp:Panel ID="cPanelConfirmInfo" runat="server" Visible="false">
            <div id="confirm-info">
                <div class="form">
                    <div class="form-section">
                        <h3>Confirm Your Information</h3>
                        <p>Before we create your account, please confirm the information you have entered.</p>
                    </div>
                    <div class="form-section">
                        <div class="inline">
                            <h3>
                                <asp:Label ID="clName" runat="server"></asp:Label>
                            </h3>
                        </div>
                        <div class="inline">
                            <asp:LinkButton ID="clbEditInfoFinal" runat="server" Text="edit" OnClick="clbEditInfoFinal_Click" CssClass="edit-info-btn"></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="clCardNumber" runat="server"></asp:Label>
                            &nbsp;&nbsp; <b>Union Card Number</b><br />
                            <asp:Label ID="clStreetAddress" runat="server"></asp:Label><br />
                            <asp:Label ID="clCityStateZip" runat="server"></asp:Label><br />
                            <asp:Label ID="clCellPhone" runat="server"></asp:Label>&nbsp;&nbsp;<b>Cell</b><br />
                            <asp:Label ID="clHomePhone" runat="server"></asp:Label>&nbsp;&nbsp;<b>Home</b><br />
                            <asp:Label ID="clEmail" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form-section">
                        <div class="inline">
                            <h3>Job Skills</h3>
                        </div>
                        <div class="inline">
                            <asp:LinkButton ID="clbAddSkillsFinal" runat="server" Text="edit" CssClass="add-skills-btn"></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Repeater ID="crepJobSkillsFinal" runat="server">
                                <ItemTemplate>
                                    <p><%# Eval("Text") %></p>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="form-section">
                        <div class="inline">
                            <h3>Licenses & Certification</h3>
                        </div>
                        <div class="inline">
                            <asp:LinkButton ID="clbAddLicensesFinal" runat="server" Text="edit" CssClass="add-licenses-btn"></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Repeater ID="crepLicensesFinal" runat="server">
                                <ItemTemplate>
                                    <p><%# Eval("Text") %></p>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="form-section">
                        <h3>Job Experience</h3>
                        <asp:Repeater ID="crepJobsFinal" runat="server" OnItemCommand="crepJobsInitial_ItemCommand">
                            <HeaderTemplate>
                                <div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="item">
                                    <div class="delete-btn-container">
                                        <asp:ImageButton ID="cibDeleteJobFinal" runat="server" ImageUrl="~/EcommerceSite/images/fancy_close.png" CommandName="delete" CommandArgument='<%# Container.ItemIndex %>' OnClientClick="return confirm('Are you sure you want to delete this job?');" />
                                    </div>
                                    <p><%# Eval("Venue") %></p>
                                    <p><%# Eval("VenueType") %></p>
                                    <p><%# Eval("Position") %></p>
                                    <p><%# Eval("StartDate") %> - <%# Eval("Status")%></p>
                                    <asp:LinkButton ID="clbEditJobFinal" runat="server" CommandName="edit" CommandArgument='<%# Container.ItemIndex %>'>Edit</asp:LinkButton>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div>
                            <%--<cms:CMSButton ID="cbAddAnotherJob" runat="server" Text="+ Add Another Job" OnClick="cbAddAnotherJob_Click" />--%>
                            <asp:LinkButton ID="clbAddJobFinal" runat="server" Text="Add a Job" CssClass="add-job-btn" OnClick="clbAddJobFinal_Click"></asp:LinkButton>
                            <%--<input type="button" id="add-another-job-btn" value="+ Add Another Job" class="btn" data-toggle="modal" data-target="#jobExpModal" />--%>
                        </div>
                        <div>
                            <cms:CMSButton ID="cbCreateAccount" runat="server" Text="Save & Create Account" OnClick="cbCreateAccount_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="error-labels">
            <asp:Label ID="lblError" runat="server" ForeColor="red" EnableViewState="false" />
            <asp:Label ID="lblText" runat="server" Visible="false" EnableViewState="false" />
        </div>
        <asp:PlaceHolder runat="server" ID="plcMFIsRequired" Visible="false">
            <div class="form-group">
                <div class="editing-form-value-cell editing-form-value-cell-offset">
                    <cms:CMSCheckBox ID="chkUseMultiFactorAutentization" runat="server" ResourceString="webparts_membership_registrationform.mfrequired" />
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcCaptcha">
            <div class="form-group">
                <div class="editing-form-label-cell">
                    <asp:Label CssClass="control-label" ID="lblCaptcha" runat="server" AssociatedControlID="scCaptcha" EnableViewState="false" />
                </div>
                <div class="editing-form-value-cell">
                    <cms:SecurityCode ID="scCaptcha" GenerateNumberEveryTime="false" ShowInfoLabel="False" runat="server" />
                </div>
            </div>
        </asp:PlaceHolder>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="cbSaveJob" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbSaveLicenses" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbSaveJobSkills" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbSaveInfoModal" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbScreen1" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbScreen2" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbScreen3" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>

