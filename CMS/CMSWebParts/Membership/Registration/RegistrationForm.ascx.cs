﻿using System;
using System.Web;

using CMS.Activities.Loggers;
using CMS.Base;
using CMS.Base.Web.UI;
using CMS.Core;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.EmailEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Localization;
using CMS.MacroEngine;
using CMS.Membership;
using CMS.PortalEngine;
using CMS.PortalEngine.Web.UI;
using CMS.Protection;
using CMS.SiteProvider;
using CMS.WebAnalytics;
using System.Web.UI;
using System.Collections.Generic;
using CMS.CustomTables;
using System.Web.UI.WebControls;
using System.Linq;
using System.Text;
using System.Web.Services;

public partial class CMSWebParts_Membership_Registration_RegistrationForm : CMSAbstractWebPart
{
    [Serializable]
    public class JobExperience
    {
        public int ID { get; set; }
        public string VenueType { get; set; }
        public string VenueTypeID { get; set; }
        public string Position { get; set; }
        public string Venue { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool CurrentPosition { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }
    [Serializable]
    public class JobValuePair
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    protected string TypedPassword
    {
        get
        {
            if (ViewState["TypedPassword"] != null)
            {
                return Convert.ToString(ViewState["TypedPassword"]);
            }
            return null;
        }
        set
        {
            ViewState["TypedPassword"] = value;
        }
    }

    #region "Text properties and constants"

    private const int EMAIL_MAX_LENGTH = 100;

    //public RegisteredUser RegUser = new RegisteredUser();


    /// <summary>
    /// Gets or sets the Skin ID.
    /// </summary>
    public override string SkinID
    {
        get
        {
            return base.SkinID;
        }
        set
        {
            base.SkinID = value;
            SetSkinID(value);
        }
    }


    /// <summary>
    /// Gets or sets the first name text.
    /// </summary>
    public string FirstNameText
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("FirstNameText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.FirstName$}"));
        }
        set
        {
            SetValue("FirstNameText", value);
            //lblFirstName.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the last name text.
    /// </summary>
    public string LastNameText
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("LastNameText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.LastName$}"));
        }
        set
        {
            SetValue("LastNameText", value);
            //lblLastName.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the e-mail text.
    /// </summary>
    public string EmailText
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("EmailText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Email$}"));
        }
        set
        {
            SetValue("EmailText", value);
            //lblEmail.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the password text.
    /// </summary>
    public string PasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("PasswordText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Password$}"));
        }
        set
        {
            SetValue("PasswordText", value);
            //lblPassword.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the confirmation password text.
    /// </summary>
    public string ConfirmPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("ConfirmPasswordText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.ConfirmPassword$}"));
        }
        set
        {
            SetValue("ConfirmPasswordText", value);
            //lblConfirmPassword.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the button text.
    /// </summary>
    public string ButtonText
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("ButtonText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Button$}"));
        }
        set
        {
            SetValue("ButtonText", value);
            cbCreateAccount.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the captcha label text.
    /// </summary>
    public string CaptchaText
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("CaptchaText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Captcha$}"));
        }
        set
        {
            SetValue("CaptchaText", value);
            lblCaptcha.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets registration approval page URL.
    /// </summary>
    public string ApprovalPage
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("ApprovalPage"), "");
        }
        set
        {
            SetValue("ApprovalPage", value);
        }
    }

    #endregion


    #region "Registration properties"

    /// <summary>
    /// Gets or sets the value that indicates whether email to user should be sent.
    /// </summary>
    public bool SendWelcomeEmail
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("SendWelcomeEmail"), true);
        }
        set
        {
            SetValue("SendWelcomeEmail", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether user is enabled after registration.
    /// </summary>
    public bool EnableUserAfterRegistration
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("EnableUserAfterRegistration"), true);
        }
        set
        {
            SetValue("EnableUserAfterRegistration", value);
        }
    }


    /// <summary>
    /// Gets or sets the sender email (from).
    /// </summary>
    public string FromAddress
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("FromAddress"), SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".CMSNoreplyEmailAddress"));
        }
        set
        {
            SetValue("FromAddress", value);
        }
    }


    /// <summary>
    /// Gets or sets the recipient email (to).
    /// </summary>
    public string ToAddress
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("ToAddress"), SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".CMSAdminEmailAddress"));
        }
        set
        {
            SetValue("ToAddress", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether after successful registration is 
    /// notification email sent to the administrator 
    /// </summary>
    public bool NotifyAdministrator
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("NotifyAdministrator"), false);
        }
        set
        {
            SetValue("NotifyAdministrator", value);
        }
    }


    /// <summary>
    /// Gets or sets the roles where is user assigned after successful registration.
    /// </summary>
    public string AssignRoles
    {
        get
        {
            return ValidationHelper.GetString(GetValue("AssignToRoles"), "");
        }
        set
        {
            SetValue("AssignToRoles", value);
        }
    }


    /// <summary>
    /// Gets or sets the sites where is user assigned after successful registration.
    /// </summary>
    public string AssignToSites
    {
        get
        {
            return ValidationHelper.GetString(GetValue("AssignToSites"), "");
        }
        set
        {
            SetValue("AssignToSites", value);
        }
    }


    /// <summary>
    /// Gets or sets the message which is displayed after successful registration.
    /// </summary>
    public string DisplayMessage
    {
        get
        {
            return ValidationHelper.GetString(GetValue("DisplayMessage"), "");
        }
        set
        {
            SetValue("DisplayMessage", value);
        }
    }


    /// <summary>
    /// Gets or set the url where is user redirected after successful registration.
    /// </summary>
    public string RedirectToURL
    {
        get
        {
            return ValidationHelper.GetString(GetValue("RedirectToURL"), "");
        }
        set
        {
            SetValue("RedirectToURL", value);
        }
    }


    /// <summary>
    /// Gets or sets value that indicates whether the captcha image should be displayed.
    /// </summary>
    public bool DisplayCaptcha
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayCaptcha"), false);
        }
        set
        {
            SetValue("DisplayCaptcha", value);
            plcCaptcha.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets the default starting alias path for newly registered user.
    /// </summary>
    public string StartingAliasPath
    {
        get
        {
            return ValidationHelper.GetString(GetValue("StartingAliasPath"), "");
        }
        set
        {
            SetValue("StartingAliasPath", value);
        }
    }


    /// <summary>
    /// Gets or sets the password minimal length.
    /// </summary>
    public int PasswordMinLength
    {
        get
        {
            return ValidationHelper.GetInteger(GetValue("PasswordMinLength"), 0);
        }
        set
        {
            SetValue("PasswordMinLength", value);
        }
    }

    #endregion


    #region "Conversion properties"

    /// <summary>
    /// Gets or sets the conversion track name used after successful registration.
    /// </summary>
    public string TrackConversionName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("TrackConversionName"), "");
        }
        set
        {
            if (value.Length > 400)
            {
                value = value.Substring(0, 400);
            }
            SetValue("TrackConversionName", value);
        }
    }


    /// <summary>
    /// Gets or sets the conversion value used after successful registration.
    /// </summary>
    public double ConversionValue
    {
        get
        {
            return ValidationHelper.GetDoubleSystem(GetValue("ConversionValue"), 0);
        }
        set
        {
            SetValue("ConversionValue", value);
        }
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (TypedPassword != null)
        {
            int pwLength = TypedPassword.Length;
            string pwDisplay = "";
            for (int x = 0; x < pwLength; x++)
            {
                pwDisplay += "*";
            }
            ctbPassword.Attributes.Add("value", pwDisplay);
            ctbConfirmPassword.Attributes.Add("value", pwDisplay);
        }
    }

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do not process
            //rfvFirstName.Enabled = false;
            //rfvEmail.Enabled = false;
            //rfvConfirmPassword.Enabled = false;
            //rfvLastName.Enabled = false;
        }
        else
        {
            ViewState["EditMode"] = false;
            ViewState["SkillsList"] = new List<JobValuePair>(0);
            ViewState["LicenseList"] = new List<JobValuePair>(0);
            ViewState["JobExpList"] = new List<JobExperience>(0);

            string skillsCustomTable = "customtable.JobSkills";
            string licenseCustomTable = "customtable.LicenseAndCerts";
            string positionsCustomTable = "customtable.JobPositions";
            string venuesCustomTable = "customtable.JobVenues";
            string venueTypeCustomTable = "customtable.JobVenueTypes";

            DataClassInfo skillsDataClassInfo = DataClassInfoProvider.GetDataClassInfo(skillsCustomTable);
            DataClassInfo licenseDataClassInfo = DataClassInfoProvider.GetDataClassInfo(licenseCustomTable);
            DataClassInfo positionsDataClassInfo = DataClassInfoProvider.GetDataClassInfo(positionsCustomTable);
            DataClassInfo venuesDataClassInfo = DataClassInfoProvider.GetDataClassInfo(venuesCustomTable);
            DataClassInfo venueTypeDataClassInfo = DataClassInfoProvider.GetDataClassInfo(venueTypeCustomTable);

            List<JobValuePair> skillsPairList = new List<JobValuePair>(0);
            List<JobValuePair> licensePairList = new List<JobValuePair>(0);
            List<JobValuePair> positionsPairList = new List<JobValuePair>(0);
            List<JobValuePair> venuesPairList = new List<JobValuePair>(0);
            List<JobValuePair> venueTypePairList = new List<JobValuePair>(0);

            foreach (var skill in CustomTableItemProvider.GetItems(skillsCustomTable))
            {
                skillsPairList.Add(new JobValuePair
                {
                    Text = ValidationHelper.GetString(skill.GetValue("Skill"), ""),
                    Value = ValidationHelper.GetString(skill.GetValue("ItemID"), "")
                });
            }

            foreach (var license in CustomTableItemProvider.GetItems(licenseCustomTable))
            {
                licensePairList.Add(new JobValuePair
                {
                    Text = ValidationHelper.GetString(license.GetValue("Name"), ""),
                    Value = ValidationHelper.GetString(license.GetValue("ItemID"), "")
                });
            }

            foreach (var position in CustomTableItemProvider.GetItems(positionsCustomTable))
            {
                positionsPairList.Add(new JobValuePair
                {
                    Text = ValidationHelper.GetString(position.GetValue("Position"), ""),
                    Value = ValidationHelper.GetString(position.GetValue("ItemID"), "")
                });
            }

            foreach (var venue in CustomTableItemProvider.GetItems(venuesCustomTable))
            {
                venuesPairList.Add(new JobValuePair
                {
                    Text = ValidationHelper.GetString(venue.GetValue("Venue"), ""),
                    Value = ValidationHelper.GetString(venue.GetValue("ItemID"), "")
                });
            }

            foreach (var venue in CustomTableItemProvider.GetItems(venueTypeCustomTable))
            {
                venueTypePairList.Add(new JobValuePair
                {
                    Text = ValidationHelper.GetString(venue.GetValue("Type"), ""),
                    Value = ValidationHelper.GetString(venue.GetValue("ItemID"), "")
                });
            }

            ccblSkills.DataSource = skillsPairList;
            ccblSkills.DataBind();

            ccblLicenses.DataSource = licensePairList;
            ccblLicenses.DataBind();

            positionsPairList.Insert(0, new JobValuePair { Text = "", Value = "" });
            cddlJobPositions.DataSource = positionsPairList;
            cddlJobPositions.DataBind();

            venuesPairList.Insert(0, new JobValuePair { Text = "", Value = "" });
            cddlJobVenues.DataSource = venuesPairList;
            cddlJobVenues.DataBind();

            crblVenueTypes.DataSource = venueTypePairList;
            crblVenueTypes.DataBind();

            // Set default visibility
            //pnlForm.Visible = true;
            lblText.Visible = false;

            // Set texts
            //lblFirstName.Text = FirstNameText;
            //lblLastName.Text = LastNameText;
            //lblEmail.Text = EmailText;
            //lblPassword.Text = PasswordText;
            //lblConfirmPassword.Text = ConfirmPasswordText;
            //cbCreateAccount.Text = ButtonText;
            lblCaptcha.Text = CaptchaText;

            if (MFAuthenticationHelper.IsMultiFactorAutEnabled && !MFAuthenticationHelper.IsMultiFactorAutRequired)
            {
                plcMFIsRequired.Visible = true;
                chkUseMultiFactorAutentization.ToolTip = GetString("webparts_membership_registrationform.mfexplanationtext");
            }
            // Set required field validators texts
            //rfvFirstName.ErrorMessage = GetString("Webparts_Membership_RegistrationForm.rfvFirstName");
            //rfvLastName.ErrorMessage = GetString("Webparts_Membership_RegistrationForm.rfvLastName");
            //rfvEmail.ErrorMessage = GetString("Webparts_Membership_RegistrationForm.rfvEmail");
            //rfvConfirmPassword.ErrorMessage = GetString("Webparts_Membership_RegistrationForm.rfvConfirmPassword");

            // Add unique validation form
            //rfvFirstName.ValidationGroup = ClientID + "_registration";
            //rfvLastName.ValidationGroup = ClientID + "_registration";
            //rfvEmail.ValidationGroup = ClientID + "_registration";
            //passStrength.ValidationGroup = ClientID + "_registration";
            //rfvConfirmPassword.ValidationGroup = ClientID + "_registration";
            //cbCreateAccount.ValidationGroup = ClientID + "_registration";


            // Set SkinID
            if (!StandAlone && (PageCycle < PageCycleEnum.Initialized))
            {
                SetSkinID(SkinID);
            }

            plcCaptcha.Visible = DisplayCaptcha;

            // WAI validation
            //lblPassword.AssociatedControlClientID = passStrength.InputClientID;

        }

        // Register client side validation
        //ctbEmail.RegisterCustomValidator(rfvEmail);
    }


    /// <summary>
    /// Sets SkinID.
    /// </summary>
    private void SetSkinID(string skinId)
    {
        if (skinId != "")
        {
            //lblFirstName.SkinID = skinId;
            //lblLastName.SkinID = skinId;
            //lblEmail.SkinID = skinId;
            //lblPassword.SkinID = skinId;
            //lblConfirmPassword.SkinID = skinId;
            ctbFirstName.SkinID = skinId;
            ctbLastName.SkinID = skinId;
            ctbEmail.SkinID = skinId;
            //passStrength.SkinID = skinId;
            ctbConfirmPassword.SkinID = skinId;
            cbCreateAccount.SkinID = skinId;
        }
    }
   
    protected bool ValidateEmail()
    {
        String siteName = SiteContext.CurrentSiteName;

        string[] siteList = { siteName };

        // If AssignToSites field set
        if (!String.IsNullOrEmpty(AssignToSites))
        {
            siteList = AssignToSites.Split(';');
        }

        // Check whether another user with this user name (which is effectively email) does not exist 
        UserInfo ui = UserInfoProvider.GetUserInfo(ctbEmail.Text);
        SiteInfo si = SiteContext.CurrentSite;
        UserInfo siteui = UserInfoProvider.GetUserInfo(UserInfoProvider.EnsureSitePrefixUserName(ctbEmail.Text, si));

        StringBuilder sb = new StringBuilder();
        bool throwError = false;

        if ((ui != null) || (siteui != null))
        {
            sb.Append(GetString("Webparts_Membership_RegistrationForm.UserAlreadyExists").Replace("%%name%%", HTMLHelper.HTMLEncode(ctbEmail.Text)) + "<br />");
            throwError = true;
            clEmail1Error.Text = "User already exists";
            clEmail1Error.Visible = true;
            ctbEmail.Attributes.Add("class", "error-control");
        }

        if ((!ctbEmail.IsValid()) || (ctbEmail.Text.Length > EMAIL_MAX_LENGTH))
        {
            sb.Append(String.Format(GetString("Webparts_Membership_RegistrationForm.EmailIsNotValid"), EMAIL_MAX_LENGTH) + "<br />");
            throwError = true;
        }

        if (ctbEmail.Text != ctbEmail2.Text)
        {
            sb.Append("Email addresses must match <br />");
            throwError = true;
        }

        if (ctbEmail.Text.Trim() == "")
       {
           ctbEmail.CssClass = "test";
          ctbEmail.Attributes["style"] = "border:1px solid red;";
           sb.Append("Please enter a valid email address <br />");
            throwError = true;
        }

        if (ctbCardNumber.Text.Trim().Length < 1)
        {
            ctbCardNumber.AddCssClass("error-control");
            //rfvCardNumber.Text = "Enter a valid card number";
            sb.Append("Please enter a valid Union Card Number");
            throwError = true;
        }

        if (throwError)
        {
            ShowError(sb.ToString());
            return false;
        }
      
        return true;
    }

    protected bool ValidatePassword()
    {
        // Check whether password is same
        if (ctbPassword.Text != ctbConfirmPassword.Text)
        {
            ShowError(GetString("Webparts_Membership_RegistrationForm.PassworDoNotMatch"));
            return false;
        }

        if ((PasswordMinLength > 0) && (ctbPassword.Text.Length < PasswordMinLength))
        {
            ShowError(String.Format(GetString("Webparts_Membership_RegistrationForm.PasswordMinLength"), PasswordMinLength));
            return false;
        }

        return true;

        //if (!passStrength.IsValid())
        //{
        //    ShowError(AuthenticationHelper.GetPolicyViolationMessage(SiteContext.CurrentSiteName));
        //    return;
        //}
    }

    protected bool ValidateInformation()
    {
        StringBuilder sb = new StringBuilder();
        bool throwError = false;

        if (ctbFirstName.Text.Trim().Length < 1)
        {
            sb.Append("Please enter a first name <br />");
            throwError = true;
        }
        if (ctbLastName.Text.Trim().Length < 1)
        {
            sb.Append("Please enter a last name <br />");
            throwError = true;
        }
        if (ctbAddress.Text.Trim().Length < 1)
        {
            sb.Append("Please enter a street address <br />");
            throwError = true;
        }
        if (ctbCity.Text.Trim().Length < 1)
        {
            sb.Append("Please enter a city <br />");
            throwError = true;
        }
        if (ctbState.Text.Trim().Length < 1)
        {
            sb.Append("Please enter a state <br />");
            throwError = true;
        }
        if (ctbZipCode.Text.Trim().Length < 1)
        {
            sb.Append("Please enter a zip code <br />");
            throwError = true;
        }
        if (ctbCellNumber.Text.Trim().Length < 1)
        {
            sb.Append("Please enter a cell number <br />");
            throwError = true;
        }

        //TODO: validate phone number format. use jquery?

        if (throwError)
        {
            ShowError(sb.ToString());
            return false;
        }
        
        return true;
    }

    /// <summary>
    /// OK click handler (Proceed registration).
    /// </summary>
    protected void cbCreateAccount_Click(object sender, EventArgs e)
    {
        if (PortalContext.IsDesignMode(PortalContext.ViewMode) || (HideOnCurrentPage) || (!IsVisible))
        {
            // Do not process
        }
        else
        {
            String siteName = SiteContext.CurrentSiteName;
            string[] siteList = { siteName };

            // If AssignToSites field set
            if (!String.IsNullOrEmpty(AssignToSites))
            {
                siteList = AssignToSites.Split(';');
            }

            #region "Banned IPs"

            // Ban IP addresses which are blocked for registration
            if (!BannedIPInfoProvider.IsAllowed(siteName, BanControlEnum.Registration))
            {
                ShowError(GetString("banip.ipisbannedregistration"));
                return;
            }

            #endregion


            #region "Check Email & password"

            


            

            #endregion


            #region "Captcha"

            // Check if captcha is required and verify captcha text
            if (DisplayCaptcha && !scCaptcha.IsValid())
            {
                // Display error message if catcha text is not valid
                ShowError(GetString("Webparts_Membership_RegistrationForm.captchaError"));
                return;
            }

            #endregion


            #region "User properties"

            UserInfo ui = UserInfoProvider.GetUserInfo(ctbEmail.Text);
            SiteInfo si = SiteContext.CurrentSite;
            UserInfo siteui = UserInfoProvider.GetUserInfo(UserInfoProvider.EnsureSitePrefixUserName(ctbEmail.Text, si));

            var userEmail = ctbEmail.Text.Trim();
            
            ui = new UserInfo();
            ui.PreferredCultureCode = "";
            ui.Email = userEmail;
            ui.FirstName = ctbFirstName.Text.Trim();
            ui.LastName = ctbLastName.Text.Trim();
            ui.FullName = UserInfoProvider.GetFullName(ui.FirstName, String.Empty, ui.LastName);
            ui.MiddleName = "";
            ui.UserMFRequired = chkUseMultiFactorAutentization.Checked;
            

            // User name as put by user (no site prefix included)
            var plainUserName = userEmail;
            ui.UserName = plainUserName;

            // Check if the given email can be used as user name
            if (!ValidationHelper.IsUserName(plainUserName))
            {
                ShowError(String.Format(GetString("Webparts_Membership_RegistrationForm.UserNameNotValid"), HTMLHelper.HTMLEncode(plainUserName)));
                return;
            }

            // Ensure site prefixes
            if (UserInfoProvider.UserNameSitePrefixEnabled(siteName))
            {
                ui.UserName = UserInfoProvider.EnsureSitePrefixUserName(plainUserName, si);
            }

            ui.Enabled = EnableUserAfterRegistration;
            ui.UserURLReferrer = CookieHelper.GetValue(CookieName.UrlReferrer);
            ui.UserCampaign = Service<ICampaignService>.Entry().CampaignCode;

            ui.SiteIndependentPrivilegeLevel = UserPrivilegeLevelEnum.None;

            ui.UserSettings.UserRegistrationInfo.IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            ui.UserSettings.UserRegistrationInfo.Agent = HttpContext.Current.Request.UserAgent;

            List<JobValuePair> SkillsList = (List<JobValuePair>)ViewState["SkillsList"];
            string skills = "";
            if (SkillsList != null && SkillsList.Any())
                skills = string.Join(",", SkillsList.Select(x => x.Value));

            List<JobValuePair> LicenseList = (List<JobValuePair>)ViewState["LicenseList"];
            string licenses = "";
            if (LicenseList != null && LicenseList.Any())
                licenses = string.Join(",", LicenseList.Select(x => x.Value));

            ui.SetValue("UnionCardNumber", ctbCardNumber.Text.Trim());
            ui.SetValue("StreetAddress", ctbAddress.Text.Trim());
            ui.SetValue("City", ctbCity.Text.Trim());
            ui.SetValue("State", ctbState.Text.Trim());
            ui.SetValue("ZipCode", ctbZipCode.Text.Trim());
            ui.SetValue("HomeNumber", ctbHomeNumber.Text.Trim());
            ui.SetValue("CellNumber", ctbCellNumber.Text.Trim());
            ui.SetValue("JobSkillIds", skills);
            ui.SetValue("LicenseAndCertIds", licenses);

            // Check whether confirmation is required
            bool requiresConfirmation = SettingsKeyInfoProvider.GetBoolValue(siteName + ".CMSRegistrationEmailConfirmation");
            bool requiresAdminApprove = false;

            if (!requiresConfirmation)
            {
                // If confirmation is not required check whether administration approval is required
                requiresAdminApprove = SettingsKeyInfoProvider.GetBoolValue(siteName + ".CMSRegistrationAdministratorApproval");
                if (requiresAdminApprove)
                {
                    ui.Enabled = false;
                    ui.UserSettings.UserWaitingForApproval = true;
                }
            }
            else
            {
                // EnableUserAfterRegistration is overridden by requiresConfirmation - user needs to be confirmed before enable
                ui.Enabled = false;
            }

            // Set user's starting alias path
            if (!String.IsNullOrEmpty(StartingAliasPath))
            {
                ui.UserStartingAliasPath = MacroResolver.ResolveCurrentPath(StartingAliasPath);
            }

            #endregion


            #region "Reserved names"

            // Check for reserved user names like administrator, sysadmin, ...
            if (UserInfoProvider.NameIsReserved(siteName, plainUserName))
            {
                ShowError(GetString("Webparts_Membership_RegistrationForm.UserNameReserved").Replace("%%name%%", HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(ui.UserName, true))));
                return;
            }

            if (UserInfoProvider.NameIsReserved(siteName, ui.UserNickName))
            {
                ShowError(GetString("Webparts_Membership_RegistrationForm.UserNameReserved").Replace("%%name%%", HTMLHelper.HTMLEncode(ui.UserNickName)));
                return;
            }

            #endregion


            #region "License limitations"

            string errorMessage = String.Empty;
            UserInfoProvider.CheckLicenseLimitation(ui, ref errorMessage);

            if (!String.IsNullOrEmpty(errorMessage))
            {
                ShowError(errorMessage);
                return;
            }

            #endregion


            // Check whether email is unique if it is required
            if (!UserInfoProvider.IsEmailUnique(userEmail, siteList, 0))
            {
                ShowError(GetString("UserInfo.EmailAlreadyExist"));
                return;
            }

            // Set password
            UserInfoProvider.SetPassword(ui, TypedPassword);

            // User has been created, add jobs
            List<JobExperience> JobExpList = (List<JobExperience>)ViewState["JobExpList"];
            if (JobExpList != null && JobExpList.Any())
            {
                string jobExperienceCustomTable = "customtable.MemberDirJobExp";
                DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(jobExperienceCustomTable);
                if (customTable != null)
                {
                    foreach (JobExperience job in JobExpList)
                    {
                        CustomTableItem item = CustomTableItem.New(jobExperienceCustomTable);
                        item.SetValue("UserID", ui.UserID);
                        item.SetValue("VenueTypeID", job.VenueTypeID);
                        item.SetValue("JobPosition", job.Position);
                        item.SetValue("Venue", job.Venue);
                        item.SetValue("StartDate", job.StartDate);
                        DateTime dt;
                        if (DateTime.TryParse(job.EndDate, out dt))
                        {
                            item.SetValue("EndDate", job.EndDate);
                        }                        
                        item.SetValue("CurrentPosition", job.CurrentPosition);
                        item.SetValue("JobDescription", job.Description);

                        try
                        {
                            item.Insert();
                        }
                        catch (Exception ex)
                        {
                            EventLogProvider.LogEvent(EventType.ERROR, 
                                "User Registration", 
                                "There was an issue inserting a user's job experience into the RegisteredMemberJobs custom table: " + ex.Message + "\n" + ex.StackTrace);
                        }
                    }
                }
            }


            #region "Welcome Emails (confirmation, waiting for approval)"

            bool error = false;
            EmailTemplateInfo template = null;

            string emailSubject = null;
            // Send welcome message with username and password, with confirmation link, user must confirm registration
            if (requiresConfirmation)
            {
                template = EmailTemplateProvider.GetEmailTemplate("RegistrationConfirmation", siteName);
                emailSubject = EmailHelper.GetSubject(template, GetString("RegistrationForm.RegistrationConfirmationEmailSubject"));
            }
            // Send welcome message with username and password, with information that user must be approved by administrator
            else if (SendWelcomeEmail)
            {
                if (requiresAdminApprove)
                {
                    template = EmailTemplateProvider.GetEmailTemplate("Membership.RegistrationWaitingForApproval", siteName);
                    emailSubject = EmailHelper.GetSubject(template, GetString("RegistrationForm.RegistrationWaitingForApprovalSubject"));
                }
                // Send welcome message with username and password, user can logon directly
                else
                {
                    template = EmailTemplateProvider.GetEmailTemplate("Membership.Registration", siteName);
                    emailSubject = EmailHelper.GetSubject(template, GetString("RegistrationForm.RegistrationSubject"));
                }
            }

            if (template != null)
            {
                // Create relation between contact and user. This ensures that contact will be correctly recognized when user approves registration (if approval is required)
                int contactId = ModuleCommands.OnlineMarketingGetCurrentContactID();
                if (contactId > 0)
                {
                    ModuleCommands.OnlineMarketingCreateRelation(ui.UserID, MembershipType.CMS_USER, contactId);
                }

                // Email message
                EmailMessage email = new EmailMessage();
                email.EmailFormat = EmailFormatEnum.Default;
                email.Recipients = ui.Email;
                email.From = SettingsKeyInfoProvider.GetValue(siteName + ".CMSNoreplyEmailAddress");
                email.Subject = emailSubject;

                try
                {
                    var resolver = MembershipResolvers.GetMembershipRegistrationResolver(ui, AuthenticationHelper.GetRegistrationApprovalUrl(ApprovalPage, ui.UserGUID, siteName, NotifyAdministrator));
                    EmailSender.SendEmailWithTemplateText(siteName, email, template, resolver, true);
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("E", "RegistrationForm - SendEmail", ex);
                    error = true;
                }
            }

            // If there was some error, user must be deleted
            if (error)
            {
                ShowError(GetString("RegistrationForm.UserWasNotCreated"));

                // Email was not send, user can't be approved - delete it
                UserInfoProvider.DeleteUser(ui);
                return;
            }

            #endregion


            #region "Administrator notification email"

            // Notify administrator if enabled and e-mail confirmation is not required
            if (!requiresConfirmation && NotifyAdministrator && (FromAddress != String.Empty) && (ToAddress != String.Empty))
            {
                EmailTemplateInfo mEmailTemplate;
                if (SettingsKeyInfoProvider.GetBoolValue(siteName + ".CMSRegistrationAdministratorApproval"))
                {
                    mEmailTemplate = EmailTemplateProvider.GetEmailTemplate("Registration.Approve", siteName);
                }
                else
                {
                    mEmailTemplate = EmailTemplateProvider.GetEmailTemplate("Registration.New", siteName);
                }

                if (mEmailTemplate == null)
                {
                    // Log missing e-mail template
                    EventLogProvider.LogEvent(EventType.ERROR, "RegistrationForm", "GetEmailTemplate", eventUrl: RequestContext.RawURL);
                }
                else
                {
                    EmailMessage message = new EmailMessage();
                    message.EmailFormat = EmailFormatEnum.Default;
                    message.From = EmailHelper.GetSender(mEmailTemplate, FromAddress);
                    message.Recipients = ToAddress;
                    message.Subject = GetString("RegistrationForm.EmailSubject");

                    try
                    {
                        MacroResolver resolver = MembershipResolvers.GetRegistrationResolver(ui);
                        EmailSender.SendEmailWithTemplateText(siteName, message, mEmailTemplate, resolver, false);
                    }
                    catch
                    {
                        EventLogProvider.LogEvent(EventType.ERROR, "Membership", "RegistrationEmail");
                    }
                }
            }

            #endregion


            #region "Web analytics"

            // Track successful registration conversion
            if (TrackConversionName != String.Empty)
            {
                if (AnalyticsHelper.AnalyticsEnabled(siteName) && !AnalyticsHelper.IsIPExcluded(siteName, RequestContext.UserHostAddress))
                {
                    // Log conversion
                    HitLogProvider.LogConversions(siteName, LocalizationContext.PreferredCultureCode, TrackConversionName, 0, ConversionValue);
                }
            }

            // Log registered user if confirmation is not required
            if (!requiresConfirmation)
            {
                AnalyticsHelper.LogRegisteredUser(siteName, ui);
            }

            #endregion


            #region "On-line marketing - activity"

            // Log registered user if confirmation is not required
            if (!requiresConfirmation)
            {
                MembershipActivityLogger.LogRegistration(ui.UserName, DocumentContext.CurrentDocument);
                // Log login activity
                if (ui.Enabled)
                {
                    MembershipActivityLogger.LogLogin(ui.UserName, DocumentContext.CurrentDocument);
                }
            }

            #endregion


            #region "Roles & authentication"

            string[] roleList = AssignRoles.Split(';');

            foreach (string sn in siteList)
            {
                // Add new user to the current site
                UserInfoProvider.AddUserToSite(ui.UserName, sn);
                foreach (string roleName in roleList)
                {
                    if (!String.IsNullOrEmpty(roleName))
                    {
                        String s = roleName.StartsWith(".", StringComparison.Ordinal) ? "" : sn;

                        // Add user to desired roles
                        if (RoleInfoProvider.RoleExists(roleName, s))
                        {
                            UserInfoProvider.AddUserToRole(ui.UserName, roleName, s);
                        }
                    }
                }
            }

            if (DisplayMessage.Trim() != String.Empty)
            {
                //pnlForm.Visible = false;
                lblText.Visible = true;
                lblText.Text = DisplayMessage;
            }
            else
            {
                if (ui.Enabled)
                {
                    AuthenticationHelper.AuthenticateUser(ui.UserName, true);
                }

                if (RedirectToURL != String.Empty)
                {
                    URLHelper.Redirect(UrlResolver.ResolveUrl(RedirectToURL));
                }

                else if (QueryHelper.GetString("ReturnURL", "") != String.Empty)
                {
                    string url = QueryHelper.GetString("ReturnURL", "");

                    // Do url decode 
                    url = Server.UrlDecode(url);

                    // Check that url is relative path or hash is ok
                    if (url.StartsWith("~", StringComparison.Ordinal) || url.StartsWith("/", StringComparison.Ordinal) || QueryHelper.ValidateHash("hash", "aliaspath"))
                    {
                        URLHelper.Redirect(UrlResolver.ResolveUrl(url));
                    }
                    // Absolute path with wrong hash
                    else
                    {
                        URLHelper.Redirect(AdministrationUrlHelper.GetErrorPageUrl("dialogs.badhashtitle", "dialogs.badhashtext"));
                    }
                }
            }

            #endregion
            
            lblError.Visible = false;
        }
    }


    /// <summary>
    /// Shows the specified error message.
    /// </summary>
    private void ShowError(string errorMessage)
    {
        lblError.Visible = true;
        lblError.Text = errorMessage;
    }

    protected void cbScreen1_Click(object sender, EventArgs e)
    {
        cPanelRegister.Visible = false;
        cPanelCreatePassword.Visible = true;
        cPanelProfile.Visible = false;
        cPanelConfirmInfo.Visible = false;

        cbScreen1.AddCssClass("active-tab");
        cbScreen2.RemoveCssClass("active-tab");
        cbScreen3.RemoveCssClass("active-tab");
    }

    protected void cbScreen2_Click(object sender, EventArgs e)
    {
        cPanelRegister.Visible = false;
        cPanelCreatePassword.Visible = false;
        cPanelProfile.Visible = true;
        cPanelConfirmInfo.Visible = false;

        cbScreen1.RemoveCssClass("active-tab");
        cbScreen2.AddCssClass("active-tab");
        cbScreen3.RemoveCssClass("active-tab");
    }
    protected void cbScreen3_Click(object sender, EventArgs e)
    {
        cPanelRegister.Visible = false;
        cPanelCreatePassword.Visible = false;
        cPanelProfile.Visible = false;
        cPanelConfirmInfo.Visible = true;

        cbScreen1.RemoveCssClass("active-tab");
        cbScreen2.RemoveCssClass("active-tab");
        cbScreen3.AddCssClass("active-tab");

        // Copy info from 2nd to 3rd screen
        FinalizeValues();
    }

    protected void cbRegister_Click(object sender, EventArgs e)
    {
        lblError.Visible = false;

        if (ValidateEmail())
        {
            cPanelButtons.Visible = true;
            cPanelRegister.Visible = false;
            cPanelCreatePassword.Visible = true;
            cPanelProfile.Visible = false;
            cPanelConfirmInfo.Visible = false;

            cbScreen1.AddCssClass("active-tab");
            cbScreen2.RemoveCssClass("active-tab");
            cbScreen3.RemoveCssClass("active-tab");
        }

    }

    protected void cbCreatePassword_Click(object sender, EventArgs e)
    {
        lblError.Visible = false;

        //if (ValidatePassword())
        //{
            cPanelRegister.Visible = false;
            cPanelCreatePassword.Visible = false;
            cPanelProfile.Visible = true;
            cPanelConfirmInfo.Visible = false;

            cbScreen1.RemoveCssClass("active-tab");
            cbScreen2.AddCssClass("active-tab");
            cbScreen3.RemoveCssClass("active-tab");

            TypedPassword = ctbPassword.Text;
        //}
        //else
        //{
        //    ctbPassword.Attributes.Add("value", "");
        //    ctbConfirmPassword.Attributes.Add("value", "");
        //}
    }



    protected void cbSaveProfile_Click(object sender, EventArgs e)
    {
        lblError.Visible = false;

        //if (ValidateInformation())
        //{
            cPanelRegister.Visible = false;
            cPanelCreatePassword.Visible = false;
            cPanelProfile.Visible = false;
            cPanelConfirmInfo.Visible = true;

            cbScreen1.RemoveCssClass("active-tab");
            cbScreen2.RemoveCssClass("active-tab");
            cbScreen3.AddCssClass("active-tab");

            // Copy values from 2nd to 3rd screen
            FinalizeValues();
        //}
    }

    
    protected void cbAddAnotherJob_Click(object sender, EventArgs e)
    {

    }
    #endregion   

    protected void FinalizeValues()
    {
        clName.Text = ctbFirstName.Text.Trim() + " " + ctbLastName.Text.Trim();
        clCardNumber.Text = ctbCardNumber.Text.Trim();
        clStreetAddress.Text = ctbAddress.Text.Trim();
        clCityStateZip.Text = ctbCity.Text.Trim() + ", " + ctbState.Text.Trim() + " " + ctbZipCode.Text.Trim();
        clCellPhone.Text = ctbCellNumber.Text.Trim();
        clHomePhone.Text = ctbHomeNumber.Text.Trim();
        clEmail.Text = ctbEmail.Text.Trim();

        ctbFirstNameModal.Text = ctbFirstName.Text.Trim();
        ctbLastNameModal.Text = ctbLastName.Text.Trim();
        ctbCardNumberModal.Text = ctbCardNumber.Text.Trim();
        ctbAddressModal.Text = ctbAddress.Text.Trim();
        ctbCityModal.Text = ctbCity.Text.Trim();
        ctbStateModal.Text = ctbState.Text.Trim();
        ctbZipCodeModal.Text = ctbZipCode.Text.Trim();
        ctbCellNumberModal.Text = ctbCellNumber.Text.Trim();
        ctbHomeNumberModal.Text = ctbHomeNumber.Text.Trim();

        List<JobValuePair> SkillsList = (List<JobValuePair>)ViewState["SkillsList"];
        if (SkillsList != null && SkillsList.Count > 0)
        {
            crepJobSkillsFinal.DataSource = SkillsList;
            crepJobSkillsFinal.DataBind();
            //clbEditJobsFinal.Text = "Edit";
        }
        else
        {
            //clbEditJobsFinal.Text = "Add";
        }

        List<JobValuePair> LicenseList = (List<JobValuePair>)ViewState["LicenseList"];
        if (LicenseList != null && LicenseList.Count > 0)
        {
            crepLicensesFinal.DataSource = LicenseList;
            crepLicensesFinal.DataBind();
            //clbEditLicensesFinal.Text = "Edit";
        }
        else
        {
            //clbEditLicensesFinal.Text = "Add";
        }

        List<JobExperience> JobExpList = (List<JobExperience>)ViewState["JobExpList"];
        if (JobExpList != null)
        {
            crepJobsFinal.DataSource = JobExpList;
            crepJobsFinal.DataBind();
        }


    }

    protected void clbAddJobVenue_Click(object sender, EventArgs e)
    {
        cPanelVenue1.Visible = false;
        cPanelVenue2.Visible = true;

        
    }

    protected void cbAddCustomVenue_Click(object sender, EventArgs e)
    {
        cPanelVenue1.Visible = true;
        cPanelVenue2.Visible = false;

        cddlJobVenues.Items.Insert(1, ctbAddJobVenue.Text.Trim());
        cddlJobVenues.SelectedIndex = 1;
    }

    protected void cbCancelCustomVenue_Click(object sender, EventArgs e)
    {
        cPanelVenue1.Visible = true;
        cPanelVenue2.Visible = false;
    }

    protected void clbAddJobPosition_Click(object sender, EventArgs e)
    {
        cPanelJobPositions1.Visible = false;
        cPanelJobPositions2.Visible = true;
    }

    protected void cbAddCustomPosition_Click(object sender, EventArgs e)
    {
        cPanelJobPositions1.Visible = true;
        cPanelJobPositions2.Visible = false;

        cddlJobPositions.Items.Insert(1, ctbAddJobPosition.Text.Trim());
        cddlJobPositions.SelectedIndex = 1;
    }

    protected void cbCancelCustomPosition_Click(object sender, EventArgs e)
    {
        cPanelJobPositions1.Visible = true;
        cPanelJobPositions2.Visible = false;
    }

    protected void cbSaveJobSkills_Click(object sender, EventArgs e)
    {
        List<JobValuePair> SkillsList = new List<JobValuePair>(0);

        var selectedItems = ccblSkills.GetSelectedItems();
        if (selectedItems != null)
        {
            foreach (var skill in selectedItems)
            {
                SkillsList.Add(new JobValuePair { Text = skill.Text, Value = skill.Value });
            }
        }

        if (SkillsList.Count > 0)
        {
            clbAddSkillsInitial.Text = "Edit";
            //clbAddSkillsFinal.Text = "Edit";
        }
        else
        {
            clbAddSkillsInitial.Text = "Add Job Skills";
            //clbAddSkillsFinal.Text = "Add Job Skills";
        }

        ViewState["SkillsList"] = SkillsList;
        crepJobSkillsInitial.DataSource = SkillsList;
        crepJobSkillsInitial.DataBind();

        crepJobSkillsFinal.DataSource = SkillsList;
        crepJobSkillsFinal.DataBind();
    }

    protected void cbSaveLicenses_Click(object sender, EventArgs e)
    {
        List<JobValuePair> LicenseList = new List<JobValuePair>(0);

        var selectedItems = ccblLicenses.GetSelectedItems();
        if (selectedItems != null)
        {
            foreach (var license in selectedItems)
            {
                LicenseList.Add(new JobValuePair { Text = license.Text, Value = license.Value });
            }
        }

        if (LicenseList.Count > 0)
        {
            clbAddLicensesInitial.Text = "Edit";
            //clbAddLicensesFinal.Text = "Edit";
        }
        else
        {
            clbAddLicensesInitial.Text = "Add Licenses & Certifications";
            //clbAddLicensesFinal.Text = "Add Licenses & Certifications";
        }

        ViewState["LicenseList"] = LicenseList;
        crepLicensesInitial.DataSource = LicenseList;
        crepLicensesInitial.DataBind();

        crepLicensesFinal.DataSource = LicenseList;
        crepLicensesFinal.DataBind();
    }

    protected void cbSaveJob_Click(object sender, EventArgs e)
    {
       
        List<JobExperience> SavedJobs = (List<JobExperience>)ViewState["JobExpList"];

        string status = "";

        if (ccbCurrentPosition.Checked)
            status = "Current";
        else
            status = ctbJobEndDate.Text;

        //DateTime? dtEnd;
        //if (ctbJobEndDate.Text == "")
        //    dtEnd = null;
        //else
        //    dtEnd = Convert.ToDateTime(ctbJobEndDate.Text);

        bool bEditMode = (bool)ViewState["EditMode"];
        if (bEditMode)
        {
            int? ID = Convert.ToInt32(chfJobExp.Value);
            if (ID != null && SavedJobs != null && SavedJobs.Count > 0)
            {
                JobExperience job = SavedJobs[Convert.ToInt32(ID)];
                if (job != null)
                {
                    job.VenueType = crblVenueTypes.SelectedItem.Text;
                    job.VenueTypeID = crblVenueTypes.SelectedValue;
                    job.Position = cddlJobPositions.SelectedItem.Text;
                    job.Venue = cddlJobVenues.SelectedItem.Text;
                    job.StartDate = ctbJobStartDate.Text.Trim();
                    job.EndDate = ctbJobEndDate.Text.Trim();
                    job.CurrentPosition = ccbCurrentPosition.Checked;
                    job.Description = ctbJobDescription.Text.Trim();
                    job.Status = status;

                    ViewState["JobExpList"] = SavedJobs;
                    crepJobsInitial.DataSource = SavedJobs;
                    crepJobsInitial.DataBind();

                    crepJobsFinal.DataSource = SavedJobs;
                    crepJobsFinal.DataBind();

                    ViewState["EditMode"] = false;
                }
            }

        }
        else
        {
            List<JobExperience> JobExpList = new List<JobExperience>(0);
            
            if (SavedJobs != null && SavedJobs.Count > 0)
            {
                foreach (JobExperience job in SavedJobs)
                {
                    JobExpList.Add(job);
                }
            }

            JobExpList.Add(new JobExperience
            {
                ID = JobExpList.Count + 1,
                VenueType = crblVenueTypes.SelectedItem.Text,
                VenueTypeID = crblVenueTypes.SelectedValue,
                Position = cddlJobPositions.SelectedItem.Text,
                Venue = cddlJobVenues.SelectedItem.Text,
                StartDate = ctbJobStartDate.Text.Trim(),
                EndDate = ctbJobEndDate.Text.Trim(),
                CurrentPosition = ccbCurrentPosition.Checked,
                Description = ctbJobDescription.Text.Trim(),
                Status = status
            });

            ViewState["JobExpList"] = JobExpList;

            crepJobsInitial.DataSource = JobExpList;
            crepJobsInitial.DataBind();

            crepJobsFinal.DataSource = JobExpList;
            crepJobsFinal.DataBind();
        }        
    }

    protected void crepJobsInitial_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
    {
        List<JobExperience> JobExpList = (List<JobExperience>)ViewState["JobExpList"];
        if (JobExpList != null && JobExpList.Count > 0)
        {
            int ID = Convert.ToInt32(e.CommandArgument);
            JobExperience job = JobExpList[ID];

            if (job != null)
            {
                if (e.CommandName == "edit")
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Open Jobs Initial", "openJobsInitial();", true);

                    crblVenueTypes.SelectedValue = job.VenueTypeID;
                    cddlJobPositions.SelectedItem.Text = job.Position;
                    cddlJobVenues.SelectedItem.Text = job.Venue;
                    ctbJobStartDate.Text = job.StartDate;
                    ctbJobEndDate.Text = job.EndDate;
                    ccbCurrentPosition.Checked = job.CurrentPosition;
                    ctbJobDescription.Text = job.Description;
                    chfJobExp.Value = ID.ToString();

                    ViewState["EditMode"] = true;

                }
                else if (e.CommandName == "delete")
                {
                    JobExpList.Remove(job);

                    crepJobsInitial.DataSource = JobExpList;
                    crepJobsInitial.DataBind();

                    crepJobsFinal.DataSource = JobExpList;
                    crepJobsFinal.DataBind();
                }
            }
        }
    }

    protected void cbSaveInfoModal_Click(object sender, EventArgs e)
    {
        clName.Text = ctbFirstNameModal.Text.Trim() + " " + ctbLastNameModal.Text.Trim();
        clCardNumber.Text = ctbCardNumberModal.Text.Trim();
        clStreetAddress.Text = ctbAddressModal.Text.Trim();
        clCityStateZip.Text = ctbCityModal.Text.Trim() + ", " + ctbStateModal.Text.Trim() + " " + ctbZipCodeModal.Text.Trim();
        clCellPhone.Text = ctbCellNumberModal.Text.Trim();
        clHomePhone.Text = ctbHomeNumberModal.Text.Trim();

        ctbFirstName.Text = ctbFirstNameModal.Text.Trim();
        ctbLastName.Text = ctbLastNameModal.Text.Trim();
        ctbCardNumber.Text = ctbCardNumberModal.Text.Trim();
        ctbAddress.Text = ctbAddressModal.Text.Trim();
        ctbCity.Text = ctbCityModal.Text.Trim();
        ctbState.Text = ctbStateModal.Text.Trim();
        ctbZipCode.Text = ctbZipCodeModal.Text.Trim();
        ctbCellNumber.Text = ctbCellNumberModal.Text.Trim();
        ctbHomeNumber.Text = ctbHomeNumberModal.Text.Trim();
    }

    protected void clbEditInfoFinal_Click(object sender, EventArgs e)
    {
        //ctbFirstNameModal.Text = ctbFirstName.Text.Trim();
    }

    protected void clbAddJobInitial_Click(object sender, EventArgs e)
    {
        crblVenueTypes.ClearSelection();
        cddlJobPositions.ClearSelection();
        cddlJobVenues.ClearSelection();
        ctbAddJobPosition.Text = "";
        ctbAddJobVenue.Text = "";
        ctbJobStartDate.Text = "";
        ctbJobEndDate.Text = "";
        ccbCurrentPosition.Checked = false;
        ctbJobDescription.Text = "";
    }

    protected void clbAddJobFinal_Click(object sender, EventArgs e)
    {
        crblVenueTypes.ClearSelection();
        cddlJobPositions.ClearSelection();
        cddlJobVenues.ClearSelection();
        ctbAddJobPosition.Text = "";
        ctbAddJobVenue.Text = "";
        ctbJobStartDate.Text = "";
        ctbJobEndDate.Text = "";
        ccbCurrentPosition.Checked = false;
        ctbJobDescription.Text = "";
    }
}