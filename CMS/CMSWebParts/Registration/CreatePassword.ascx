<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_Registration_CreatePassword"  CodeFile="~/CMSWebParts/Registration/CreatePassword.ascx.cs" %>
<style>
    input {
        border:1px solid #000;
    }
</style>
<div class="create-password" style="background-color:#fff;">
    <h3>Create your password</h3>
    <p>Please create a password to begin creating your account.</p>
    <div class="form">
        <div class="left">
            Password<br />
            <asp:TextBox ID="ctbPassword1" runat="server" />
        </div>
        <div class="right">
            Confirm Password<br />
            <asp:TextBox ID="ctbPassword2" runat="server" />
        </div>
        <asp:Button ID="cbSubmit" runat="server" Text="Submit" />
    </div>
</div>