using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;
using System.Collections.Generic;
using CMS.DataEngine;
using CMS.CustomTables;
using CMS.Membership;
using CMS.SiteProvider;
using System.Collections.Specialized;
using System.Diagnostics;

public partial class CMSWebParts_Localone_StewardReportApproval : CMSAbstractWebPart
{
    #region "Properties"
    public List<StewardJobs> StewardReportDetailsList;
    public StewardReport StewardReport;
    public List<StewardJobs> UnionJobsList;
    public List<StewardJobs> ProductionJobsList;
    private Dictionary<string, string> qParamsDictionary = new Dictionary<string, string>();
    private readonly string ApproverRole = "StewardReportsApprover";
    private readonly string ctClassStewardReportApproval = "customtable.stewardreportapproval";
    private readonly string ctClassStewardReport = "customtable.stewardreport";



    #endregion

    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {

        var crossPage = RequestHelper.IsPostBack();
        var postBack = IsPostBack;

        if (this.StopProcessing)
        {

        }
        else
        {
            // get list from custom table
            CurrentUserInfo currentUser = MembershipContext.AuthenticatedUser;

            if (currentUser == null)
            {
                Response.Redirect("~/");
                return;
            }
            // check role : if the user is not in the Steward Reports Approver
            if (!currentUser.IsInRole(ApproverRole, SiteContext.CurrentSiteName))
            {
                Response.Redirect("~/");
                return;
            }


            //1. Check if there are 3 query string parameters excluding aliaspath query string
            if (Request.QueryString.Count >= 1)
            {
                // Insert Each Key Value query string pair into the qParamsDictionary
                for (int i = 0; i < Request.QueryString.Count; i++)
                {
                    qParamsDictionary.Add(Request.QueryString.GetKey(i),
                                            Request.QueryString.Get(i));
                }
            }
            else
            {
                //LOG ERROR -- > Not enough Query Parameters Passed -- > DISPLAY MESSAGE
            }

            //2.Return Data Set of the Report Details
            DataSet reportDetails = GetReportDetails(qParamsDictionary);

            //3. Parse DataSet into List of 
            GetStewardReportJobDetails(reportDetails);

            //4. Create single report object from dataset
            CreateStewardReport(reportDetails);

            //5. Initialize ASP Controls
            SetControlValues(StewardReport);           
           
            
        }
    }

    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion

    protected void ApproveReport_Click(object sender, EventArgs e)
    {

        CustomTableItem reportTableItem = CustomTableItemProvider.GetItem(StewardReport.ReportId, ctClassStewardReportApproval);
        reportTableItem.SetValue("Approved", 1);
        reportTableItem.SetValue("Denied", 0);
        reportTableItem.SetValue("UnderApproval", 0);
        reportTableItem.SetValue("ApprovedBy", CurrentUser.UserID);
        reportTableItem.Update();
        pnlUnderApprovalStatus.Visible = false;
        pnlAprovedStatus.Visible = true;

        Response.Redirect("~/members/Steward-Report-Listing");

    }

    protected void DenyReport_Click(object sender, EventArgs e)
    {

        CustomTableItem reportTableItem = CustomTableItemProvider.GetItem(StewardReport.ReportId, ctClassStewardReportApproval);
        reportTableItem.SetValue("Approved", 0);
        reportTableItem.SetValue("Denied", 1);
        reportTableItem.SetValue("UnderApproval", 0);
        reportTableItem.SetValue("ApprovedBy", CurrentUser.UserID);
        reportTableItem.Update();
        pnlUnderApprovalStatus.Visible = false;
        pnlDeniedStatus.Visible = true;


        Response.Redirect("~/members/Steward-Report-Listing");

    }

    public DataSet GetReportDetails(Dictionary<string, string> queryParamsDictionary)
    {

        DataSet reportDetails = new DataSet();
        string reportNumber = "";
        qParamsDictionary.TryGetValue("report", out reportNumber);
        int reportId = Convert.ToInt32(reportNumber);

        var reportDetailsQuery = new DataQuery("customtable.stewardreport.GetReportDetailsByReportID");
        QueryDataParameters parameters = new QueryDataParameters();
        parameters.Add("@reportID", reportId);
        reportDetailsQuery.Parameters = parameters;

        DataSet reportDetailsDataSet = reportDetailsQuery.Result;

        return reportDetailsDataSet;

    }

    public void GetStewardReportJobDetails(DataSet reportDetailsDataSet)
    {
        StewardReportDetailsList = new List<StewardJobs>();


        if (reportDetailsDataSet.Tables.Count > 0)
        {
            try
            {
                foreach (DataRow rowItem in reportDetailsDataSet.Tables[0].Rows)
                {

                    StewardReportDetailsList.Add(new StewardJobs()
                    {
                        Name = AddFieldValue(rowItem, "Name"),
                        LocalOneUnionCardNumber = AddFieldValue(rowItem, "LocalOneUnionCardNumber"),
                        UnionCardNumber = AddFieldValue(rowItem, "UnionCardNumber"),
                        Position = AddFieldValue(rowItem, "Position"),
                        Hours = AddFieldValue(rowItem, "Hours"),
                        Shows = AddFieldValue(rowItem, "Shows"),
                        StartDate = Convert.ToDateTime(rowItem["StartDate"]),
                        EndDate = Convert.ToDateTime(rowItem["EndDate"]),
                        SentForApproval = Convert.ToBoolean(rowItem["SentForApproval"]),
                        JobApproved = Convert.ToBoolean(rowItem["JobApproved"]),
                        ApprovedBy = AddFieldValue(rowItem, "ApprovedBy"),
                        JobType = AddFieldValue(rowItem, "JobType"),
                        StewardUserId = Convert.ToInt32(rowItem["ItemCreatedBy"]),
                        StewardUserName = AddFieldValue(rowItem, "StewardUserName"),
                        StewardName = AddFieldValue(rowItem,"StewardName")

                    });
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        else
        {
            //LOG WARNING: DISPLAY NO DATA FOR REPORT. STEWARD MUST HAVE DELETED ALL JOBS FROM STEWARDREPORTS TABLE
        }

    }

    public void CreateStewardReport(DataSet reportDetailsDataSet)
    {

        if (reportDetailsDataSet.Tables.Count >= 1)
        {
            try
            {
                for (int i = 0; i < 1; i++)
                {
                    DataTable dt = reportDetailsDataSet.Tables[i];
                    if (dt.Rows.Count > 0)
                    {
                        DataRow rowItem = dt.Rows[i];

                        StewardReport = new StewardReport()
                        {
                            ReportId = Convert.ToInt32(rowItem["ItemID"]),
                            StewardUserId = Convert.ToInt32(rowItem["ItemCreatedBy"].ToString()),
                            StewardUserName = AddFieldValue(rowItem, "StewardUserName"),
                            Name = AddFieldValue(rowItem, "StewardName"),
                            Venue = AddFieldValue(rowItem, "Venue"),
                            WeekStarting = AddFieldValue(rowItem, "WeekStarting"),
                            Approved = Convert.ToBoolean(rowItem["Approved"]),
                            UnderApproval = Convert.ToBoolean(rowItem["UnderApproval"]),
                            Denied = Convert.ToBoolean(rowItem["Denied"].ToString()),
                            ApprovedBy = AddFieldValue(rowItem, "ApprovedBy"),
                            StartDate = Convert.ToDateTime(rowItem["StartDate"]),
                            EndDate = Convert.ToDateTime(rowItem["EndDate"]),
                            Status = AddFieldValue(rowItem, "Status"),
                            Empty = false

                        };
                    }
                    else
                    {
                        StewardReport = new StewardReport();
                        StewardReport.Empty = true;
                    }

                }

            }
            catch (Exception e)
            {

                throw e;
            }
        }
        else
        {
            //LOG WARNING: DISPLAY NO DATA FOR REPORT. STEWARD MUST HAVE DELETED ALL JOBS FROM STEWARDREPORTS TABLE
        }

    }

    public void SetControlValues(StewardReport stewardReport)
    {
        if (!stewardReport.Empty)
        {
            ltrStewardName.Text = StewardReport.Name;
            ltrWeekEnding.Text = $"{StewardReport.StartDate.ToShortDateString()} - {StewardReport.EndDate.ToShortDateString()}";
            ltrReportStatus.Text = $"{StewardReport.Status}";
            ltrVenue.Text = StewardReport.Venue;

            if (stewardReport.UnderApproval)
            {
                pnlUnderApprovalStatus.Visible = true;
                pnlDeniedStatus.Visible = false;
                pnlAprovedStatus.Visible = false;
                pnlApproveReport.Visible = true;
            }
            else if (stewardReport.Denied)
            {
                pnlUnderApprovalStatus.Visible = false;
                pnlDeniedStatus.Visible = true;
                pnlAprovedStatus.Visible = false;
                pnlApproveReport.Visible = true;
            }
            else
            {
                pnlAprovedStatus.Visible = true;
                pnlUnderApprovalStatus.Visible = false;
                pnlDeniedStatus.Visible = false;
                pnlApproveReport.Visible = false;
            }
        }
        else
        {

            pnlEmptyReport.Visible = true;
            ReportTable.Visible = false;
            pnlApproveReport.Visible = false;
            pnlStewardDetails.Visible = false;
            pnlEmptyReportReturn.Visible = true;

            pnlUnderApprovalStatus.Visible = false;
            pnlDeniedStatus.Visible = false;
            pnlAprovedStatus.Visible = false;           
          
        }
    }

    private string AddFieldValue(DataRow row,
                             string fieldName)
    {
        if (!DBNull.Value.Equals(row[fieldName]))
            return (string)row[fieldName] + "";
        else
            return String.Empty;
    }




    protected void ReturnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/");
    }
}




public enum ReportStatus
{
    Approved,
    UnderApproval,
    Denied
}

public class StewardJobs
{
    public int StewardUserId { get; set; }
    public string StewardUserName { get; set; }
    public string Name { get; set; }
    public string StewardName { get; set; }
    public string LocalOneUnionCardNumber { get; set; }
    public string UnionCardNumber { get; set; }
    public string JobType { get; set; }
    public string Position { get; set; }
    public string Hours { get; set; }
    public string Shows { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public bool SentForApproval { get; set; }
    public bool JobApproved { get; set; }
    public string ApprovedBy { get; set; }

    public StewardJobs()
    {
        StewardUserId = 0;
        StewardUserName = "";
        Name = "";
        StewardName = "";
        LocalOneUnionCardNumber = "";
        UnionCardNumber = "";
        JobType = "";
        Position = "";
        Hours = "";
        Shows = "";
        StartDate = DateTime.Now;
        EndDate = DateTime.Now;
        SentForApproval = false;
        JobApproved = true;
        ApprovedBy = "";
    }
}

public class StewardReport
{
    public int ReportId { get; set; }
    public int StewardUserId { get; set; }
    public string StewardUserName { get; set; }
    public string Name { get; set; }
    public string Venue { get; set; }
    public string WeekStarting { get; set; }
    public bool Approved { get; set; }
    public bool UnderApproval { get; set; }
    public bool Denied { get; set; }
    public string ApprovedBy { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public string Status { get; set; }
    public bool Empty { get; set; }

    public StewardReport()
    {
        StewardUserId = 0;
        Name = "";
        StewardUserName = "";
        Venue = "";
        WeekStarting = "";
        Approved = false;
        UnderApproval = false;
        Denied = false;
        ApprovedBy = "";
        StartDate = DateTime.Now;
        EndDate = DateTime.Now;
        Status = "";
        Empty = true;

    }

}
