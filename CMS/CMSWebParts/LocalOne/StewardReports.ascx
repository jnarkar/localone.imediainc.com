﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_LocalOne_StewardReports" CodeFile="~/CMSWebParts/LocalOne/StewardReports.ascx.cs" %>


<script src="~/CMSScripts/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.js"></script>
<link href="~/CMSScripts/EasyAutocomplete-1.3.5/easy-autocomplete.css" rel="stylesheet" />
<script type="text/javascript">
    /**
    AutoComplete
    */
    $(document).ready(function () {

        var jData = <%= jsonUsers %>;        


        var unionOptions = {
            data: jData,

            //listLocation: function (jData) {
            //    return jData;
            //},s

            getValue: function (jData) {
                return jData["FullName"];
            },

            list: {

                match: {
                    enabled: true
                },

                onSelectItemEvent: function () {
                    var value = $('.autocomplete-search-union').getSelectedItemData().UserName;
                    $(".union-member").val(value).trigger("change");
                },

            },

            placeholder: "Search for members"
        }

        $(".autocomplete-search-union").easyAutocomplete(unionOptions);

        var prodOptions = {
            data: jData,

            //listLocation: function (jData) {
            //    return jData;
            //},

            getValue: function (jData) {
                return jData["FullName"];
            },

            list: {

                match: {
                    enabled: true
                },

                onSelectItemEvent: function () {
                    var value = $('.autocomplete-search-production').getSelectedItemData().UserName;
                    $(".production-member").val(value).trigger("change");
                }
            },
            
            placeholder: "Search for members"
        }

        $(".autocomplete-search-production").easyAutocomplete(prodOptions);




    })

</script>

<asp:Panel runat="server" ID="pnlReportApprovalStatus" Visible="false">
    <h4>The report is under approval process, you cannot edit or remove job. </h4>
    <div style="display: none">
        <asp:TextBox runat="server" ID="txtReportDisable" CssClass="report-disable" Text="True"></asp:TextBox>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlReportApproved" Visible="false">
    <h4>Congratulations, your report is approved!</h4>
    <div style="display: none">
        <asp:TextBox runat="server" ID="TextBox1" CssClass="report-disable" Text="True"></asp:TextBox>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlReportDenied" Visible="false">
    <h4>Sorry, your reports are denied, you can edit report and send it back for approval.</h4>
</asp:Panel>


<asp:MultiView runat="server" ActiveViewIndex="0" ID="mvStewardRegistration">
    <asp:View ID="vRegistration" runat="server">
        <!-- Drop down list -->
        <div class="venue-top">
            <div class="line-wrap">
                <div class="venue-list-wrapper-stew">
                    <asp:DropDownList runat="server" ID="ddlWeek" OnSelectedIndexChanged="ddlWeek_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <asp:Panel CssClass="panel-add-copy" runat="server" ID="pnlAddNewJob">
                <div style="clear: both; display: inline-block">
                    <input type="button" id="add_new_job" data-modalid="#addNewJobModal" class="imedia-modal-popup" value="+ Add a New Job" />
                </div>
            </asp:Panel>
            <asp:Panel CssClass="panel-add-copy" runat="server" ID="pnlCopyJobs">
                <div style="clear: both; display: inline-block">
                    <input type="button" id="copy_from_prev_week" data-modalid="#copyJobModal" class="imedia-modal-popup" style="display: inline-block" value="Copy Jobs from a Selected Week" />
                </div>
            </asp:Panel>

            <!-- Venue and Steward details -->
            <div>
                <h3>
                    <asp:Literal runat="server" ID="ltrVenue"></asp:Literal>
                </h3>
                <div class="steward-info">
                    <div class="items">
                        <span class="labels">Steward:</span>
                        <span class="content">
                            <asp:Literal ID="ltrStewardName" runat="server" /></span><br />
                    </div>
                    <div class="items">
                        <span class="labels">Week Ending: </span>
                        <span class="content">
                            <asp:Literal ID="ltrWeekEnding" runat="server"></asp:Literal></span>
                    </div>
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlNoJobs" Visible="false">
                <div>
                    <p>No jobs have been added for this week.</p>
                    <p>Click + Add a New Job or Copy Jobs from Previous Week to start adding jobs to this week.</p>
                </div>
            </asp:Panel>

            <div class="jobs-wrapper">
                <div id="jobs-union-wrapper">
                    <h4>Union</h4>
                    <table class="tabledata union-jobs" id="data-table-union" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td>Local #</td>
                                <td>Union Card Number</td>
                                <td>Position</td>
                                <td>Hours</td>
                                <td>Shows</td>
                                <td>Edit</td>
                                <td>Remove</td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div id="jobs-production-wrapper">
                    <h4>Production</h4>
                    <table class="tabledata production-jobs" id="data-table-production" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td>Local #</td>
                                <td>Union Card Number</td>
                                <td>Position</td>
                                <td>Hours</td>
                                <td>Shows</td>
                                <td>Edit</td>
                                <td>Remove</td>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- Hidden Values -->
            <div style="display: none;">
                <asp:TextBox runat="server" CssClass="all-jobs" ID="txtJobs"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="selected-venue-type" ID="txtSelectedVenueType"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="selected-venue" ID="txtSelectedVenue"></asp:TextBox>
                <asp:Literal ID="ltrVenueTypes" runat="server" Visible="true" />
                <!-- Jobs related hidden fields -->
                <asp:TextBox runat="server" CssClass="all-jobs-union" ID="txtJobsUnion"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="all-jobs-production" ID="txtJobsProduction"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="select-week-jobs-exist" ID="txtNoJobsExistForReport"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="current-week-submitted" ID="txtCurrenWeekSubmitted"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="selected-week-editable" ID="txtJobsEditable"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="all-members" ID="txtMembers"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="union-member" ID="txtUnionMember"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="production-member" ID="txtProductionMember"></asp:TextBox>
                <%--<asp:TextBox runat="server" ID="txtVenue"></asp:TextBox>--%>
                <asp:HiddenField runat="server" ID="hdnVenue" />
            </div>
        </div>
        <!-- Submit Button -->
        <div>
            <div style="display: none;">
                Result:
                <asp:Literal ID="ltrResult" runat="server" />
            </div>

            <!-- Send for Approval Button -->
            <asp:Panel runat="server" ID="pnlSendReport">
                <div style="clear: both;">
                    <input type="button" id="send_report" data-modalid="#sendReportModal" class="imedia-modal-popup btn-send-report" value="Send Report" />
                </div>
            </asp:Panel>

        </div>
    </asp:View>
    <asp:View ID="vUnderApproval" runat="server">
        <div style="color: #ff6a00; font-weight: bold; padding-top: 25px;">
            Your approval request is under process, the result will be notified to you by email.
        </div>
    </asp:View>
    <asp:View ID="vStewardRoleWithIssues" runat="server">
        <div>
            You are with Steward Role but you are not listed under our Approval Process. Kindly contact an adminsitrator with your query.
        </div>
    </asp:View>
    <asp:View ID="vAuthorizeError" runat="server">
        <div>
            You are not authorize to view this page.
        </div>
    </asp:View>
    <asp:View ID="vGenericError" runat="server">
        <div style="color: red; font-weight: bold;">
            Sorry for inconvenience and we are workng this issue.
        </div>
    </asp:View>
    <asp:View ID="vSuccess" runat="server">
        <div>
            Thanks for your approval request, the request has been sent to an Administrator. You will be notfied by email.
        </div>
    </asp:View>
</asp:MultiView>


<!-- Add New Job Modal -->
<div class="modal fade" id="addNewJobModal" tabindex="-1" role="dialog"
    aria-labelledby="addNewJobModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addNewJobModalLabel">Add Job</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="add_job_tabs">
                    <li role="presentation" class="active">
                        <a href="#add_job_Union" aria-controls="add_job_Union" role="tab" data-toggle="tab">Union Members</a>
                    </li>
                    <li role="presentation">
                        <a href="#add_job_Production" aria-controls="add_job_Production" role="tab" data-toggle="tab">Production Crew</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content" id="add_new_job_modal">
                    <div role="tabpanel" class="tab-pane active" id="add_job_Union">
                        <div>
                            <div class="form-group autocomplete-wrapper">
                                <label for="ddlMembersUnion">Name: *</label>
                                <asp:TextBox runat="server" ID="txtAutoCompleteUnion" CssClass="autocomplete-search autocomplete-search-union txt_union_member form-control"></asp:TextBox>
                                <span class="error-label hide">This is required field</span>
                            </div>

                            <div class="form-group">
                                <label for="txtUnionCardNumber">Union Card Number: *</label>
                                <asp:TextBox runat="server" ID="txtUnionCardNumber" CssClass="form-control text-required job_union txt_card_number"></asp:TextBox>
                                <span class="error-label hide">This is required field</span>
                            </div>
                            <div class="form-group">
                                <label for="txtLocalUnionNumber">Local Union Number: *</label>
                                <asp:TextBox runat="server" ID="txtUnionLocalUnionNumber" CssClass="form-control text-required job_union txt_local_union_number"></asp:TextBox>
                                <span class="error-label hide">This is required field</span>
                            </div>
                 			<div class="form-group venue-list-wrapper-modal">
                                <label for="txtUnionPosition">Position: *</label>
                                <asp:DropDownList runat="server" CssClass="job_union ddl_position select-required" ID="ddlUnionPosition">
                                    <asp:ListItem Value="-1">Select Position</asp:ListItem>
                                </asp:DropDownList>
                                <span class="error-label hide">This is required field</span>
                            </div>
                            <div class="form-group">
                                <label for="txtUnionHours">Hours: *</label>
                                <asp:TextBox runat="server" ID="txtUnionHours" CssClass="form-control text-required job_union txt_hours"></asp:TextBox>
                                <span class="error-label hide">This is required field</span>
                            </div>
                            <div class="form-group">
                                <label for="txtUnionShows">Shows: *</label>
                                <asp:TextBox runat="server" ID="txtUnionShows" CssClass="form-control text-required job_union txt_shows"></asp:TextBox>
                                <span class="error-label hide">This is required field</span>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnUnionAddNewJob" runat="server" Text="Save Job" CssClass="save-job-btn form-validation" data-parentid="#add_job_Union" OnClick="btnUnionAddNewJob_Click" />
                            <a href="#a" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="add_job_Production">
                        <div>
                            <div class="form-group autocomplete-wrapper">
                                <label for="ddlMembersUnion">Name: *</label>
                                <asp:TextBox runat="server" ID="txtAutoCompleteProduction" CssClass="autocomplete-search autocomplete-search-production txt_production_member form-control"></asp:TextBox>
                                <span class="error-label hide">This is required field</span>
                            </div>
                            <div class="form-group">
                                <label for="txtProductionCardNumber">Union Card Number: *</label>
                                <asp:TextBox runat="server" ID="txtProductionCardNumber" CssClass="form-control text-required job_prod txt_card_number"></asp:TextBox>
                                <span class="error-label hide">This is required field</span>
                            </div>
                            <div class="form-group">
                                <label for="txtProductionLocalUnionNumber">Local Union Number: *</label>
                                <asp:TextBox runat="server" ID="txtProductionLocalUnionNumber" CssClass="form-control text-required job_prod txt_local_union_number"></asp:TextBox>
                                <span class="error-label hide">This is required field</span>
                            </div>
				
						    <div class="form-group venue-list-wrapper-modal">
                                <label for="txtProductionPosition">Position: *</label>
                                <asp:DropDownList runat="server" CssClass="venue-list-wrapper-modal job_prod ddl_position select-required" ID="ddlProductionPosition">
                                    <asp:ListItem Value="-1">Select Position</asp:ListItem>
                                </asp:DropDownList>
                                <span class="error-label hide">This is required field</span>
                            </div>
                            <div class="form-group">
                                <label for="txtProductionHours">Hours: *</label>
                                <asp:TextBox runat="server" ID="txtProductionHours" CssClass="form-control text-required job_prod txt_hours"></asp:TextBox>
                                <span class="error-label hide">This is required field</span>
                            </div>
                            <div class="form-group">
                                <label for="txtProductionShows">Shows: *</label>
                                <asp:TextBox runat="server" ID="txtProductionShows" CssClass="form-control text-required job_prod txt_shows"></asp:TextBox>
                                <span class="error-label hide">This is required field</span>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnProductionAddNewJob" runat="server" Text="Save Job" CssClass="save-job-btn form-validation" data-parentid="#add_job_Production" OnClick="btnProductionAddNewJob_Click" />
                            <a href="#a" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
                <div style="display: none;">
                    <asp:TextBox runat="server" ID="txtJobIdToEdit" CssClass="item-guid"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Remove Job Modal -->
<div class="modal fade" id="removeJobModal" tabindex="-1" role="dialog"
    aria-labelledby="removeJobModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="removeJobModalLabel">Delete Job</h4>
            </div>
            <div class="modal-body">
                <div>
                    Are you sure you want to delete this job?
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnRemoveJob" runat="server" Text="Yes" CssClass="remove-job-btn-yes" OnClick="btnRemoveJob_Click" />
                <a href="#a" data-dismiss="modal">Cancel</a>
                <div style="display: none;">
                    <asp:TextBox runat="server" ID="txtJobIdToRemove" CssClass="item-guid"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Copy Job Modal -->
<div class="modal fade" id="copyJobModal" tabindex="-1" role="dialog"
    aria-labelledby="copyJobModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="copyJobModalLabel">Copy Jobs</h4>
            </div>
            <div class="modal-body">		
				<div>
                    <asp:RadioButtonList runat="server" ID="radWeek" AutoPostBack="false">
                    </asp:RadioButtonList>
                </div>			
                <div>
                    Are you sure you want to copy all the jobs and insert them into the selected week?
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button runat="server" ID="btnCopyJobs" Text="Yes" Visible="false" CssClass="btn-copy-jobs" OnClick="btnCopyJobs_Click" />
                <a href="#a" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>

<!-- Send Report Modal -->
<div class="modal fade" id="sendReportModal" tabindex="-1" role="dialog"
    aria-labelledby="sendReportModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="sendReportModalLabel">Send Report</h4>
            </div>
            <div class="modal-body">
                <div style="clear: both;">
                    <label for="txtNotes">Notes</label><br />
                    <asp:TextBox runat="server" ID="txtNotes" TextMode="MultiLine" Rows="5"></asp:TextBox>
                </div>
                <div>
                    This is the one time action which cannot be undone, would you still like to send a report?
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button runat="server" ID="btnSendReport" CssClass="btn-send-report" Text="Yes" OnClick="btnSendReport_Click" />
                <a href="#a" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>


<style>

    .autocomplete-wrapper{
        position: relative;
        width: 60%;
    }
    .autocomplete-search {
        width: 100% !important;
    }

    .easy-autocomplete {
        width: 100% !important;
    }

    .easy-autocomplete-container ul {
        padding-left: 0 !important;
    }

        .easy-autocomplete-container ul li {
            margin-bottom: 0 !important;
            padding-bottom: 12px !important;
        }

    .easy-autocomplete-container li::before {
        display: none !important;
    }

    .panel-add-copy {
        display: inline-block;
    }

    .dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_paginate {
        display: none;
    }

    .dataTable td.dataTables_empty {
        color: #5a6164;
        font-weight: bold;
        text-align: center;
        padding: 25px 0;
        font-size: 25px;
    }

    .form-horizontal .form-group {
        margin: 0;
    }

    .form-control {
        width: auto;
    }

    .registration {
        background-color: #fff;
    }

    .inline {
        display: inline-block;
        vertical-align: top;
    }

    .modal input[type=checkbox], .modal input[type=radio] {
        height: auto;
    }

    a {
        color: #337ab7;
    }

        a:hover {
            color: #337ab7;
            text-decoration: underline;
        }

    input[type=text], input[type=password] {
        border: 1px solid #000;
    }

    .item {
        display: inline-block;
        border: 1px solid #000;
        padding: 15px;
    }

    input[type=image] {
        height: auto;
        padding: 0;
        position: absolute;
        right: -12px;
        top: -12px;
    }

    .delete-btn-container {
        position: relative;
    }

    .form-section {
        padding: 20px 0;
    }

    .tab {
        width: 200px;
        background-color: #fff;
        color: #204d74;
    }

    .screen1 {
        border-radius: 50px 0px 0px 50px;
        border-right: none;
    }

    .screen2 {
        border-radius: 0;
    }

    .screen3 {
        border-radius: 0 50px 50px 0;
        border-left: none;
    }

    .nav-bar {
        padding-top: 20px;
        font-size: 0;
    }

    .active-tab {
        background-color: #204d74;
        color: #fff;
    }

    .error-labels {
        padding-bottom: 20px;
    }

    .job-modal-error, .info-modal-error {
        color: red;
    }
</style>
