using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;
using CMS.Membership;
using CMS.PortalEngine;
using CMS.SiteProvider;
using CMS.Protection;
using System.Collections.Generic;
using CMS.Base.Web.UI;
using CMS.CustomTables;
using CMS.DataEngine;
using System.Web.Services;
using System.Linq;
using System.Globalization;
using CMS.EventLog;
using CMS.Base;

public partial class CMSWebParts_LocalOne_LocalOneMyProfile : CMSAbstractWebPart
{

    [Serializable]
    public class JobExperience
    {
        public int ID { get; set; }
        public string VenueType { get; set; }
        public string VenueTypeID { get; set; }
        public string Position { get; set; }
        public string Venue { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool CurrentPosition { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Action { get; set; }
        public int JobVenueID { get; set; }
        public int JobPositionID { get; set; }


    }
    [Serializable]
    public class JobValuePair
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    List<JobExperience> globalJobExperience = new List<JobExperience>();

    #region "Properties"



    #endregion


    #region "Methods"


    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();

    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {



        if (AuthenticationHelper.IsAuthenticated())
        {

            //Get User Info & Initialize controls    
            var currentUser = MembershipContext.AuthenticatedUser;

            UserInfo userInfo = UserInfoProvider.GetUserInfo(currentUser.UserID);
            List<JobExperience> userJobExp = new List<JobExperience>();

            //Source of Repeater must come from the database, after postback. 

            ViewState["EditMode"] = false;
            ViewState["DeleteMode"] = false;
            ViewState["SkillsList"] = new List<JobValuePair>(0);
            ViewState["LicenseList"] = new List<JobValuePair>(0);
            ViewState["JobExpList"] = new List<JobExperience>(0);

            string skillsCustomTable = "customtable.JobSkills";
            string licenseCustomTable = "customtable.LicenseAndCerts";
            string positionsCustomTable = "customtable.JobPositions";
            string venuesCustomTable = "customtable.JobVenues";
            string venueTypeCustomTable = "customtable.JobVenueTypes";

            DataClassInfo skillsDataClassInfo = DataClassInfoProvider.GetDataClassInfo(skillsCustomTable);
            DataClassInfo licenseDataClassInfo = DataClassInfoProvider.GetDataClassInfo(licenseCustomTable);
            DataClassInfo positionsDataClassInfo = DataClassInfoProvider.GetDataClassInfo(positionsCustomTable);
            DataClassInfo venuesDataClassInfo = DataClassInfoProvider.GetDataClassInfo(venuesCustomTable);
            DataClassInfo venueTypeDataClassInfo = DataClassInfoProvider.GetDataClassInfo(venueTypeCustomTable);

            List<JobValuePair> skillsPairList = new List<JobValuePair>(0);
            List<JobValuePair> licensePairList = new List<JobValuePair>(0);
            List<JobValuePair> positionsPairList = new List<JobValuePair>(0);
            List<JobValuePair> venuesPairList = new List<JobValuePair>(0);
            List<JobValuePair> venueTypePairList = new List<JobValuePair>(0);


            foreach (var skill in CustomTableItemProvider.GetItems(skillsCustomTable))
            {
                skillsPairList.Add(new JobValuePair
                {
                    Text = ValidationHelper.GetString(skill.GetValue("Skill"), ""),
                    Value = ValidationHelper.GetString(skill.GetValue("ItemID"), "")
                });
            }

            foreach (var license in CustomTableItemProvider.GetItems(licenseCustomTable))
            {
                licensePairList.Add(new JobValuePair
                {
                    Text = ValidationHelper.GetString(license.GetValue("Name"), ""),
                    Value = ValidationHelper.GetString(license.GetValue("ItemID"), "")
                });
            }

            foreach (var position in CustomTableItemProvider.GetItems(positionsCustomTable))
            {
                positionsPairList.Add(new JobValuePair
                {
                    Text = ValidationHelper.GetString(position.GetValue("Position"), ""),
                    Value = ValidationHelper.GetString(position.GetValue("ItemID"), "")
                });
            }

            foreach (var venue in CustomTableItemProvider.GetItems(venuesCustomTable))
            {
                venuesPairList.Add(new JobValuePair
                {
                    Text = ValidationHelper.GetString(venue.GetValue("Venue"), ""),
                    Value = ValidationHelper.GetString(venue.GetValue("ItemID"), "")
                });
            }

            foreach (var venue in CustomTableItemProvider.GetItems(venueTypeCustomTable))
            {
                venueTypePairList.Add(new JobValuePair
                {
                    Text = ValidationHelper.GetString(venue.GetValue("Type"), ""),
                    Value = ValidationHelper.GetString(venue.GetValue("ItemID"), "")
                });
            }

            ccblSkills.DataSource = skillsPairList;
            ccblSkills.DataBind();

            //Sets Job Skills Selected CheckBox & Placeholder
            string[] userJobSkillIds = GetUserJobSKillIDs(userInfo);
            if (!ExtensionMethod.IsNullOrEmpty(userJobSkillIds))
            {
                foreach (var id in userJobSkillIds)
                {
                    ccblSkills.Items.FindByValue(id).Selected = true;
                }
                List<JobValuePair> SkillsList = new List<JobValuePair>(0);

                var selectedItems = ccblSkills.GetSelectedItems();
                if (selectedItems != null)
                {
                    foreach (var skill in selectedItems)
                    {
                        SkillsList.Add(new JobValuePair { Text = skill.Text, Value = skill.Value });
                    }
                }

                if (SkillsList.Count > 0)
                {
                    clbAddSkillsInitial.Text = "Edit";
                }
                else
                {
                    clbAddSkillsInitial.Text = "Add Job Skills";
                }

                ViewState["SkillsList"] = SkillsList;
                crepJobSkillsInitial.DataSource = SkillsList;
                crepJobSkillsInitial.DataBind();
            }


            ccblLicenses.DataSource = licensePairList;
            ccblLicenses.DataBind();

            //Sets Job Skills Selected CheckBox & Placeholder
            string[] userLicensesCertsIds = GetUserLicensesIDs(userInfo);
            if (!ExtensionMethod.IsNullOrEmpty(userLicensesCertsIds))
            {
                foreach (var id in userLicensesCertsIds)
                {
                    ccblLicenses.Items.FindByValue(id).Selected = true;
                }


                List<JobValuePair> LicenseList = new List<JobValuePair>(0);

                var selectedItems = ccblLicenses.GetSelectedItems();
                if (selectedItems != null)
                {
                    foreach (var license in selectedItems)
                    {
                        LicenseList.Add(new JobValuePair { Text = license.Text, Value = license.Value });
                    }
                }

                if (LicenseList.Count > 0)
                {
                    clbAddLicensesInitial.Text = "Edit";
                }
                else
                {
                    clbAddLicensesInitial.Text = "Add Licenses & Certifications";
                }
                ViewState["LicenseList"] = LicenseList;
                crepLicensesInitial.DataSource = LicenseList;
                crepLicensesInitial.DataBind();

            }

            positionsPairList.Insert(0, new JobValuePair { Text = "", Value = "" });
            cddlJobPositions.DataSource = positionsPairList;
            cddlJobPositions.DataBind();



            venuesPairList.Insert(0, new JobValuePair { Text = "", Value = "" });
            cddlJobVenues.DataSource = venuesPairList;
            cddlJobVenues.DataBind();

            crblVenueTypes.DataSource = venueTypePairList;
            crblVenueTypes.DataBind();


            //Display List of User Jobs If Any Exist for Current User
            userJobExp = GetJobExperiences(currentUser.UserID);
            if (userJobExp != null)
            {
                ViewState["JobExpList"] = userJobExp;
                crepJobsInitial.DataSource = userJobExp;
                crepJobsInitial.DataBind();
            }


            //Check if User has any JobSkillIDs. If so, must use values to update controls, and update VIEWSTATE["SkillsLIst"]


            var userGender = (int)userInfo.UserSettings.GetValue("UserGender");

            ctbUnionCardNumber.Text = userInfo.GetValue("UnionCardNumber", "").ToString();
            ctbFirstName.Text = userInfo.GetValue("FirstName", "").ToString();
            ctbLastName.Text = userInfo.GetValue("LastName", "").ToString();
            ctbStreetAddress.Text = userInfo.GetValue("StreetAddress", "").ToString();
            ctbCity.Text = userInfo.GetValue("City", "").ToString();
            ctbState.Text = userInfo.GetValue("State", "").ToString();
            ctbZipCode.Text = userInfo.GetValue("ZipCode", "").ToString();
            ctbCellNumber.Text = userInfo.GetValue("CellNumber", "").ToString();
            ctbHomeNumber.Text = userInfo.GetValue("HomeNumber", "").ToString();
            ctlUserName.Text = userInfo.GetValue("UserName", "").ToString();
            ctbEmailInput.Text = userInfo.GetValue("Email", "").ToString();
            ctbNotificationEmail.Text = userInfo.UserSettings.GetValue("UserMessagingNotificationEmail", "").ToString();
            //ddlTimeZone.TimeZoneID = (int)userInfo.UserSettings.GetValue("UserTimeZoneID", 0);
            //ctbDateOfBirth.Text = userInfo.UserSettings.GetValue("UserDateOfBirth","").ToString();
            if (userGender > 0)
            {
                rbGender.SelectedValue = userGender.ToString();
            }

        }
        else
        {
            Visible = false;
        }


    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion


    protected void cbUpdateProfile_Click(object sender, EventArgs e)
    {
        if (PortalContext.IsDesignMode(PortalContext.ViewMode) || (HideOnCurrentPage) || (!IsVisible))
        {
            // Do not process
        }
        else
        {
            String siteName = SiteContext.CurrentSiteName;
            string[] siteList = { siteName };

            var currentUser = MembershipContext.AuthenticatedUser;

            UserInfo userInfo = UserInfoProvider.GetUserInfo(currentUser.UserID);

            #region "Banned IPs"

            // Ban IP addresses which are blocked for registration
            if (!BannedIPInfoProvider.IsAllowed(siteName, BanControlEnum.Registration))
            {
                ShowError(GetString("banip.ipisbannedregistration"));
                return;
            }

            #endregion


            #region "Update User Properties"
            //Validate Data Types
            //// Especially DOB



            ////Update Skill & License List
            //List<JobValuePair> SkillsList = (List<JobValuePair>)ViewState["SkillsList"];
            //string skills = "";
            //if (SkillsList != null && SkillsList.Any())
            //    skills = string.Join(",", SkillsList.Select(x => x.Value));


            //List<JobValuePair> LicenseList = (List<JobValuePair>)ViewState["LicenseList"];
            //string licenses = "";
            //if (LicenseList != null && LicenseList.Any())
            //    licenses = string.Join(",", LicenseList.Select(x => x.Value));    

            string skills = "";
            var selectedSkills = ccblSkills.GetSelectedItems();
            if (selectedSkills != null)
            {
                skills = string.Join(",", selectedSkills.Select(x => x.Value));

            }

            string skillsText = "";
            var selectedSkillsText = ccblSkills.GetSelectedItems();
            if (selectedSkillsText != null)
            {
                skillsText = string.Join(",", selectedSkillsText.Select(x => x.Text));

            }



            string licenses = "";
            var selectedLicenses = ccblLicenses.GetSelectedItems();
            if (selectedLicenses != null)
            {
                licenses = string.Join(",", selectedLicenses.Select(x => x.Value));
            }

            string licensesText = "";
            var selectedLicensesText = ccblLicenses.GetSelectedItems();
            if (selectedLicensesText != null)
            {
                licensesText = string.Join(",", selectedLicensesText.Select(x => x.Text));
            }



            string unionCard = ValidationHelper.GetString(ctbUnionCardNumber.Text.Trim(), null, CultureInfo.CurrentCulture);
            string firstName = ValidationHelper.GetString(ctbFirstName.Text.Trim(), null, CultureInfo.CurrentCulture);
            string lastName = ValidationHelper.GetString(ctbLastName.Text.Trim(), null, CultureInfo.CurrentCulture);
            string streetAddress = ValidationHelper.GetString(ctbStreetAddress.Text.Trim(), null, CultureInfo.CurrentCulture);
            string city = ValidationHelper.GetString(ctbCity.Text.Trim(), null, CultureInfo.CurrentCulture);
            string state = ValidationHelper.GetString(ctbFirstName.Text.Trim(), null, CultureInfo.CurrentCulture);
            string zipCode = ValidationHelper.GetString(ctbZipCode.Text.Trim(), null, CultureInfo.CurrentCulture);
            string cellNumber = ValidationHelper.GetString(ctbCellNumber.Text.Trim(), null, CultureInfo.CurrentCulture);
            string homeNumber = ValidationHelper.GetString(ctbHomeNumber.Text.Trim(), null, CultureInfo.CurrentCulture);
            string email = ValidationHelper.GetString(ctbEmailInput.Text.Trim(), null, CultureInfo.CurrentCulture);
            string notificationEmail = ValidationHelper.GetString(ctbNotificationEmail.Text.Trim(), null, CultureInfo.CurrentCulture);
            //int timeZoneID = ValidationHelper.GetInteger(ddlTimeZone.TimeZoneID, 1, CultureInfo.CurrentCulture);
            int timeZoneID = 0;
            //string dateOfBirth = ValidationHelper.GetString(ctbDateOfBirth.Text.Trim(), null, CultureInfo.CurrentCulture);

            //DateTime dob = DateTime.ParseExact(dateOfBirth, "YYYY-MM-DD hh:mm:ss", 
            //        System.Globalization.CultureInfo.InvariantCulture);



            if (timeZoneID == 0)
            {
                timeZoneID = 22;
            }
            userInfo.SetValue("UnionCardNumber", unionCard);
            userInfo.SetValue("FirstName", firstName);
            userInfo.SetValue("LastName", lastName);
            userInfo.SetValue("StreetAddress", streetAddress);
            userInfo.SetValue("City", city);
            userInfo.SetValue("State", state);
            userInfo.SetValue("ZipCode", zipCode);
            userInfo.SetValue("CellNumber", cellNumber);
            userInfo.SetValue("HomeNumber", homeNumber);
            userInfo.SetValue("Email", email);
            userInfo.SetValue("JobSkillIds", skills);
            userInfo.SetValue("JobSkillsListText", skillsText);
            userInfo.SetValue("LicenseAndCertIds", licenses);
            userInfo.SetValue("LicenseCertListText", licensesText);
            userInfo.UserSettings.SetValue("UserMessagingNotificationEmail", notificationEmail);
            userInfo.UserSettings.SetValue("UserTimeZoneID", timeZoneID);

            //userInfo.UserSettings.SetValue("UserDateOfBirth", dob);         
            
            userInfo.Update();

            // User has been created, aCreate Job
            AddUpdateJobExperiences();

           
            #endregion

        }
    }

    protected void clbAddJobVenue_Click(object sender, EventArgs e)
    {
        cPanelVenue1.Visible = false;
        cPanelVenue2.Visible = true;


    }

    protected void cbAddCustomVenue_Click(object sender, EventArgs e)
    {
        cPanelVenue1.Visible = true;
        cPanelVenue2.Visible = false;

        cddlJobVenues.Items.Insert(1, ctbAddJobVenue.Text.Trim());
        cddlJobVenues.SelectedIndex = 1;
    }

    protected void cbCancelCustomVenue_Click(object sender, EventArgs e)
    {
        cPanelVenue1.Visible = true;
        cPanelVenue2.Visible = false;
    }

    protected void clbAddJobPosition_Click(object sender, EventArgs e)
    {
        cPanelJobPositions1.Visible = false;
        cPanelJobPositions2.Visible = true;
    }

    protected void cbAddCustomPosition_Click(object sender, EventArgs e)
    {
        cPanelJobPositions1.Visible = true;
        cPanelJobPositions2.Visible = false;

        cddlJobPositions.Items.Insert(1, ctbAddJobPosition.Text.Trim());
        cddlJobPositions.SelectedIndex = 1;
    }

    protected void cbCancelCustomPosition_Click(object sender, EventArgs e)
    {
        cPanelJobPositions1.Visible = true;
        cPanelJobPositions2.Visible = false;
    }

    

    //Will update the Selected Job Skills displayed
    protected void cbSaveJobSkills_Click(object sender, EventArgs e)
    {
        var currentUser = MembershipContext.AuthenticatedUser;

        UserInfo userInfo = UserInfoProvider.GetUserInfo(currentUser.UserID);
        List<JobValuePair> SkillsList = new List<JobValuePair>(0);

        var selectedItems = ccblSkills.GetSelectedItems();
        if (selectedItems != null)
        {
            foreach (var skill in selectedItems)
            {
                SkillsList.Add(new JobValuePair { Text = skill.Text, Value = skill.Value });
            }
        }

        if (SkillsList.Count > 0)
        {
            clbAddSkillsInitial.Text = "Edit";
            //clbAddSkillsFinal.Text = "Edit";
        }
        else
        {
            clbAddSkillsInitial.Text = "Add Job Skills";
            //clbAddSkillsFinal.Text = "Add Job Skills";
        }
       

        ViewState["SkillsList"] = SkillsList;
        string skills = "";
        var selectedSkills = ccblSkills.GetSelectedItems();
        if (selectedSkills != null)
        {
            skills = string.Join(",", selectedSkills.Select(x => x.Value));

        }


        string skillsText = "";
        var selectedSkillsText = ccblSkills.GetSelectedItems();
        if (selectedSkillsText != null)
        {
            skillsText = string.Join(",", selectedSkillsText.Select(x => x.Text));

        }

        userInfo.SetValue("JobSkillIds", skills);
        userInfo.SetValue("JobSkillsListText", skillsText);
        userInfo.Update();
        crepJobSkillsInitial.DataSource = SkillsList;
        crepJobSkillsInitial.DataBind();


    }

    protected void cbSaveLicenses_Click(object sender, EventArgs e)
    {
        var currentUser = MembershipContext.AuthenticatedUser;
        UserInfo userInfo = UserInfoProvider.GetUserInfo(currentUser.UserID);
        List<JobValuePair> SkillsList = new List<JobValuePair>(0);
        List<JobValuePair> LicenseList = new List<JobValuePair>(0);


        var selectedItems = ccblLicenses.GetSelectedItems();
        if (selectedItems != null)
        {
            foreach (var license in selectedItems)
            {
                LicenseList.Add(new JobValuePair { Text = license.Text, Value = license.Value });
            }
        }

        if (LicenseList.Count > 0)
        {
            clbAddLicensesInitial.Text = "Edit";
            //clbAddLicensesFinal.Text = "Edit";
        }
        else
        {
            clbAddLicensesInitial.Text = "Add Licenses & Certifications";
            //clbAddLicensesFinal.Text = "Add Licenses & Certifications";
        }

        ViewState["LicenseList"] = LicenseList;

        string licenses = "";
        var selectedLicenses = ccblLicenses.GetSelectedItems();
        if (selectedLicenses != null)
        {
            licenses = string.Join(",", selectedLicenses.Select(x => x.Value));
        }

        string licensesText = "";
        var selectedLicensesText = ccblLicenses.GetSelectedItems();
        if (selectedLicensesText != null)
        {
            licensesText = string.Join(",", selectedLicensesText.Select(x => x.Text));
        }
        userInfo.SetValue("LicenseAndCertIds", licenses);
        userInfo.SetValue("LicenseCertListText", licensesText);
        userInfo.Update();

        crepLicensesInitial.DataSource = LicenseList;
        crepLicensesInitial.DataBind();

    }

    protected void cbSaveJob_Click(object sender, EventArgs e)
    {
        List<JobExperience> SavedJobs = (List<JobExperience>)ViewState["JobExpList"];
        //var currentUser = MembershipContext.AuthenticatedUser;

        //UserInfo userInfo = UserInfoProvider.GetUserInfo(currentUser.UserID);
        //List<JobExperience> SavedJobs = LoadJobExperiences(userInfo);

        string status = "";

        if (ccbCurrentPosition.Checked)
            status = "Current";
        else
            status = ctbJobEndDate.Text;

        //DateTime? dtEnd;
        //if (ctbJobEndDate.Text == "")
        //    dtEnd = null;
        //else
        //    dtEnd = Convert.ToDateTime(ctbJobEndDate.Text);

        bool bEditMode = (bool)ViewState["EditMode"];
        if (bEditMode)
        {
            int? ID = Convert.ToInt32(chfJobExp.Value);
            if (ID != null && SavedJobs != null && SavedJobs.Count > 0)
            {
                JobExperience job = SavedJobs[Convert.ToInt32(ID)];
                if (job != null)
                {
                    job.VenueType = crblVenueTypes.SelectedItem.Text;
                    job.VenueTypeID = crblVenueTypes.SelectedValue;
                    job.Position = cddlJobPositions.SelectedItem.Text;
                    job.Venue = cddlJobVenues.SelectedItem.Text;
                    job.StartDate = ctbJobStartDate.Text.Trim();
                    job.EndDate = ctbJobEndDate.Text.Trim();
                    job.CurrentPosition = ccbCurrentPosition.Checked;
                    job.Description = ctbJobDescription.Text.Trim();
                    job.Status = status;
                    job.JobVenueID = cddlJobVenues.SelectedValue.ToInteger(0);
                    job.JobPositionID = cddlJobPositions.SelectedValue.ToInteger(0);
                    if (job.ID == 0)
                    {
                        job.Action = "Add";

                    }
                    else
                    {
                        job.Action = "Update";
                    }


                    ViewState["JobExpList"] = SavedJobs;
                    crepJobsInitial.DataSource = SavedJobs;
                    crepJobsInitial.DataBind();

                    ViewState["EditMode"] = false;
                    AddUpdateJobExperiences();
                }

            }

        }
        else
        {
            List<JobExperience> JobExpList = new List<JobExperience>(0);

            if (SavedJobs != null && SavedJobs.Count > 0)
            {
                foreach (JobExperience job in SavedJobs)
                {
                    JobExpList.Add(job);
                }
            }

            JobExpList.Add(new JobExperience
            {

                VenueType = crblVenueTypes.SelectedItem.Text,
                VenueTypeID = crblVenueTypes.SelectedValue,
                Position = cddlJobPositions.SelectedItem.Text,
                Venue = cddlJobVenues.SelectedItem.Text,
                StartDate = ctbJobStartDate.Text.Trim(),
                EndDate = ctbJobEndDate.Text.Trim(),
                CurrentPosition = ccbCurrentPosition.Checked,
                Description = ctbJobDescription.Text.Trim(),
                Status = status,
                JobVenueID = cddlJobVenues.SelectedValue.ToInteger(0),
                JobPositionID = cddlJobPositions.SelectedValue.ToInteger(0),
                 Action = "Add"
            });
            ViewState["JobExpList"] = JobExpList;

            crepJobsInitial.DataSource = JobExpList;
            crepJobsInitial.DataBind();

            AddUpdateJobExperiences();

        }



    }

    protected void crepJobsInitial_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
    {

        List<JobExperience> JobExpList = (List<JobExperience>)ViewState["JobExpList"];
        if (JobExpList != null && JobExpList.Count > 0)
        {
            int ID = Convert.ToInt32(e.CommandArgument);
            JobExperience job = JobExpList[ID];

            var currentPosition = cddlJobPositions.Items.FindByText(job.Position);


            //var currentVenue = cddlJobVenues.Items.FindByText(job.Venue);

            var currentVenue = cddlJobVenues.Items.FindByValue(job.JobVenueID.ToString());
            var currentJobPosition = cddlJobPositions.Items.FindByValue(job.JobPositionID.ToString());


            if (job != null)
            {
                if (e.CommandName == "edit")
                {

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Open Jobs Initial", "openJobsInitial();", true);
                    char[] charsToTrim = { ' ' };

                    crblVenueTypes.SelectedValue = job.VenueTypeID;
                    cddlJobPositions.SelectedValue = currentPosition.Value;
                    cddlJobVenues.SelectedValue = currentVenue.Value;
                    ctbJobStartDate.Text = job.StartDate.Trim(charsToTrim);
                    ctbJobEndDate.Text = job.EndDate.Trim(charsToTrim);
                    ccbCurrentPosition.Checked = job.CurrentPosition;
                    ctbJobDescription.Text = job.Description;
                    chfJobExp.Value = ID.ToString();

                    ViewState["EditMode"] = true;


                }
                else if (e.CommandName == "delete")
                {
                    bool deleted = DeleteJobExperience(job.ID);
                    if (deleted)
                    {
                        JobExpList.Remove(job);
                        ViewState["JobExpList"] = JobExpList;
                        crepJobsInitial.DataSource = JobExpList;
                        crepJobsInitial.DataBind();
                    }

                }
            }

        }
    }

    protected void clbEditInfoFinal_Click(object sender, EventArgs e)
    {
        //ctbFirstNameModal.Text = ctbFirstName.Text.Trim();
    }

    protected void clbAddJobInitial_Click(object sender, EventArgs e)
    {
        crblVenueTypes.ClearSelection();
        cddlJobPositions.ClearSelection();
        cddlJobVenues.ClearSelection();
        ctbAddJobPosition.Text = "";
        ctbAddJobVenue.Text = "";
        ctbJobStartDate.Text = "";
        ctbJobEndDate.Text = "";
        ccbCurrentPosition.Checked = false;
        ctbJobDescription.Text = "";
    }

    protected void clbAddJobFinal_Click(object sender, EventArgs e)
    {


        crblVenueTypes.ClearSelection();
        cddlJobPositions.SelectedIndex = 0;
        cddlJobVenues.ClearSelection();
        ctbAddJobPosition.Text = "";
        ctbAddJobVenue.Text = "";
        ctbJobStartDate.Text = "";
        ctbJobEndDate.Text = "";
        ccbCurrentPosition.Checked = false;
        ctbJobDescription.Text = "";
    }

    protected string[] GetUserJobSKillIDs(UserInfo userInfo)
    {
        string userSkillIds = userInfo.GetValue("JobSkillIds", "").ToString();

        string[] splitUserSkillIds = null;

        if (userSkillIds != "" || !String.IsNullOrEmpty(userSkillIds))
        {
            splitUserSkillIds = userSkillIds.Split(',');

            foreach (var id in splitUserSkillIds)
            {
                id.Trim();
            }

            return splitUserSkillIds;
        }

        return splitUserSkillIds;
    }

    protected string[] GetUserLicensesIDs(UserInfo userInfo)
    {
        string userLicenseAndCertIds = userInfo.GetValue("LicenseAndCertIds", "").ToString();

        string[] splitLicenseAndCertIds = null;

        if (userLicenseAndCertIds != "")
        {
            splitLicenseAndCertIds = userLicenseAndCertIds.Split(',');

            foreach (var id in splitLicenseAndCertIds)
            {
                id.Trim();
            }

            return splitLicenseAndCertIds;
        }

        return splitLicenseAndCertIds;
    }

    protected void CreateJobExperience(UserInfo userInfo)
    {
        List<JobExperience> JobExpList = (List<JobExperience>)ViewState["JobExpList"];
        if (JobExpList != null && JobExpList.Any())
        {
            string jobExperienceCustomTable = "customtable.MemberDirJobExp";
            DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(jobExperienceCustomTable);
            if (customTable != null)
            {
                foreach (JobExperience job in JobExpList)
                {
                    CustomTableItem item = CustomTableItem.New(jobExperienceCustomTable);
                    item.SetValue("UserID", userInfo.UserID);
                    item.SetValue("VenueTypeID", job.VenueTypeID);
                    item.SetValue("JobPosition", job.Position);
                    item.SetValue("Venue", job.Venue);
                    item.SetValue("StartDate", job.StartDate);
                    DateTime dt;
                    if (DateTime.TryParse(job.EndDate, out dt))
                    {
                        item.SetValue("EndDate", job.EndDate);
                    }
                    item.SetValue("CurrentPosition", job.CurrentPosition);
                    item.SetValue("JobDescription", job.Description);

                    try
                    {
                        item.Update();
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider.LogEvent(EventType.ERROR,
                            "User Registration",
                            "There was an issue inserting a user's job experience into the RegisteredMemberJobs custom table: " + ex.Message + "\n" + ex.StackTrace);
                    }
                }
            }
        }
    }

    protected void AddUpdateJobExperiences()
    {
        //Create List
        List<JobExperience> jobsList = (List<JobExperience>)ViewState["JobExpList"];
        List<JobExperience> addJobList = new List<JobExperience>();
        List<JobExperience> updateJobList = new List<JobExperience>();
        List<JobExperience> updateJobViewStateList = new List<JobExperience>();

        string customTableClassName = "customtable.MemberDirJobExp";
        // Gets the custom table
        DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);

        var currentUser = MembershipContext.AuthenticatedUser;

        if (jobsList.Where(x => x.Action == "Add").Count() > 0)
        {
            foreach (JobExperience job in jobsList.Where(x => x.Action == "Add"))
            {
                addJobList.Add(job);
            }

            //Inserting New Jobs
            foreach (JobExperience job in addJobList)
            {
                CustomTableItem item = CustomTableItem.New(customTableClassName);
                item.SetValue("UserID", currentUser.UserID);
                item.SetValue("VenueTypeID", job.VenueTypeID);
                item.SetValue("JobPosition", job.Position);
                item.SetValue("Venue", job.Venue);
                item.SetValue("StartDate", job.StartDate);
                DateTime dt;
                if (DateTime.TryParse(job.EndDate, out dt))
                {
                    item.SetValue("EndDate", job.EndDate);
                }
                item.SetValue("CurrentPosition", job.CurrentPosition);
                item.SetValue("JobDescription", job.Description);
                item.SetValue("JobVenueID", job.JobVenueID);
                item.SetValue("JobPositionID", job.JobPositionID);
                try
                {
                    item.Insert();
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogEvent(EventType.ERROR,
                        "My Profile",
                        "There was an issue adding Job Experience" + ex.Message + "\n" + ex.StackTrace);
                }
            }

        }

        if (jobsList.Where(x => x.Action == "Update").Count() > 0)
        {
            foreach (JobExperience job in jobsList.Where(x => x.Action == "Update"))
            {
                updateJobList.Add(job);
            }

            //Update Existing Jobs
            foreach (JobExperience job in updateJobList)
            {
                CustomTableItem item = CustomTableItemProvider.GetItems(customTableClassName).Where(x => x.ItemID == job.ID).Single();
                if (item != null)
                {
                    item.SetValue("VenueTypeID", job.VenueTypeID);
                    item.SetValue("JobPosition", job.Position);
                    item.SetValue("Venue", job.Venue);
                    DateTime dt;
                    if (DateTime.TryParse(job.EndDate, out dt))
                    {
                        item.SetValue("EndDate", job.EndDate);
                    }
                    if (DateTime.TryParse(job.StartDate, out dt))
                    {
                        item.SetValue("StartDate", job.StartDate);
                    }
                    item.SetValue("CurrentPosition", job.CurrentPosition);
                    item.SetValue("JobDescription", job.Description);
                    item.SetValue("JobVenueID", job.JobVenueID);
                    item.SetValue("JobPositionID", job.JobPositionID);
                    try
                    {
                        item.Update();
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider.LogEvent(EventType.ERROR,
                       "My Profile",
                       "There was an issue Updating User's Job Experience" + ex.Message + "\n" + ex.StackTrace);

                    }
                }

            }

        }
        //1. Get All Job Experiences View from DB, 
        //2. Clear JobExpList View State 
        //3. Add it to ViewState if Job Experience List from DB contains data. 
        //4. Bind Data to Job Experience DataSource
        updateJobViewStateList = GetJobExperiences(currentUser.UserID);
        ViewState["JobExpList"] = "";
        if (updateJobViewStateList != null || updateJobViewStateList.Count > 0)
        {
            ViewState["JobExpList"] = updateJobViewStateList;
            crepJobsInitial.DataSource = updateJobViewStateList;
            crepJobsInitial.DataBind();
        }


    }

    protected bool DeleteJobExperience(int jobId)
    {

        string customTableClassName = "customtable.MemberDirJobExp";
        bool itemDeleted = false;

        // Gets the custom table
        DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
        if (customTable != null)
        {
            // Gets the first custom table record whose value in the 'ItemText' starts with 'New text'
            CustomTableItem item = CustomTableItemProvider.GetItems(customTableClassName).Where(x => x.ItemID == jobId).Single();

            if (item != null)
            {
                // Deletes the custom table record from the database
                itemDeleted = item.Delete();


                return itemDeleted;
            }
            else
            {

                return itemDeleted;
            }

        }
        return itemDeleted;
    }

    protected List<JobExperience> GetJobExperiences(int userInfoId)
    {
        char[] charsToTrim = { ' ' };
        List<JobExperience> jobExpList = new List<JobExperience>();
        string jobExperienceCustomTable = "customtable.MemberDirJobExp";
        string venueTypeTable = "customtable.JobVenueTypes";
        string jobStatus = "";


        DataClassInfo jobCustomTable = DataClassInfoProvider.GetDataClassInfo(jobExperienceCustomTable);
        DataClassInfo venueCustomTable = DataClassInfoProvider.GetDataClassInfo(venueTypeTable);

        if (jobCustomTable != null)
        {
            foreach (var item in CustomTableItemProvider.GetItems(jobExperienceCustomTable).WhereEquals("UserID", userInfoId))
            {

                string sDate = item.GetValue("StartDate", "").ToString();
                string eDate = item.GetValue("EndDate", "").ToString();

                bool currentPos = item.GetValue("CurrentPosition").ToBoolean(false);

                string formattedStartDate = ExtensionMethod.ConvertToDate(sDate);
                string formattedEndDate = ExtensionMethod.ConvertToDate(eDate);

                int venueTypeId = item.GetValue("VenueTypeID").ToInteger(0);
                string venueType = CustomTableItemProvider.GetItem(venueTypeId, venueTypeTable).GetValue("Type").ToString();

                int statusResult = GetJobStatusResult(eDate, currentPos);

                switch (statusResult)
                {
                    case 1:
                        jobStatus = "Current";
                        break;
                    case 2:
                        jobStatus = formattedEndDate;
                        break;
                    case 3:
                        jobStatus = " ";
                        break;
                    default:
                        break;
                }

                jobExpList.Add(new JobExperience
                {
                    ID = (int)item.GetValue("ItemID"),
                    VenueTypeID = item.GetValue("VenueTypeID").ToString(),
                    VenueType = venueType,
                    Position = item.GetValue("JobPosition").ToString(),
                    Venue = item.GetValue("Venue").ToString(),
                    StartDate = formattedStartDate,
                    EndDate = formattedEndDate,
                    CurrentPosition = (bool)item.GetValue("CurrentPosition"),
                    Description = item.GetValue("JobDescription").ToString(),
                    Status = jobStatus,
                    JobVenueID = item.GetValue("JobVenueID").ToInteger(0),
                    JobPositionID = item.GetValue("JobPositionID").ToInteger(0),
                });
            }

        }
        return jobExpList;
    }

    protected int GetJobStatusResult(string unformattedDate, bool currentPosition)
    {
        DateTime jobEndDate;
        DateTime todaysDate = DateTime.Now;

        int positionStatus = 0;


        //Check if date string is empty
        if (!String.IsNullOrEmpty(unformattedDate))
        {
            //Try to convert string to DateTime
            try
            {
                jobEndDate = Convert.ToDateTime(unformattedDate);
            }
            catch (InvalidCastException e)
            {

                throw e;
            }

            //Check if the End Date is in the future(>) Show that the position is current. 
            if (jobEndDate > todaysDate)
            {
                //Current
                positionStatus = 1;
            }
            else
            {
                //End Date
                positionStatus = 2;
            }

        }
        else
        {
            // End Date is empty - Evaulate Current Position
            if (currentPosition)
            {
                //Current
                positionStatus = 1;
            }
            else
            {
                //Empty
                positionStatus = 3;
            }
        }
        return positionStatus;
    }

}
public static class ExtensionMethod
{
    public static bool IsNullOrEmpty(this Array array)
    {
        return (array == null || array.Length == 0);
    }

    public static string ConvertToDate(string dateToFormat)
    {
        string parsedDate = "";

        if (!String.IsNullOrEmpty(dateToFormat))
        {
            parsedDate = Convert.ToDateTime(dateToFormat).ToShortDateString();
        }

        return parsedDate;
    }

}
