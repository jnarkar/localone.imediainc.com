using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;
using CMS.CustomTables;
using CMS.DataEngine;
using CMS.Membership;
using System.Text;
using System.Collections.Generic;

public partial class CMSWebParts_Availability : CMSAbstractWebPart
{
    #region "Properties"

    

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            CurrentUserInfo currentUser = MembershipContext.AuthenticatedUser;

            if (currentUser == null)
                return;

            // Prepares the code name (class name) of the custom table
            string customTableClassName = "customtable.Availability";
            string sAvailableText = "NO";
            // Gets the custom table
            DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
            if (customTable != null)
            {
                // Gets the first custom table record whose value in the 'UserID' field is equal to LoggedIn User UserID
                CustomTableItem availableUser = CustomTableItemProvider.GetItems(customTableClassName)
                                                                    .WhereEquals("UserName", currentUser.UserName)
                                                                    .FirstObject;

                if (availableUser != null)
                {
                    // user already marked as available
                    sAvailableText = "YES";
                    lnkAvailable.CssClass = "avail added";
                    // check when was this row created
                    DateTime rowCreated = availableUser.GetDateTimeValue("ItemCreatedWhen", DateTime.Now);
                    if (rowCreated < DateTime.Now.AddDays(-3))
                    {
                        sAvailableText = "NO";
                        lnkAvailable.CssClass = "avail";
                        availableUser.Delete(); // delete data records
                    }
                }

            }

            // if record doesn't exist
            lnkAvailable.Text = sAvailableText;
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion

    string GetFieldValueFromCustomTable(string _customTableClassName, string[] idsInString, string _columnName)
    {
        string[] sReturn = null;
        DataClassInfo ct = DataClassInfoProvider.GetDataClassInfo(_customTableClassName);
        if (ct != null)
        {
            int iTemp = 0;
            List<int> lstTempIds = new List<int>();
            foreach (var item in idsInString)
            {
                if (int.TryParse(item, out iTemp))
                {
                    lstTempIds.Add(iTemp);
                }
            }
            int[] ids = lstTempIds.ToArray();


            var ct_items = CustomTableItemProvider.GetItems(_customTableClassName)
                .WhereIn("ItemID", ids);
            if (ct_items.Count > 0)
            {
                sReturn = new string[ct_items.Count];
                int i = 0;
                foreach (var item in ct_items)
                {
                    sReturn.SetValue(item.GetStringValue(_columnName, ""), i);
                    i++;
                }
            }
        }
        return sReturn.Join(" | ");
    }

    protected void lnkAvailable_Click(object sender, EventArgs e)
    {
        CurrentUserInfo currentUser = MembershipContext.AuthenticatedUser;
        if (currentUser == null)
            return;

        // Prepares the code name (class name) of the custom table
        string customTableClassName = "customtable.Availability";

        // Gets the custom table
        DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
        if (customTable != null)
        {
            // Gets the first custom table record whose value in the 'UserID' field is equal to LoggedIn User UserID
            CustomTableItem availableUser = CustomTableItemProvider.GetItems(customTableClassName)
                                                                .WhereEquals("UserName", currentUser.UserName)
                                                                .FirstObject;

            string sAvailableText = "NO";

            if (availableUser != null)
            {
                availableUser.Delete(); // delete data records
                sAvailableText = "NO";
                lnkAvailable.CssClass = "avail";
            }
            else
            {
                // to get job skills
                string sJobSkillIds = currentUser.GetStringValue("JobSkillIds", "");
                string[] ids = sJobSkillIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                string sJobs = GetFieldValueFromCustomTable("customtable.JobSkills", ids, "Skill");

                // get license
                string sLicenseCertsIds = currentUser.GetStringValue("LicenseAndCertIds", "");
                ids = sLicenseCertsIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                string sLicenses = GetFieldValueFromCustomTable("customtable.LicenseAndCerts", ids, "Name");

                // create row in customtable
                CustomTableItem newItem = CustomTableItem.New(customTableClassName);
                newItem.SetValue("UserName", currentUser.UserName);
                newItem.SetValue("Name", currentUser.FullName);
                newItem.SetValue("CellNumber", currentUser.GetValue("CellNumber"));
                newItem.SetValue("HomeNumber", currentUser.GetValue("HomeNumber"));
                newItem.SetValue("JobSkillIds", sJobs);
                newItem.SetValue("LicenseAndCertIds", sLicenses);
                newItem.Insert();
                sAvailableText = "YES";
                lnkAvailable.CssClass = "avail added";
            }
            lnkAvailable.Text = sAvailableText;

        }
    }
}



