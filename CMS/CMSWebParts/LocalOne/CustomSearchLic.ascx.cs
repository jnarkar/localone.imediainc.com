using System;

using CMS.PortalEngine.Web.UI;
using CMS.DataEngine;
using CMS.CustomTables;

public partial class CMSWebParts_LocalOne_CustomSearchLic : CMSAbstractWebPart
{
    #region "Properties"


   #endregion


   #region "Methods"

   /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


   /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            // Gets the custom table
            DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
            if (customTable != null)
            {
               // Gets the first custom table record whose value in the 'ItemName' field is equal to "SampleName"
                var item2 = CustomTableItemProvider.GetItems(customTableClassName);
                repItems.DataSource = item2;
				repItems.TransformationName = "IATSE.Transformations.MemberList";
                repItems.DataBind();
            }
        }
    }

    private static string customTableClassName = "CMS_User";

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        
        Response.Redirect(String.Format(CurrentDocument.NodeAliasPath + "?q={0}", txtSearchBox.Text));
        string customTableClassName = "CMS_User";
        // Gets the custom table
        DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
        if (customTable != null)
        {
            // Gets the first custom table record whose value in the 'ItemName' field is equal to "SampleName"
            var item2 = CustomTableItemProvider.GetItems(customTableClassName).Where(String.Format("FullName like '%{0}%' OR LicenseCertListText like '%{0}%'", txtSearchBox.Text));
            repItems.DataSource = item2;
            repItems.TransformationName = "IATSE.Transformations.MemberList";
            repItems.DataBind();
        }
    }

    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }

   #endregion
}

