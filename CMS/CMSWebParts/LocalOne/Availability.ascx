<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_Availability"  CodeFile="~/CMSWebParts/LocalOne/Availability.ascx.cs" %>

<asp:PlaceHolder runat="server">
<ul class="top">
    <%if (CurrentUser.IsInRole("StewardReportsApprover", CurrentSite.SiteName))
        {%>
            <li><a href="/members/Steward-Report-Listing">Stewards Report Approval</a></li>
       <% } %>
    <li><a href="/members/Steward-Reports-Registration">Stewards Report</a></li>
    <li><a href="/members/Employment/Member-Directory">Member Directory</a></li>
    <li>
        <span class="avail-btn">Available for Work?<asp:LinkButton runat="server" CssClass="avail" ID="lnkAvailable" OnClick="lnkAvailable_Click">NO</asp:LinkButton></span>
    </li>
</ul>
    </asp:PlaceHolder>