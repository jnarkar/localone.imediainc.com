using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;

public partial class CMSWebParts_LocalOne_Registration : CMSAbstractWebPart
{
    public class LocalOneMember
    {
        public string Email { get; set; }
        public string UnionCardNumber { get; set; }

    }
    #region "Properties"

    

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion

    protected void cbScreen1_Click(object sender, EventArgs e)
    {
        cPanelRegister.Visible = false;
        cPanelCreatePassword.Visible = true;
        cPanelProfile.Visible = false;
        cPanelConfirmInfo.Visible = false;
    }


    #region make this highly efficient and error free
    System.Collections.Generic.List<Panel> lst;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // add your panel to control show/hide
            lst = new System.Collections.Generic.List<Panel>(){
                cPanelRegister, cPanelCreatePassword, cPanelProfile, cPanelConfirmInfo
            };
        }
    }

    /// <summary>
    /// Generic Method to show only required panel and hide rest of the panels
    /// </summary>
    /// <param name="_this"></param>
    private void showThis(Panel _this)
    {
        foreach (var item in lst)
        {
            item.Visible = (item == _this);
        }
    }
    #endregion

    protected void cbScreen2_Click(object sender, EventArgs e)
    {
        cPanelRegister.Visible = false;
        cPanelCreatePassword.Visible = false;
        cPanelProfile.Visible = true;
        cPanelConfirmInfo.Visible = false;
    }
    protected void cbScreen3_Click(object sender, EventArgs e)
    {
        cPanelRegister.Visible = false;
        cPanelCreatePassword.Visible = false;
        cPanelProfile.Visible = false;
        cPanelConfirmInfo.Visible = true;
    }

    protected void cbRegister_Click(object sender, EventArgs e)
    {
        cPanelButtons.Visible = true;
        cPanelRegister.Visible = false;
        cPanelCreatePassword.Visible = true;
        cPanelProfile.Visible = false;
        cPanelConfirmInfo.Visible = false;
    }

    protected void cbCreatePassword_Click(object sender, EventArgs e)
    {
        cPanelRegister.Visible = false;
        cPanelCreatePassword.Visible = false;
        cPanelProfile.Visible = true;
        cPanelConfirmInfo.Visible = false;
    }

    

    protected void cbSaveProfile_Click(object sender, EventArgs e)
    {
        cPanelRegister.Visible = false;
        cPanelCreatePassword.Visible = false;
        cPanelProfile.Visible = false;
        cPanelConfirmInfo.Visible = true;
    }


    protected void cbEditInfo_Click(object sender, EventArgs e)
    {

    }

    protected void cbAddAnotherJob_Click(object sender, EventArgs e)
    {

    }

    protected void cbEditLicenses_Click(object sender, EventArgs e)
    {

    }

    protected void cbEditJobs_Click(object sender, EventArgs e)
    {

    }

    
}



