<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_LocalOne_Registration" CodeFile="~/CMSWebParts/LocalOne/Registration.ascx.cs" %>
<style>
    .registration {
        background-color: #fff;
    }

    .inline {
        display: inline-block;
    }

    input {
        border: 1px solid #000;
    }
</style>

<asp:UpdatePanel ID="cupRegistration" runat="server">
    <ContentTemplate>
        <asp:Panel ID="cPanelButtons" runat="server" Visible="false">
            <asp:Button ID="cbScreen1" runat="server" Text="Create Password" OnClick="cbScreen1_Click" />
            <asp:Button ID="cbScreen2" runat="server" Text="Create Profile" OnClick="cbScreen2_Click" />
            <asp:Button ID="cbScreen3" runat="server" Text="Confirm Your Information" OnClick="cbScreen3_Click" />
        </asp:Panel>
        <asp:Panel ID="cPanelRegister" runat="server" Visible="true">
            <div id="register">
                <p>Please enter your email and Union Card Number. After successfully logging in, you will proceed to registration.</p>
                <div class="form">
                    <div class="inline">
                        Email Address: *<br />
                        <asp:TextBox ID="ctbEmail" runat="server"></asp:TextBox>
                    </div>
                    <div class="inline">
                        Confirm Email Address: *<br />
                        <asp:TextBox ID="ctbEmail2" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        Union Card Number:<br />
                        <asp:TextBox ID="ctbCardNumber" runat="server"></asp:TextBox><br />
                        <asp:Button ID="cbRegister" runat="server" CssClass="register-btn" Text="Login & Proceed to Registration" OnClick="cbRegister_Click" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="cPanelCreatePassword" runat="server" Visible="false">
            <div id="create-password">
                <h3>Create your password</h3>
                <p>Please create a password to begin creating your account.</p>
                <div class="form">
                    <div class="inline">
                        Password<br />
                        <asp:TextBox ID="ctbPassword1" runat="server" />
                    </div>
                    <div class="inline">
                        Confirm Password<br />
                        <asp:TextBox ID="ctbPassword2" runat="server" />
                    </div>
                    <div>
                        <asp:Button ID="cbCreatePassword" runat="server" CssClass="create-password-btn" Text="Save & Continue" OnClick="cbCreatePassword_Click" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="cPanelProfile" runat="server" Visible="false">
            <div id="create-profile">
                <h3>Create your profile</h3>
                <p>Please enter your personal information about job skills, licenses, certifications or jobs.</p>
                <h3>Personal Information</h3>
                <div class="form">
                    Name: *<br />
                    <div class="inline">
                        <asp:TextBox ID="ctbFirstName" runat="server"></asp:TextBox><br />
                        First Name
                    </div>
                    <div class="inline">
                        <asp:TextBox ID="ctbLastName" runat="server"></asp:TextBox><br />
                        Last Name
                    </div>
                    <div>
                        Street Address: *<br />
                        <asp:TextBox ID="ctbAddress" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <div class="inline">
                            City: *<br />
                            <asp:TextBox ID="ctbCity" runat="server"></asp:TextBox>
                        </div>
                        <div class="inline">
                            State: *<br />
                            <asp:TextBox ID="ctbState" runat="server"></asp:TextBox>
                        </div>
                        <div class="inline">
                            Zip Code: *<br />
                            <asp:TextBox ID="ctbZipCode" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div>
                        <div class="inline">
                            Cell Number: *<br />
                            <asp:TextBox ID="ctbCellNumber" runat="server"></asp:TextBox>
                        </div>
                        <div class="inline">
                            Home Number:
                            <br />
                            <asp:TextBox ID="ctbHomeNumber" runat="server"></asp:TextBox>
                        </div>
                    </div>
				    
                    <div>
                        <h3>Job Skills1:</h3>
                        <br />
                        <input type="button" id="add-skills-btn" value="+ Add Job Skills" />
                    </div>
                    <div>
                        <h3>Licenses & Certifications:</h3>
                        <br />
                        <input type="button" id="add-licenses-btn" value="+ Add Licenses & Certifications" />
                    </div>
                    <div>
                        <h3>Job Experience:</h3>
                        <br />
                        <input type="button" id="add-job-btn" value="+ Add a Job" />
                    </div>
                    <div>
                        <asp:Button ID="cbSaveProfile" runat="server" Text="Save & Continue" OnClick="cbSaveProfile_Click" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="cPanelConfirmInfo" runat="server" Visible="false">
            <div id="confirm-info">
                <h3>Confirm Your Information</h3>
                <p>Before we create your account, please confirm the information you have entered.</p>
                <div class="form">
                    <div>
                        <h3>
                            <asp:Label ID="clName" runat="server"></asp:Label></h3>
                        <asp:Button ID="cbEditInfo" runat="server" Text="edit" OnClick="cbEditInfo_Click" /><br />
                        <asp:Label ID="clCardNumber" runat="server"></asp:Label>
                        &nbsp; Union Card Number<br />
                        <asp:Label ID="clStreetAddress" runat="server"></asp:Label><br />
                        <asp:Label ID="clCityStateZip" runat="server"></asp:Label><br />
                        <asp:Label ID="clCellPhone" runat="server"></asp:Label><br />
                        <asp:Label ID="clHomePhone" runat="server"></asp:Label><br />
                        <asp:Label ID="clEmail" runat="server"></asp:Label>
                    </div>
                    <div>
                        <h3>Job Skills</h3>
                        <asp:Button ID="cbEditJobs" runat="server" Text="edit" OnClick="cbEditJobs_Click" /><br />
                        <asp:Repeater ID="crepJobSkills" runat="server">
                            <ItemTemplate>
                                <p><%# Eval("Title") %></p>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div>
                        <h3>Licenses & Certification</h3>
                        <asp:Button ID="cbEditLicenses" runat="server" Text="edit" OnClick="cbEditLicenses_Click" /><br />
                        <asp:Repeater ID="crepLicenses" runat="server">
                            <ItemTemplate>
                                <p><%# Eval("Title") %></p>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div>
                        <h3>Job Experience</h3>
                        <asp:Repeater ID="crepJobs" runat="server">
                            <HeaderTemplate>
                                <div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="item">
                                    <p><%# Eval("Venue") %></p>
                                    <p><%# Eval("VenueType") %></p>
                                    <p><%# Eval("Title") %></p>
                                    <p><%# Eval("Dates") %></p>
                                    <asp:LinkButton ID="clbEditJob" runat="server" CommandName="edit" CommandArgument='<%# Eval("ID") %>'>Edit</asp:LinkButton>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div>
                            <asp:Button ID="cbAddAnotherJob" runat="server" Text="+ Add Another Job" OnClick="cbAddAnotherJob_Click" />
                        </div>
                        <div>
                            <asp:Button ID="cbCreateAccount" runat="server" Text="Save & Create Account" OnClick="cbSaveProfile_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="cbScreen1" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbScreen2" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbScreen3" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbRegister" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbCreatePassword" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbSaveProfile" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbEditInfo" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbEditJobs" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbEditLicenses" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbAddAnotherJob" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="cbCreateAccount" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>


