<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_LocalOne_CustomSearch"  CodeFile="~/CMSWebParts/LocalOne/CustomSearch.ascx.cs" %>
<div class="search-filter md">
    <label>Search:</label>
    <asp:TextBox runat="server" ID="txtSearchBox" />
    <asp:Button Text="SUBMIT" runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" />
</div>
<div>
    <div class="member">
        <div class="member-row first">
            <div class="col-33">
                <p>Name</p>
            </div>
            <div class="col-33">
                <p>Position</p>
            </div>
            <div class="col-33">
                <p>Venue</p>
            </div>
            <!--<div class="col-25">
                <p>Phone Number</p>
            </div>-->
        </div>
        <cms:queryrepeater id="repItems" runat="server" /> 
    </div>
</div>

     
