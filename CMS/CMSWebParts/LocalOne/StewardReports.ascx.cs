using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;
using CMS.Membership;
using CMS.DataEngine;
using CMS.CustomTables;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CMS.SiteProvider;
using CMS.EmailEngine;
using CMS.EventLog;
using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class CMSWebParts_LocalOne_StewardReports : CMSAbstractWebPart
{
    #region "Properties"
    public DateTime startDateOfWeek { get; set; }
    public DateTime endDateOfWeek { get; set; }
    public string jsonUsers = "";
    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }

    /*Role Names*/
    private readonly string MemberRole = "RegisteredUser";
    private readonly string StewardRole = "LocalOneSteward";
    /* Custom Table Class Name*/
    private readonly string ctClassVenues = "customtable.stewardjobvenues";
    private readonly string ctClassStewardRegistrationApproval = "customtable.stewardregistration_approval";
    private readonly string ctClassStewardReport = "customtable.stewardreport";
    private readonly string ctClassPositions = "customtable.JobPositions";
    private readonly string ctClassStewardReportApproval = "customtable.stewardreportapproval";


    protected void Page_Load(object sender, EventArgs e)
    {



        startDateOfWeek = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
        DateTime currentStartOfWeek = startDateOfWeek; // the current start of the week
        endDateOfWeek = startDateOfWeek.AddDays(7);


        SetAutoCompleteUsers();

        if (!IsPostBack)
        {
            // hide panels

            // #### get jobs for this week only
            for (int i = 0; i < 4; i++)
            {
                DateTime temp = startDateOfWeek.AddDays(-(7 * i));
                ddlWeek.Items.Add(new ListItem
                {
                    Text = temp.ToString("MMM dd, yyyy") + " - " + temp.AddDays(7).ToString("MMM dd, yyyy"),
                    Value = temp.ToShortDateString()
                });
            }

            var listSelectedWeekJobsTemp = CustomTableItemProvider.GetItems(ctClassStewardReportApproval)
                .WhereEquals("StewardUserName", CurrentUser.UserName);
                //.WhereEquals("Approved", true)
                //.WhereEquals("UnderApproval", true);

            // this is for the listing radio button list in modal popup
            for (int i = 0; i < 4; i++)
            {
                // WeekStarting
                bool dtEnabled = true;
                DateTime temp = startDateOfWeek.AddDays(-(7 * i));
                foreach (var item in listSelectedWeekJobsTemp)
                {
                    if (temp.Equals(DateTime.Parse(item.GetStringValue("WeekStarting", DateTime.Now.ToString())))){
                        dtEnabled = false;
                        break;
                    }
                }

                radWeek.Items.Add(new ListItem
                {
                    Text = temp.ToString("MMM dd, yyyy") + " - " + temp.AddDays(7).ToString("MMM dd, yyyy"),
                    Value = temp.ToShortDateString(),
                    Enabled = dtEnabled
                });
            }

            if (Session["PreviousDDLIndex"] != null)
            {
                ddlWeek.SelectedIndex = (int)Session["PreviousDDLIndex"];
            }
          

            // load positions
            DataClassInfo dciPositions = DataClassInfoProvider.GetDataClassInfo(ctClassPositions);
            if (dciPositions != null)
            {
                var lstPositions = CustomTableItemProvider.GetItems(ctClassPositions);
                if (lstPositions != null)
                {
                    if (lstPositions.TotalRecords > 0)
                    {
                        // load positions
                        foreach (var position in lstPositions)
                        {
                            string sPosition = position.GetStringValue("Position", "NA");
                            ddlUnionPosition.Items.Add(new ListItem { Text = sPosition, Value = sPosition });
                            ddlProductionPosition.Items.Add(new ListItem { Text = sPosition, Value = sPosition });
                        }
                    }
                }
            }
        }


        //startDateOfWeek = SelectedDates[0] == null ? currentStartOfWeek : DateTime.Parse(SelectedDates[1]);
        startDateOfWeek = DateTime.Parse(ddlWeek.SelectedValue);
        //endDateOfWeek = SelectedDates[1] == null ? endDateOfWeek : DateTime.Parse(SelectedDates[1]);
        endDateOfWeek = startDateOfWeek.AddDays(7);


        // set week
        ltrWeekEnding.Text = String.Format("{0} to {1}", startDateOfWeek.ToString("MMM dd, yyyy"), endDateOfWeek.ToString("MMM dd, yyyy"));

        #region Checks to see if the week rendered is the current week. If so do not show Copy Jobs Button


        bool isCurrentWeek = startDateOfWeek.ToShortDateString().Equals(currentStartOfWeek.ToShortDateString());


        //if current week is false, current week report exists, and current week report is in denied , the nrender copy jobs for selected week that is either in under approval or approved

        #endregion


        pnlReportApprovalStatus.Visible = false;
        pnlReportApproved.Visible = false;
        pnlReportDenied.Visible = false;


        #region Check Report status for selected week
        // check the status of the report



        var listSelectedWeekJobs = CustomTableItemProvider.GetItems(ctClassStewardReportApproval)
            .WhereEquals("StewardUserName", CurrentUser.UserName)
            .WhereEquals("WeekStarting", startDateOfWeek.ToShortDateString())
            //.WhereEquals("UnderApproval", true)
            .FirstObject;
			



        if (listSelectedWeekJobs != null)
        {

            bool bUnderApproval = listSelectedWeekJobs.GetBooleanValue("UnderApproval", true);
            bool bApproved = listSelectedWeekJobs.GetBooleanValue("Approved", true);
            txtNoJobsExistForReport.Text = "false";
            if (bUnderApproval)
            {
				pnlReportApprovalStatus.Visible = !bApproved;
                pnlAddNewJob.Visible = false;
                pnlSendReport.Visible = false;
                //pnlCopyJobs.Visible = true;
                //btnCopyJobs.Visible = true;
                ShowCopySelectedWeek(isCurrentWeek, startDateOfWeek, (bUnderApproval == bApproved));

            }
            else
            {
                //bool bApproved = listSelectedWeekJobs.GetBooleanValue("Approved", false);
                if (bApproved)
                {
                    pnlReportApproved.Visible = true;
                    pnlAddNewJob.Visible = false;
                    pnlSendReport.Visible = false;
                    //pnlCopyJobs.Visible = true;
                    //btnCopyJobs.Visible = true;
                    ShowCopySelectedWeek(isCurrentWeek, startDateOfWeek, false);

                }
                else
                {
                    
                    pnlReportDenied.Visible = true;
                    pnlAddNewJob.Visible = true;
                    txtJobsEditable.Text = "True";
                    pnlSendReport.Visible = false;
                    ShowCopySelectedWeek(isCurrentWeek, startDateOfWeek, true );

                }
            }

        }
        else
        {
            pnlAddNewJob.Visible = true;
            pnlSendReport.Visible = true;
            pnlCopyJobs.Visible = false;
            btnCopyJobs.Visible = false;
            txtNoJobsExistForReport.Text = "true";
            txtJobsEditable.Text = "True";
        }


        #endregion

        // At last load tables
        LoadDataTable();
    }

    /// <summary>
    /// This will load table from custom table
    /// </summary>
    private void LoadDataTable()
    {
        // the user in MemberRole and looking forward to request for an approval
        DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(ctClassStewardReport);
        if (customTable != null)
        {


            var lstJobUnion = CustomTableItemProvider.GetItems(ctClassStewardReport)
              .WhereEquals("JobType", "Union")
              .WhereEquals("StewardUserName", CurrentUser.UserName) // Uncomment this to show only stewards data
              .WhereEquals("StartDate", startDateOfWeek.ToShortDateString())
              .WhereEquals("EndDate", endDateOfWeek.ToShortDateString());

            if (lstJobUnion != null)
            {
                if (lstJobUnion.TotalRecords > 0)
                    txtJobsUnion.Text = lstJobUnion.ToJSON("data", true);
                else
                    txtJobsUnion.Text = "";
            }


            var lstJobProd = CustomTableItemProvider.GetItems(ctClassStewardReport)
                .WhereEquals("JobType", "Production")
                .WhereEquals("StewardUserName", CurrentUser.UserName)
                .WhereEquals("StartDate", startDateOfWeek.ToShortDateString())
                .WhereEquals("EndDate", endDateOfWeek.ToShortDateString());
            if (lstJobProd != null)
            {

                if (lstJobProd.TotalRecords > 0)
                    txtJobsProduction.Text = lstJobProd.ToJSON("data", true);
                else
                    txtJobsProduction.Text = "";
            }
        }

    }

    private void ReloadPage()
    {
        if (Session["PreviousDDLIndex"] != null)
        {
            Session.Remove("PreviousDDLIndex");
            Session.Add("PreviousDDLIndex", ddlWeek.SelectedIndex);
        }
        else
        {
            Session.Add("PreviousDDLIndex", ddlWeek.SelectedIndex);
        }

        Response.Redirect(Request.RawUrl, true);
    }
    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            // get list from custom table
            CurrentUserInfo currentUser = MembershipContext.AuthenticatedUser;

            if (currentUser == null)
            {
                Response.Redirect("~/");
                return;
            }

            // check role : if the user is not Member throw them out
            if (!currentUser.IsInRole(MemberRole, SiteContext.CurrentSiteName))
            {
                mvStewardRegistration.SetActiveView(vAuthorizeError);
                return;
            }

            // check if the user is in steward role
            if (currentUser.IsInRole(StewardRole, SiteContext.CurrentSiteName))
            {
                // check if the user has approval
                DataClassInfo ctSteardApproval = DataClassInfoProvider.GetDataClassInfo(ctClassStewardRegistrationApproval);
                if (ctSteardApproval != null)
                {
                    CustomTableItem ctiStewardApproval = CustomTableItemProvider.GetItems(ctClassStewardRegistrationApproval)
                        .WhereEquals("UserName", currentUser.UserName)
                        .FirstObject;
                    if (ctiStewardApproval != null)
                    {
                        bool bApproved = ctiStewardApproval.GetBooleanValue("Approved", false);
                        if (!bApproved)
                        {
                            // redirect to next page
                            Response.Redirect(CurrentDocument.Parent.NodeAliasPath);
                        }
                        // set Venue
                        hdnVenue.Value = ctiStewardApproval.GetStringValue("Venue", "N/A");
                        ltrVenue.Text = hdnVenue.Value;
                    }
                }
            }

            // set the user details
            ltrStewardName.Text = String.Format("{0} {1}", currentUser.FirstName, currentUser.LastName);
        }
    }

    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion

    protected void SendEmail()
    {
        CMS.EmailEngine.EmailMessage em = new CMS.EmailEngine.EmailMessage();
        em.EmailFormat = CMS.EmailEngine.EmailFormatEnum.Html;
        em.From = "noreply@imediainc.com";
        //em.Recipients = "support@imediainc.com";
        em.Recipients = "dthorn@iatse-local1.org, emac@iatse-local1.org, amanno@iatse-local1.org";
        em.Subject = "Report Approval";
        //string bodyText = "";
        em.Body = String.Format("Steward {0} ; {1}, has sent report for approval for week ending {2}.", CurrentUser.FullName, CurrentUser.UserName, endDateOfWeek.ToString("ddMMMyyyy"));

        CMS.EmailEngine.EmailSender.SendEmail(CurrentSiteName, em);

    }

    /// <summary>
    /// Final step for current week to send report for approval
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSendReport_Click(object sender, EventArgs e)
    {
        // mark current week's job with flag
        DataClassInfo dciSendReportApproval = DataClassInfoProvider.GetDataClassInfo(ctClassStewardReportApproval);
        if (dciSendReportApproval != null)
        {
            // get data for current week
            var lstJobsForCurrentWeek = CustomTableItemProvider.GetItems(ctClassStewardReportApproval)
                .WhereEquals("StewardUserName", CurrentUser.UserName)
                .WhereEquals("WeekStarting", startDateOfWeek);

            if (lstJobsForCurrentWeek != null)
            {
                if (lstJobsForCurrentWeek.TotalRecords > 0)
                {
                    CustomTableItem reportForApproval = lstJobsForCurrentWeek.FirstObject;
                    if (reportForApproval != null)
                    {
                        reportForApproval.SetValue("StewardUserName", CurrentUser.UserName);
                        reportForApproval.SetValue("WeekStarting", startDateOfWeek.ToShortDateString());
                        reportForApproval.SetValue("UnderApproval", true);
                        reportForApproval.SetValue("Approved", false);
                        reportForApproval.SetValue("Denied", false);
                        reportForApproval.SetValue("Notes", txtNotes.Text);

                        reportForApproval.Update();
                    }
                }
                else
                {
                    // insert new record
                    CustomTableItem ctiNewReportApproval = CustomTableItem.New(ctClassStewardReportApproval);
                    ctiNewReportApproval.SetValue("StewardUserName", CurrentUser.UserName);
                    ctiNewReportApproval.SetValue("WeekStarting", startDateOfWeek.ToShortDateString());
                    ctiNewReportApproval.SetValue("UnderApproval", true);
                    ctiNewReportApproval.SetValue("Approved", false);
                    ctiNewReportApproval.SetValue("Denied", false);
                    ctiNewReportApproval.SetValue("Notes", txtNotes.Text);
                    ctiNewReportApproval.SetValue("StartDate", startDateOfWeek.ToShortDateString());
                    ctiNewReportApproval.SetValue("EndDate", endDateOfWeek.ToShortDateString());

                    ctiNewReportApproval.Insert();
                }
                SendEmail();
                ReloadPage();
            }
            else
            {
                // show error saying there is no data in custom table
                // "customtable.stewardreportapproval"
            }
        }
        else
        {
            // show error here saying 
            // "customtable.stewardreportapproval" doesn't exist
        }

    }

    /// <summary>
    /// Remove jobs from CustomTable, only for the current week
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRemoveJob_Click(object sender, EventArgs e)
    {
        // get the itemGUID 
        string sJobItemGUID = txtJobIdToRemove.Text + "";

        if (sJobItemGUID != "")
        {
            CustomTableItem jobToRemove = CustomTableItemProvider.GetItems(ctClassStewardReport)
                .WhereEquals("ItemGUID", sJobItemGUID).FirstObject;

            if (jobToRemove != null)
            {
                jobToRemove.Delete();
                LoadDataTable();
            }
        }
    }

    /// <summary>
    /// Currently not in use as page just needs to get postback
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        //
    }

    /// <summary>
    /// Currently not in use as page just needs to get postback
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Btn_Click(object sender, EventArgs e)
    {
        //
    }



    /// <summary>
    /// Copy jobs from selected week in drop down list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCopyJobs_Click(object sender, EventArgs e)
    {
        //if (string.Equals(txtNoJobsExistForReport.Text, "false", StringComparison.OrdinalIgnoreCase) 
        //    && string.Equals(txtCurrenWeekSubmitted.Text, "false", StringComparison.OrdinalIgnoreCase))
        //{

            // get current selected date
            DateTime startDateOfWeek = DateTime.Parse(ddlWeek.SelectedValue);
            DateTime endDateOfWeek = startDateOfWeek.AddDays(7);

            DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(ctClassStewardReport);
            if (customTable != null)
            {
                // get job list for Union
                //var lstSelectedWeekJobs = CustomTableItemProvider.GetItems(ctClassStewardReport)
                //    .WhereGreaterThan("ItemCreatedWhen", startDateOfWeek)
                //    .WhereLessThan("ItemCreatedWhen", endDateOfWeek);

                // get job list for Union
                var lstSelectedWeekJobs = CustomTableItemProvider.GetItems(ctClassStewardReport)
                     .WhereEquals("StartDate", startDateOfWeek.ToShortDateString())
                     .WhereEquals("EndDate", endDateOfWeek.ToShortDateString());

                DateTime startOfCurrentWeek = DateTime.Parse(radWeek.SelectedValue); //Selected week
                DateTime endOfCurrentWeek = startOfCurrentWeek.AddDays(7);

                if (lstSelectedWeekJobs != null)
                {
                    foreach (var ctiJob in lstSelectedWeekJobs)
                    {
                        //var clonedItem = ctiJob.Clone();
                        //clonedItem.Insert();
                        CustomTableItem newJob = CustomTableItem.New(ctClassStewardReport);
                        newJob.SetValue("UserName", ctiJob.GetStringValue("UserName", ""));
                        newJob.SetValue("StewardUserName", ctiJob.GetStringValue("StewardUserName", ""));
                        newJob.SetValue("Venue", ctiJob.GetStringValue("Venue", ""));
                        newJob.SetValue("Name", ctiJob.GetStringValue("Name", ""));
                        newJob.SetValue("JobType", ctiJob.GetStringValue("JobType", ""));
                        newJob.SetValue("LocalOneUnionCardNumber", ctiJob.GetStringValue("LocalOneUnionCardNumber", ""));
                        newJob.SetValue("UnionCardNumber", ctiJob.GetStringValue("UnionCardNumber", ""));
                        newJob.SetValue("Position", ctiJob.GetStringValue("Position", ""));
                        newJob.SetValue("Hours", ctiJob.GetStringValue("Hours", ""));
                        newJob.SetValue("SentForApproval", 0);
                        newJob.SetValue("Shows", ctiJob.GetStringValue("Shows", ""));
                        newJob.SetValue("StartDate", startOfCurrentWeek.ToShortDateString());
                        newJob.SetValue("EndDate", endOfCurrentWeek.ToShortDateString());
                        newJob.SetValue("JobApproved", 0);


                        newJob.Insert();
                    }
                }
            }
            ReloadPage();
        //}
    }

    /// <summary>
    /// Add new job for Union
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUnionAddNewJob_Click(object sender, EventArgs e)
    {

        //ReloadPage();
        //return;
        if (txtJobIdToEdit.Text != "")
        {
            // update existing item
            string sJobItemGUID = txtJobIdToEdit.Text;
            // get the CustomTableItem

            CustomTableItem newJob = CustomTableItemProvider.GetItem(Guid.Parse(sJobItemGUID), ctClassStewardReport);

            newJob.SetValue("UserName", txtUnionMember.Text);
            newJob.SetValue("StewardUserName", CurrentUser.UserName);
            newJob.SetValue("Venue", hdnVenue.Value);
            newJob.SetValue("Name", txtAutoCompleteUnion.Text);
            newJob.SetValue("JobType", "Union");
            newJob.SetValue("LocalOneUnionCardNumber", txtUnionLocalUnionNumber.Text);
            newJob.SetValue("UnionCardNumber", txtUnionCardNumber.Text);
            newJob.SetValue("Position", ddlUnionPosition.Text);
            newJob.SetValue("Hours", txtUnionHours.Text);
            newJob.SetValue("Shows", txtUnionShows.Text);

            newJob.Update();

            if (pnlReportDenied.Visible)
            {
                CustomTableItem reportTableItem = CustomTableItemProvider.GetItems(ctClassStewardReportApproval)
                                                .WhereEquals("StewardUserName", CurrentUser.UserName)
                                                .WhereEquals("WeekStarting", startDateOfWeek.ToShortDateString())
                                                //.WhereEquals("UnderApproval", true)
                                                .FirstObject;

                reportTableItem.SetValue("Approved", 0);
                reportTableItem.SetValue("Denied", 0);
                reportTableItem.SetValue("UnderApproval", 1);
                reportTableItem.Update();

            }
        }
        else
        {
            // get all the values from 
            CustomTableItem newJob = CustomTableItem.New(ctClassStewardReport);
            newJob.SetValue("UserName", txtUnionMember.Text);
            newJob.SetValue("StewardUserName", CurrentUser.UserName);
            newJob.SetValue("Venue", hdnVenue.Value);
            newJob.SetValue("Name", txtAutoCompleteUnion.Text);
            newJob.SetValue("JobType", "Union");
            newJob.SetValue("LocalOneUnionCardNumber", txtUnionLocalUnionNumber.Text);
            newJob.SetValue("UnionCardNumber", txtUnionCardNumber.Text);
            newJob.SetValue("Position", ddlUnionPosition.Text);
            newJob.SetValue("Hours", txtUnionHours.Text);
            newJob.SetValue("Shows", txtUnionShows.Text);
            newJob.SetValue("SentForApproval", 0);
            newJob.SetValue("JobApproved", 0);
            newJob.SetValue("StartDate", startDateOfWeek.ToShortDateString());
            newJob.SetValue("EndDate", endDateOfWeek.ToShortDateString());

            newJob.Insert();
        }
        ReloadPage();
    }

    /// <summary>
    /// Add new job for production type
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnProductionAddNewJob_Click(object sender, EventArgs e)
    {
        //ReloadPage();
        //return;
        if (txtJobIdToEdit.Text != "")
        {
            // update existing item
            string sJobItemGUID = txtJobIdToEdit.Text;

            CustomTableItem newJob = CustomTableItemProvider.GetItem(Guid.Parse(sJobItemGUID), ctClassStewardReport);
            newJob.SetValue("UserName", txtProductionMember.Text);
            newJob.SetValue("StewardUserName", CurrentUser.UserName);
            newJob.SetValue("Venue", hdnVenue.Value);
            newJob.SetValue("Name", txtAutoCompleteProduction.Text);
            newJob.SetValue("LocalOneUnionCardNumber", txtProductionLocalUnionNumber.Text);
            newJob.SetValue("UnionCardNumber", txtProductionCardNumber.Text);
            newJob.SetValue("JobType", "Production");
            newJob.SetValue("Position", ddlProductionPosition.Text);
            newJob.SetValue("Hours", txtProductionHours.Text);
            newJob.SetValue("Shows", txtProductionShows.Text);
            newJob.Update();
        }
        else
        {
            // get all the values from 
            CustomTableItem newJob = CustomTableItem.New(ctClassStewardReport);
            newJob.SetValue("UserName", txtProductionMember.Text);
            newJob.SetValue("StewardUserName", CurrentUser.UserName);
            newJob.SetValue("Venue", hdnVenue.Value);
            newJob.SetValue("Name", txtAutoCompleteProduction.Text);
            newJob.SetValue("JobType", "Production");
            newJob.SetValue("LocalOneUnionCardNumber", txtProductionLocalUnionNumber.Text);
            newJob.SetValue("UnionCardNumber", txtProductionCardNumber.Text);
            newJob.SetValue("Position", ddlProductionPosition.Text);
            newJob.SetValue("Hours", txtProductionHours.Text);
            newJob.SetValue("Shows", txtProductionShows.Text);
            newJob.SetValue("SentForApproval", 0);
            newJob.SetValue("JobApproved", 0);
            newJob.SetValue("StartDate", startDateOfWeek.ToShortDateString());
            newJob.SetValue("EndDate", endDateOfWeek.ToShortDateString());

            newJob.Insert();
        }
        ReloadPage();
    }


    public void ShowCopySelectedWeek(bool isCurrentWeek, DateTime currentStartOfWeek, bool showCopyButtonForSelectedList )
    {
		if (showCopyButtonForSelectedList)
					{
						pnlCopyJobs.Visible = true;
						btnCopyJobs.Visible = true;
					}else{
						pnlCopyJobs.Visible = true;
						btnCopyJobs.Visible = true;
					}
					 return;
/*
        DateTime dteCurrentStartOfWeek = currentStartOfWeek;

        var reportForCurrentWeek = CustomTableItemProvider.GetItems(ctClassStewardReportApproval)
            .WhereEquals("StewardUserName", CurrentUser.UserName)
            .WhereEquals("WeekStarting", dteCurrentStartOfWeek.ToShortDateString())
            .FirstObject;
        

        if (reportForCurrentWeek != null)
        {
            pnlCopyJobs.Visible = false;
            btnCopyJobs.Visible = false;
            txtCurrenWeekSubmitted.Text = "true";
            bool isCurrentWeekApproved = reportForCurrentWeek.GetBooleanValue("Approved", false);
            bool isCurrentWeekDenied = reportForCurrentWeek.GetBooleanValue("Denied", false);

            if (!isCurrentWeek)
            {
                if (isCurrentWeekApproved)
                {

                    if (showCopyButtonForSelectedList)
					{
						pnlCopyJobs.Visible = true;
						btnCopyJobs.Visible = true;
					}else{
						pnlCopyJobs.Visible = false;
						btnCopyJobs.Visible = false;
					}

                }
                else if (isCurrentWeekDenied)
                {
					
					pnlCopyJobs.Visible = true;
					btnCopyJobs.Visible = true;
					
                }
            }
            else
            {
                
					pnlCopyJobs.Visible = true;
					btnCopyJobs.Visible = true;
				

            }
        }
        else
        {
            if (showCopyButtonForSelectedList)
            {
                pnlCopyJobs.Visible = true;
                btnCopyJobs.Visible = true;
            }
            txtCurrenWeekSubmitted.Text = "false";
        }
		*/
    }


    public string DataTableToJSONWithJSONNet(DataTable table)
    {
        table.Columns.Remove("FirstName");
        table.Columns.Remove("LastName");

        string JSONString = string.Empty;
        return JsonConvert.SerializeObject(table);
    }
    
    public void SetAutoCompleteUsers()
    {
        RoleInfo role = RoleInfoProvider.GetRoleInfo(MemberRole, SiteContext.CurrentSiteName);
        if (role != null)
        {
            // generate DataQuery
            var query = new DataQuery("cms.user.imediaselectuserbyrole");
            // generate the querydataparameters
            QueryDataParameters parameters = new QueryDataParameters();
            parameters.Add("@RoleID", role.RoleID);
            query.Parameters = parameters;

            DataSet selectedUser = query.Result;

            jsonUsers = DataTableToJSONWithJSONNet(selectedUser.Tables[0]);

            //Dropdown no longer users...Autocomplete is used instead
            //foreach (DataRow item in selectedUser.Tables[0].Rows)
            //{
            //    string sOptionValue = item["UserName"].ToString();
            //    //string sOptionText = String.Format("{0} {1}", ValidationHelper.GetString(item["FirstName"], ""), ValidationHelper.GetString(item["LastName"], ""), sOptionValue);
            //    string sOptionText = ValidationHelper.GetString(item["FullName"], "N/A");
            //    ddlUnionMembers.Items.Add(new ListItem(sOptionText, sOptionValue));

            //    ddlProductionMembers.Items.Add(new ListItem(sOptionText, sOptionValue));
            //}
        }
    }


}
