<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_LocalOne_LocalOneMyProfile"  CodeFile="~/CMSWebParts/LocalOne/LocalOneMyProfile.ascx.cs" %>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        $(".start-datepicker").datepicker();
        $('.end-datepicker').datepicker();
        //My Profile Show Modal
       
    });


</script>

<!-- Job Modal -->
<div class="modal fade" id="jobSkillsModal" tabindex="-1" role="dialog" aria-labelledby="jobSkillsModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="jobSkillsModalLabel">Job Skills</h4>
            </div>
            <div class="modal-body">
                <div>
                    <asp:CheckBoxList ID="ccblSkills" runat="server" DataTextField="Text" DataValueField="Value"></asp:CheckBoxList>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="cbSaveJobSkills" runat="server" Text="Save" OnClick="cbSaveJobSkills_Click" CssClass="save-skill-btn" />
                <a href="#a" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>


<!-- Licenses Modal -->
<div class="modal fade" id="licensesModal" tabindex="-1" role="dialog" aria-labelledby="licensesModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="licensesModalLabel">Licenses & Certifications</h4>
            </div>
            <div class="modal-body">
                <div>
                    <asp:CheckBoxList ID="ccblLicenses" runat="server" DataTextField="Text" DataValueField="Value">
                    </asp:CheckBoxList>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="cbSaveLicenses" runat="server" Text="Save" OnClick="cbSaveLicenses_Click" CssClass="save-license-btn" />
                <a href="#a" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>


<!-- Job Experience Modal -->
<div class="modal fade" id="jobExpModal" tabindex="-1" role="dialog" aria-labelledby="jobExpModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <asp:UpdatePanel ID="cupJobExpModal" runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="jobExpModalLabel">Job Experience</h4>
                        <asp:HiddenField ID="chfJobExp" runat="server" />
                    </div>
                    <div class="modal-body">
                        <div>
                            <label>Venue Type: *</label><br />
                            <asp:RadioButtonList ID="crblVenueTypes" CssClass="rbl-venue-types" runat="server" DataTextField="Text" DataValueField="Value" RepeatDirection="Horizontal">
                            </asp:RadioButtonList>
                            <asp:Label ID="clVenueTypes" runat="server" CssClass="field-error"></asp:Label>
                        </div>
                        <div>
                            <div>
                                <label>Job Position: *</label>
                            </div>
                            <asp:Panel ID="cPanelJobPositions1" runat="server">
                                <div class="inline">
                                    <asp:DropDownList ID="cddlJobPositions" runat="server" CssClass="position-dropdown" DataTextField="Text" DataValueField="Value">
                                    </asp:DropDownList>
                                    <asp:Label ID="clJobPosition" runat="server" CssClass="field-error"></asp:Label>
                                </div>
                                <div class="inline">
                                    <div class="small">
                                        Don't See Your Position?
                                    </div>
                                    <div>
                                        <!--<asp:LinkButton ID="clbAddJobPosition" runat="server" CssClass="add-job-position" OnClick="clbAddJobPosition_Click" Text="Add A New Position"></asp:LinkButton>-->
                                        <a href="mailto:">Please contact (name)</a>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="cPanelJobPositions2" runat="server" Visible="false">
                                <div class="inline">
                                    <asp:TextBox ID="ctbAddJobPosition" runat="server"></asp:TextBox>
                                </div>
                                <div class="inline">
                                    <asp:Button ID="cbAddCustomPosition" runat="server" Text="Save" OnClick="cbAddCustomPosition_Click" />
                                    <asp:Button ID="cbCancelCustomPosition" runat="server" Text="Cancel" OnClick="cbCancelCustomPosition_Click" />
                                </div>
                            </asp:Panel>
                        </div>
                        <div>
                            <div>
                                <label>Venue: *</label>
                                <asp:Panel ID="cPanelVenue1" runat="server">
                                    <div class="inline">
                                        <asp:DropDownList ID="cddlJobVenues" runat="server" CssClass="venue-dropdown" DataTextField="Text" DataValueField="Value">
                                        </asp:DropDownList>
                                        <asp:Label ID="clVenue" runat="server" CssClass="field-error"></asp:Label>
                                    </div>
                                    <div class="inline">
                                        <div class="small">
                                            Don't See Your Venue?
                                        </div>
                                        <div>
                                            <!--<asp:LinkButton ID="clbAddJobVenue" runat="server" CssClass="add-venue" OnClick="clbAddJobVenue_Click" Text="Add A New Venue"></asp:LinkButton>-->
                                            <a href="mailto:">Please contact (name)</a>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="cPanelVenue2" runat="server" Visible="false">
                                    <div class="inline">
                                        <asp:TextBox ID="ctbAddJobVenue" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="inline">
                                        <asp:Button ID="cbAddCustomVenue" runat="server" Text="Save" OnClick="cbAddCustomVenue_Click" />
                                        <asp:Button ID="cbCancelCustomVenue" runat="server" Text="Cancel" OnClick="cbCancelCustomVenue_Click" />
                                    </div>
                                </asp:Panel>
                            </div>                            
                        </div>
                        <div>
                            <div class="inline">
                                <label>Start Date: *</label><br />
                                <asp:TextBox ID="ctbJobStartDate" runat="server" CssClass="start-datepicker"></asp:TextBox>
                                <asp:Label ID="clJobStateDate" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>End Date:</label><br />
                                <div>
                                    <asp:TextBox ID="ctbJobEndDate" runat="server" CssClass="end-datepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="inline">
                                <asp:CheckBox ID="ccbCurrentPosition" runat="server" CssClass="current-position" Text="Current Position" />
                            </div>
                        </div>
                        <div>
                            <div>
                                Job Description:
                            </div>
                            <div>
                                <asp:TextBox ID="ctbJobDescription" runat="server" TextMode="MultiLine" Height="140"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <asp:Button ID="cbSaveJob" runat="server" Text="Save Job" OnClick="cbSaveJob_Click" CssClass="save-job-btn" />
                        <a href="#a" data-dismiss="modal">Cancel</a>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="crepJobsInitial" EventName="ItemCommand" />
                    <asp:AsyncPostBackTrigger ControlID="clbAddJobInitial" EventName="Click" />
                    <%--<asp:AsyncPostBackTrigger ControlID="cbSaveJob" EventName="Click" />--%>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>


<section id="profile-alt-form">

    <form class="formfield-section">
        
        <div class="form-group">
            <label>UserName:</label>       
            <asp:Label runat="server" ID="ctlUserName" ></asp:Label>
            <asp:Label runat="server" ID="ctlErrorUserName" ></asp:Label>
        </div>

        <div class="form-group">
            <label>Union Card Number:</label>
            <cms:CMSTextBox runat="server" ID="ctbUnionCardNumber"  CssClass="form-control"/>
            <asp:Label runat="server" ID="ctlUnionCardNumber" CssClass="field-error"></asp:Label>
        </div>    
        
        

        <div class="form-group">
            <label>First Name:</label>
            <cms:CMSTextBox runat="server" ID="ctbFirstName"  CssClass="form-control"/>
             <asp:Label runat="server" ID="ctlFirstName" CssClass="field-error"></asp:Label>
        </div>

         <div class="form-group">
            <label>Last Name:</label>
            <cms:CMSTextBox runat="server" ID="ctbLastName"  CssClass="form-control"/>
             <asp:Label runat="server" ID="ctlLastName" CssClass="field-error"></asp:Label>
        </div>
              
        <div class="form-group">
           <label>E-Mail:</label>
           <%--<cms:EmailInput runat="server" ID="ctbEmailInput" CssClass="form-control"/>--%>
            <cms:CMSTextBox runat="server" ID="ctbEmailInput"  CssClass="form-control" TextMode="Email"/>
           <asp:Label runat="server" ID="ctlEmailInput" CssClass="field-error"></asp:Label>
        </div>

         <div class="form-group">
            <label>Street Address:</label>
            <cms:CMSTextBox runat="server" ID="ctbStreetAddress"  CssClass="form-control"/>
             <asp:Label runat="server" ID="ctlStreetAddress" CssClass="field-error"></asp:Label>
        </div>

         <div class="form-group">
            <label>City:</label>
            <cms:CMSTextBox runat="server" ID="ctbCity"  CssClass="form-control"/>
             <asp:Label runat="server" ID="ctlCity" CssClass="field-error"></asp:Label>
        </div>

         <div class="form-group">
            <label>State:</label>
            <cms:CMSTextBox runat="server" ID="ctbState"  CssClass="form-control"/>
             <asp:Label runat="server" ID="ctlState" CssClass="field-error"></asp:Label>
        </div>

         <div class="form-group">
            <label>Zip Code:</label>
            <cms:CMSTextBox runat="server" ID="ctbZipCode"  CssClass="form-control"/>
             <asp:Label runat="server" ID="ctlZipCode" CssClass="field-error"></asp:Label>
        </div>

      <%--  <div class="form-group">
            <label>Date of Birth:</label>            
             <cms:CMSTextBox runat="server" ID="ctbDateOfBirth"  CssClass="form-control"/>
            <asp:Label runat="server" ID="Label1" CssClass="field-error"></asp:Label>
        </div> --%>

         <div class="form-group">
            <label>Cell Number:</label>
            <cms:CMSTextBox runat="server" ID="ctbCellNumber"  CssClass="form-control"/>
             <asp:Label runat="server" ID="ctlCellNumber" CssClass="field-error"></asp:Label>
        </div>

        <div class="form-group">
            <label>Home Number:</label>
            <cms:CMSTextBox runat="server" ID="ctbHomeNumber"  CssClass="form-control"/>
            <asp:Label runat="server" ID="ctlHomeNumber" CssClass="field-error"></asp:Label>
        </div>

        <div class="form-group">
            <label>Messaging notification e-mail:</label>
            <cms:CMSTextBox runat="server" ID="ctbNotificationEmail" />
        </div>

        <div class="form-group gender-input">
            <label>Gender:</label>            
             <asp:RadioButtonList ID="rbGender"  runat="server" RepeatDirection="Horizontal">
                 <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                 <asp:ListItem Text="Female" Value="2"></asp:ListItem>
              </asp:RadioButtonList>
            <asp:Label runat="server" ID="ctlGender" CssClass="field-error"></asp:Label>
        </div>

        <div class="form-group">
            <label>Time Zone:</label>
            <%--<asp:DropDownList ID="ddlTimeZone" runat="server" CssClass="position-dropdown" DataTextField="Text" DataValueField="Value">
             </asp:DropDownList>--%>
            <%--<cms:TimeZoneDropDown ID="ddlTimeZone" runat="server" CssClass="position-dropdown" UseZoneNameForSelection="false" >

            </cms:TimeZoneDropDown>--%>
            <asp:Label runat="server" ID="ctlTimeZone" CssClass="field-error"></asp:Label>
        </div>             

        <div class="form-group">
            
            <asp:UpdatePanel ID="cupRegistration" runat="server">               
                <ContentTemplate>
                    <asp:Panel ID="cPanelProfile" runat="server" Visible="true">
                        <div id="create-profile">
                            <div class="form">
                                <div class="form-section item-objects" >
                                    <h3>Job Skills:</h3>
                                    <asp:Repeater ID="crepJobSkillsInitial" runat="server">
                                        <ItemTemplate>
                                            <p><%# Eval("Text") %></p>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:LinkButton ID="clbAddSkillsInitial"   runat="server" Text="Add Job Skills" CssClass="add-skills-btn"></asp:LinkButton>
                                </div>
                                <div class="form-section item-objects">
                                    <h3>Licenses & Certifications:</h3>
                                    <asp:Repeater ID="crepLicensesInitial" runat="server">
                                        <ItemTemplate>
                                            <p><%# Eval("Text") %></p>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:LinkButton ID="clbAddLicensesInitial" runat="server"  Text="Add Licenses & Certifications" CssClass="add-licenses-btn"></asp:LinkButton>
                                </div>
                                <div class="form-section">
                                    <h3>Job Experience:</h3>
                                    <asp:Repeater ID="crepJobsInitial" runat="server"  OnItemCommand="crepJobsInitial_ItemCommand">
                                        <HeaderTemplate>
                                            <div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="item item-objects">
                                                <div class="delete-btn-container">
                                                    <asp:ImageButton ID="cibDeleteJobInitial" runat="server" ImageUrl="~/EcommerceSite/images/fancy_close.png" CommandName="delete" CommandArgument='<%# Container.ItemIndex %>' OnClientClick="return confirm('Are you sure you want to delete this job?');" />
                                                </div>
                                                <p><%# Eval("Venue") %></p>
                                                <p><%# Eval("VenueType") %></p>
                                                <p><%# Eval("Position") %></p>                                        
                                                <p><%# Eval("StartDate") %> - <%# (Eval("CurrentPosition").ToString() == "0") ? Eval("EndDate") : "Current" %></p>
                                                <asp:LinkButton ID="clbEditJobInitial" ClientID="clbEditJobInitial" runat="server" CommandName="edit" CommandArgument='<%# Container.ItemIndex %>'>Edit</asp:LinkButton>

                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <%--<input type="button" id="add-job-btn" value="+ Add a Job" class="btn" data-toggle="modal" data-target="#jobExpModal" />--%>
                                    <asp:LinkButton ID="clbAddJobInitial"  ClientID="clbAddJobInitial" runat="server" Text="Add a Job" CssClass="add-job-btn" OnClick="clbAddJobInitial_Click"></asp:LinkButton>
                                </div>

                            </div>
                        </div>
                    </asp:Panel>

                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="cbSaveJob" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="cbSaveLicenses" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="cbSaveJobSkills" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="cbUpdateProfile" EventName="Click" />               

                </Triggers>
            </asp:UpdatePanel>
        </div>
        
        <div class="form-group">
       <cms:CMSButton ID="cbUpdateProfile" runat="server"  Text="Submt" OnClick="cbUpdateProfile_Click"/>
        </div>

    </form>
</section>


