<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_LocalOne_registrationform_member" CodeFile="~/CMSWebParts/LocalOne/registrationform_member.ascx.cs" %>
<%@ Register Src="~/CMSFormControls/Captcha/SecurityCode.ascx" TagName="SecurityCode"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Membership/FormControls/Passwords/PasswordStrength.ascx" TagName="PasswordStrength"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Inputs/EmailInput.ascx" TagName="EmailInput"
    TagPrefix="cms" %>
<asp:Label ID="lblError" runat="server" ForeColor="red" EnableViewState="false" />
<asp:Label ID="lblText" runat="server" Visible="false" EnableViewState="false" />
<asp:Panel ID="pnlForm" runat="server" DefaultButton="btnOK">
    <div class="registration-form">
        <asp:MultiView runat="server" ID="mvRegistrationForm" ActiveViewIndex="0">
            <!-- Step 1 -->
            <asp:View runat="server" ID="vStep1">
                <div>Step 1</div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label CssClass="control-label" ID="lblEmail" runat="server" AssociatedControlID="txtEmail" EnableViewState="false" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:EmailInput ID="txtEmail" runat="server" />
                        <br />
                        <cms:CMSRequiredFieldValidator ID="rfvEmail" runat="server" ValidationGroup="vgStep1" Display="Dynamic" EnableViewState="false" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label CssClass="control-label" ID="lblEmailConfirm" runat="server" AssociatedControlID="txtEmailConfirm" EnableViewState="false" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:EmailInput ID="txtEmailConfirm" runat="server" />
                        <br />
                        <cms:CMSRequiredFieldValidator ID="rfvEmailConfirm" runat="server" Display="Dynamic" ValidationGroup="vgStep1" ControlToValidate="txtEmailConfirm" EnableViewState="false" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label CssClass="control-label" ID="lblUnionCard" runat="server" AssociatedControlID="txtUnionCardNumber" EnableViewState="false" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:CMSTextBox ID="txtUnionCardNumber" EnableEncoding="true" runat="server" MaxLength="100" /><br />
                    </div>
                </div>
                <div>
                    <asp:Button runat="server" ID="btnStep1" Text="Proceed to Registration" OnClick="btnStep1_Click" ValidationGroup="vgStep1" />
                </div>
            </asp:View>
            <!-- Step 2 -->
            <asp:View runat="server" ID="vStep2">
                <div>Ceate your password</div>
                <div>Please create a password to begin creating your account.</div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <cms:LocalizedLabel CssClass="control-label" ID="lblPassword" runat="server" EnableViewState="false" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:PasswordStrength runat="server" ID="passStrength" ShowValidationOnNewLine="true" ValidationGroup="vgStep2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label CssClass="control-label" ID="lblConfirmPassword" runat="server" AssociatedControlID="txtConfirmPassword"
                            EnableViewState="false" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:CMSTextBox ID="txtConfirmPassword" runat="server" TextMode="Password" MaxLength="100" /><br />
                        <cms:CMSRequiredFieldValidator ID="rfvConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                            Display="Dynamic" EnableViewState="false" ValidationGroup="vgStep2" />
                    </div>
                </div>
                <div>
                    <asp:Button runat="server" ID="btnStep2" Text="Save & Continue" OnClick="btnStep2_Click" ValidationGroup="vgStep2" />
                </div>
            </asp:View>
            <asp:View runat="server" ID="vStep3">
                <div>Create your profile</div>
                <div>Please enter your personal information and job skills, licenses, certifications or jobs.</div>
                <div>Personal Information</div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label CssClass="control-label" ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName" EnableViewState="false" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:CMSTextBox ID="txtFirstName" EnableEncoding="true" runat="server" MaxLength="100" /><br />
                        <cms:CMSRequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                            Display="Dynamic" EnableViewState="false" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label CssClass="control-label" ID="lblLastName" runat="server" AssociatedControlID="txtLastName" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:CMSTextBox ID="txtLastName" EnableEncoding="true" runat="server" MaxLength="100" /><br />
                        <cms:CMSRequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                            Display="Dynamic" EnableViewState="false" />
                    </div>
                </div>
                <!-- Address -->
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label Text="Street Address" runat="server" CssClass="control-label" ID="lblStreetAddress" AssociatedControlID="txtStreetAddress" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:CMSTextBox ID="txtStreetAddress" EnableEncoding="true" runat="server" MaxLength="100" /><br />
                        <cms:CMSRequiredFieldValidator ID="rfvStreetAddress" runat="server" ControlToValidate="txtStreetAddress"
                            Display="Dynamic" EnableViewState="false" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label Text="City" runat="server" CssClass="control-label" ID="lblCity" AssociatedControlID="txtCity" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:CMSTextBox ID="txtCity" EnableEncoding="true" runat="server" MaxLength="100" /><br />
                        <cms:CMSRequiredFieldValidator ID="CMSRequiredFieldValidator1" runat="server" ControlToValidate="txtCity"
                            Display="Dynamic" EnableViewState="false" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label Text="State" runat="server" CssClass="control-label" ID="lblState" AssociatedControlID="ddlState" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:CMSDropDownList runat="server" ID="ddlState" DataTextField="StateDisplayName" DataValueField="StateDisplayName">
                            <asp:ListItem Text="Select" Value="-1" />
                        </cms:CMSDropDownList>
                        <br />
                        <cms:CMSRequiredFieldValidator ID="CMSRequiredFieldValidator2" runat="server" ControlToValidate="txtCity"
                            Display="Dynamic" EnableViewState="false" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label Text="Zip Code" runat="server" CssClass="control-label" ID="lblZipCode" AssociatedControlID="txtZipCode" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:CMSTextBox ID="txtZipCode" EnableEncoding="true" runat="server" MaxLength="100" /><br />
                        <cms:CMSRequiredFieldValidator ID="CMSRequiredFieldValidator3" runat="server" ControlToValidate="txtZipCode"
                            Display="Dynamic" EnableViewState="false" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label Text="Cell Number" runat="server" CssClass="control-label" ID="lblCellNumber" AssociatedControlID="txtCellNumber" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:CMSTextBox ID="txtCellNumber" EnableEncoding="true" runat="server" MaxLength="100" /><br />
                        <cms:CMSRequiredFieldValidator ID="CMSRequiredFieldValidator4" runat="server" ControlToValidate="txtCellNumber"
                            Display="Dynamic" EnableViewState="false" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="editing-form-label-cell">
                        <asp:Label Text="Home Number" runat="server" CssClass="control-label" ID="lblHomeNumber" AssociatedControlID="txtHomeNumber" />
                    </div>
                    <div class="editing-form-value-cell">
                        <cms:CMSTextBox ID="txtHomeNumber" EnableEncoding="true" runat="server" MaxLength="100" /><br />
                        <cms:CMSRequiredFieldValidator ID="CMSRequiredFieldValidator5" runat="server" ControlToValidate="txtHomeNumber"
                            Display="Dynamic" EnableViewState="false" />
                    </div>
                </div>

                <div class="form-group" id="job_skills">
                    <div class="editing-form-label-cell">
                        <span>Job Skills: optional</span>
                    </div>
                    <div class="cat-list">
                        <span>Display list here</span>
                    </div>
                    <div class="editing-form-value-cell">
                        <a href="#" class="add-cat add-job-skills">+ Add job Skils</a>
                    </div>
                    <div class="show-modal">
                        List of categories
                    </div>
                </div>

                <!-- Licenses & Certificates -->
                <div class="form-group" id="job_licenses">
                    <div class="editing-form-label-cell">
                        <span>Licenses & Certifications: optional</span>
                    </div>
                    <div class="cat-list">
                        <span>Display list here</span>
                    </div>
                    <div class="editing-form-value-cell">
                        <a href="#" class="add-cat add-license-certificates">+ Add job Skils</a>
                    </div>
                    <div class="show-modal">
                        List of Licenses & Certificates
                    </div>
                </div>

                <!-- Job Experiences -->
                <div class="form-group" id="job_experiences">
                    <div class="editing-form-label-cell">
                        <span>Job Experience: optional</span>
                    </div>
                    <div class="cat-list">
                        <span>Display list here</span>
                    </div>
                    <div class="editing-form-value-cell">
                        <a href="#" class="add-cat add-job-experience">+ Add a Job</a>
                    </div>
                    <div class="show-modal">
                        List of Job Experiences
                    </div>
                </div>


                <div>
                    <asp:Button runat="server" ID="btnStep3" Text="Save & Continue" OnClick="btnStep3_Click" ValidationGroup="vgStep3" />
                </div>
            </asp:View>
            <asp:View runat="server" ID="vStep4">
                <div>Confirm Your Information</div>
                <div>Before we create your account, please confirm the information you have entered.</div>
                <div style="display: none;">
                    <asp:Button runat="server" ID="btnStep4" Text="Save & Create Account" OnClick="btnStep4_Click" />
                </div>
                <asp:PlaceHolder runat="server" ID="plcCaptcha">
                    <div class="form-group">
                        <div class="editing-form-label-cell">
                            <asp:Label CssClass="control-label" ID="lblCaptcha" runat="server" AssociatedControlID="scCaptcha" EnableViewState="false" />
                        </div>
                        <div class="editing-form-value-cell">
                            <cms:SecurityCode ID="scCaptcha" GenerateNumberEveryTime="false" ShowInfoLabel="False" runat="server" />
                        </div>
                    </div>
                </asp:PlaceHolder>
                <div class="form-group form-group-submit">
                    <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" ButtonStyle="Default"
                        EnableViewState="false" />
                </div>
            </asp:View>
        </asp:MultiView>
        <div class="form-horizontal">

            <asp:PlaceHolder runat="server" ID="plcMFIsRequired" Visible="false">
                <div class="form-group">
                    <div class="editing-form-value-cell editing-form-value-cell-offset">
                        <cms:CMSCheckBox ID="chkUseMultiFactorAutentization" runat="server" ResourceString="webparts_membership_registrationform.mfrequired" />
                    </div>
                </div>
            </asp:PlaceHolder>

        </div>
    </div>
</asp:Panel>
<style>
    .registration-form {
        background-color: #fff;
        padding: 20px;
    }
</style>
