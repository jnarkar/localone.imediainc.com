﻿using System;

using CMS.FormEngine;
using CMS.Helpers;
using CMS.PortalEngine.Web.UI;
using System.Collections.Generic;
using CMS.Base.Web.UI;
using System.Web.UI;

public partial class CMSWebParts_LocalOne_myprofile_localone2 : CMSAbstractWebPart
{
    #region "Public properties"

    /// <summary>
    /// Gets or sets the name of alternative form
    /// Default value is cms.user.EditProfile
    /// </summary>
    /// 
    [Serializable]
    public class JobExperience
    {
        public int ID { get; set; }
        public string VenueType { get; set; }
        public string VenueTypeID { get; set; }
        public string Position { get; set; }
        public string Venue { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool CurrentPosition { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }
    [Serializable]
    public class JobValuePair
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }


    public string AlternativeFormName
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("AlternativeFormName"), "cms.user.EditProfile");
        }
        set
        {
            SetValue("AlternativeFormName", value);
            myProfile.AlternativeFormName = value;
        }
    }

    /// <summary>
    /// Indicates if field visibility could be edited on user form.
    /// </summary>
    public bool AllowEditVisibility
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("AllowEditVisibility"), myProfile.AllowEditVisibility);
        }
        set
        {
            SetValue("AllowEditVisibility", value);
            myProfile.AllowEditVisibility = value;
        }
    }


    /// <summary>
    /// Relative URL where user is redirected, after form content is successfully modified.
    /// </summary>
    public string AfterSaveRedirectURL
    {
        get
        {
            return ValidationHelper.GetString(GetValue("AfterSaveRedirectURL"), String.Empty);
        }
        set
        {
            SetValue("AfterSaveRedirectURL", value);
        }
    }


    /// <summary>
    /// Submit button label. Valid input is a resource string. Default value is general.submit.
    /// </summary>
    public string SubmitButtonResourceString
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("SubmitButtonResourceString"), "general.submit");
        }
        set
        {
            SetValue("SubmitButtonResourceString", value);
        }
    }


    /// <summary>
    /// Displays required field mark next to field labels if fields are required. Default value is false.
    /// </summary>
    public bool MarkRequiredFields
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("MarkRequiredFields"), false);
        }
        set
        {
            SetValue("MarkRequiredFields", value);
        }
    }


    /// <summary>
    /// Displays colon behind label text in form. Default value is true.
    /// </summary>
    public bool UseColonBehindLabel
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("UseColonBehindLabel"), true);
        }
        set
        {
            SetValue("UseColonBehindLabel", value);
        }
    }


    /// <summary>
    /// Form CSS class.
    /// </summary>
    public string FormCSSClass
    {
        get
        {
            return ValidationHelper.GetString(GetValue("FormCSSClass"), String.Empty);
        }
        set
        {
            SetValue("FormCSSClass", value);
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        myProfile.IsLiveSite = true;

        if (StopProcessing)
        {
            myProfile.StopProcessing = true;
        }
        else
        {
            // Get alternative form info
            AlternativeFormInfo afi = AlternativeFormInfoProvider.GetAlternativeFormInfo(AlternativeFormName);
            if (afi != null)
            {
                myProfile.AlternativeFormName = AlternativeFormName;
                myProfile.AllowEditVisibility = AllowEditVisibility;
                myProfile.AfterSaveRedirectURL = AfterSaveRedirectURL;
                myProfile.SubmitButtonResourceString = SubmitButtonResourceString;
                myProfile.FormCSSClass = FormCSSClass;
                myProfile.MarkRequiredFields = MarkRequiredFields;
                myProfile.UseColonBehindLabel = UseColonBehindLabel;
                myProfile.ValidationErrorMessage = GetString("general.errorvalidationerror");
            }
            else
            {
                lblError.Text = String.Format(GetString("altform.formdoesntexists"), AlternativeFormName);
                lblError.Visible = true;
                plcContent.Visible = false;
            }
        }
    }


    /// <summary>
    /// Reload data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }
    protected void cbCreateAccount_Click(object sender, EventArgs e)
    { }
    protected void cbSaveProfile_Click(object sender, EventArgs e)
    {
        lblError.Visible = false;

        //if (ValidateInformation())
        //{

        cPanelProfile.Visible = true;
        cPanelConfirmInfo.Visible = true;



        // Copy values from 2nd to 3rd screen
        FinalizeValues();
        //}
    }

    protected void FinalizeValues()
    {


        List<JobValuePair> SkillsList = (List<JobValuePair>)ViewState["SkillsList"];
        if (SkillsList != null && SkillsList.Count > 0)
        {
            crepJobSkillsFinal.DataSource = SkillsList;
            crepJobSkillsFinal.DataBind();
            //clbEditJobsFinal.Text = "Edit";
        }
        else
        {
            //clbEditJobsFinal.Text = "Add";
        }

        List<JobValuePair> LicenseList = (List<JobValuePair>)ViewState["LicenseList"];
        if (LicenseList != null && LicenseList.Count > 0)
        {
            crepLicensesFinal.DataSource = LicenseList;
            crepLicensesFinal.DataBind();
            //clbEditLicensesFinal.Text = "Edit";
        }
        else
        {
            //clbEditLicensesFinal.Text = "Add";
        }

        List<JobExperience> JobExpList = (List<JobExperience>)ViewState["JobExpList"];
        if (JobExpList != null)
        {
            crepJobsFinal.DataSource = JobExpList;
            crepJobsFinal.DataBind();
        }


    }

    protected void clbAddJobVenue_Click(object sender, EventArgs e)
    {
        cPanelVenue1.Visible = false;
        cPanelVenue2.Visible = true;


    }

    protected void cbAddCustomVenue_Click(object sender, EventArgs e)
    {
        cPanelVenue1.Visible = true;
        cPanelVenue2.Visible = false;

        cddlJobVenues.Items.Insert(1, ctbAddJobVenue.Text.Trim());
        cddlJobVenues.SelectedIndex = 1;
    }

    protected void cbCancelCustomVenue_Click(object sender, EventArgs e)
    {
        cPanelVenue1.Visible = true;
        cPanelVenue2.Visible = false;
    }

    protected void clbAddJobPosition_Click(object sender, EventArgs e)
    {
        cPanelJobPositions1.Visible = false;
        cPanelJobPositions2.Visible = true;
    }

    protected void cbAddCustomPosition_Click(object sender, EventArgs e)
    {
        cPanelJobPositions1.Visible = true;
        cPanelJobPositions2.Visible = false;

        cddlJobPositions.Items.Insert(1, ctbAddJobPosition.Text.Trim());
        cddlJobPositions.SelectedIndex = 1;
    }

    protected void cbCancelCustomPosition_Click(object sender, EventArgs e)
    {
        cPanelJobPositions1.Visible = true;
        cPanelJobPositions2.Visible = false;
    }

    protected void cbSaveJobSkills_Click(object sender, EventArgs e)
    {
        List<JobValuePair> SkillsList = new List<JobValuePair>(0);

        var selectedItems = ccblSkills.GetSelectedItems();
        if (selectedItems != null)
        {
            foreach (var skill in selectedItems)
            {
                SkillsList.Add(new JobValuePair { Text = skill.Text, Value = skill.Value });
            }
        }

        if (SkillsList.Count > 0)
        {
            clbAddSkillsInitial.Text = "Edit";
            //clbAddSkillsFinal.Text = "Edit";
        }
        else
        {
            clbAddSkillsInitial.Text = "Add Job Skills";
            //clbAddSkillsFinal.Text = "Add Job Skills";
        }

        ViewState["SkillsList"] = SkillsList;
        crepJobSkillsInitial.DataSource = SkillsList;
        crepJobSkillsInitial.DataBind();

        crepJobSkillsFinal.DataSource = SkillsList;
        crepJobSkillsFinal.DataBind();
    }

    protected void cbSaveLicenses_Click(object sender, EventArgs e)
    {
        List<JobValuePair> LicenseList = new List<JobValuePair>(0);

        var selectedItems = ccblLicenses.GetSelectedItems();
        if (selectedItems != null)
        {
            foreach (var license in selectedItems)
            {
                LicenseList.Add(new JobValuePair { Text = license.Text, Value = license.Value });
            }
        }

        if (LicenseList.Count > 0)
        {
            clbAddLicensesInitial.Text = "Edit";
            //clbAddLicensesFinal.Text = "Edit";
        }
        else
        {
            clbAddLicensesInitial.Text = "Add Licenses & Certifications";
            //clbAddLicensesFinal.Text = "Add Licenses & Certifications";
        }

        ViewState["LicenseList"] = LicenseList;
        crepLicensesInitial.DataSource = LicenseList;
        crepLicensesInitial.DataBind();

        crepLicensesFinal.DataSource = LicenseList;
        crepLicensesFinal.DataBind();
    }

    protected void cbSaveJob_Click(object sender, EventArgs e)
    {

        List<JobExperience> SavedJobs = (List<JobExperience>)ViewState["JobExpList"];

        string status = "";

        if (ccbCurrentPosition.Checked)
            status = "Current";
        else
            status = ctbJobEndDate.Text;

        //DateTime? dtEnd;
        //if (ctbJobEndDate.Text == "")
        //    dtEnd = null;
        //else
        //    dtEnd = Convert.ToDateTime(ctbJobEndDate.Text);

        bool bEditMode = (bool)ViewState["EditMode"];
        if (bEditMode)
        {
            int? ID = Convert.ToInt32(chfJobExp.Value);
            if (ID != null && SavedJobs != null && SavedJobs.Count > 0)
            {
                JobExperience job = SavedJobs[Convert.ToInt32(ID)];
                if (job != null)
                {
                    job.VenueType = crblVenueTypes.SelectedItem.Text;
                    job.VenueTypeID = crblVenueTypes.SelectedValue;
                    job.Position = cddlJobPositions.SelectedItem.Text;
                    job.Venue = cddlJobVenues.SelectedItem.Text;
                    job.StartDate = ctbJobStartDate.Text.Trim();
                    job.EndDate = ctbJobEndDate.Text.Trim();
                    job.CurrentPosition = ccbCurrentPosition.Checked;
                    job.Description = ctbJobDescription.Text.Trim();
                    job.Status = status;

                    ViewState["JobExpList"] = SavedJobs;
                    crepJobsInitial.DataSource = SavedJobs;
                    crepJobsInitial.DataBind();

                    crepJobsFinal.DataSource = SavedJobs;
                    crepJobsFinal.DataBind();

                    ViewState["EditMode"] = false;
                }
            }

        }
        else
        {
            List<JobExperience> JobExpList = new List<JobExperience>(0);

            if (SavedJobs != null && SavedJobs.Count > 0)
            {
                foreach (JobExperience job in SavedJobs)
                {
                    JobExpList.Add(job);
                }
            }

            JobExpList.Add(new JobExperience
            {
                ID = JobExpList.Count + 1,
                VenueType = crblVenueTypes.SelectedItem.Text,
                VenueTypeID = crblVenueTypes.SelectedValue,
                Position = cddlJobPositions.SelectedItem.Text,
                Venue = cddlJobVenues.SelectedItem.Text,
                StartDate = ctbJobStartDate.Text.Trim(),
                EndDate = ctbJobEndDate.Text.Trim(),
                CurrentPosition = ccbCurrentPosition.Checked,
                Description = ctbJobDescription.Text.Trim(),
                Status = status
            });

            ViewState["JobExpList"] = JobExpList;

            crepJobsInitial.DataSource = JobExpList;
            crepJobsInitial.DataBind();

            crepJobsFinal.DataSource = JobExpList;
            crepJobsFinal.DataBind();
        }
    }

    protected void crepJobsInitial_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
    {
        List<JobExperience> JobExpList = (List<JobExperience>)ViewState["JobExpList"];
        if (JobExpList != null && JobExpList.Count > 0)
        {
            int ID = Convert.ToInt32(e.CommandArgument);
            JobExperience job = JobExpList[ID];

            if (job != null)
            {
                if (e.CommandName == "edit")
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Open Jobs Initial", "openJobsInitial();", true);

                    crblVenueTypes.SelectedValue = job.VenueTypeID;
                    cddlJobPositions.SelectedItem.Text = job.Position;
                    cddlJobVenues.SelectedItem.Text = job.Venue;
                    ctbJobStartDate.Text = job.StartDate;
                    ctbJobEndDate.Text = job.EndDate;
                    ccbCurrentPosition.Checked = job.CurrentPosition;
                    ctbJobDescription.Text = job.Description;
                    chfJobExp.Value = ID.ToString();

                    ViewState["EditMode"] = true;

                }
                else if (e.CommandName == "delete")
                {
                    JobExpList.Remove(job);

                    crepJobsInitial.DataSource = JobExpList;
                    crepJobsInitial.DataBind();

                    crepJobsFinal.DataSource = JobExpList;
                    crepJobsFinal.DataBind();
                }
            }
        }
    }


    protected void clbEditInfoFinal_Click(object sender, EventArgs e)
    {
        //ctbFirstNameModal.Text = ctbFirstName.Text.Trim();
    }

    protected void clbAddJobInitial_Click(object sender, EventArgs e)
    {
        crblVenueTypes.ClearSelection();
        cddlJobPositions.ClearSelection();
        cddlJobVenues.ClearSelection();
        ctbAddJobPosition.Text = "";
        ctbAddJobVenue.Text = "";
        ctbJobStartDate.Text = "";
        ctbJobEndDate.Text = "";
        ccbCurrentPosition.Checked = false;
        ctbJobDescription.Text = "";
    }

    protected void clbAddJobFinal_Click(object sender, EventArgs e)
    {
        crblVenueTypes.ClearSelection();
        cddlJobPositions.ClearSelection();
        cddlJobVenues.ClearSelection();
        ctbAddJobPosition.Text = "";
        ctbAddJobVenue.Text = "";
        ctbJobStartDate.Text = "";
        ctbJobEndDate.Text = "";
        ccbCurrentPosition.Checked = false;
        ctbJobDescription.Text = "";
    }

}