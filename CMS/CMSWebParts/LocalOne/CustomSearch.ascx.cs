using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;
using CMS.DataEngine;
using CMS.CustomTables;

public partial class CMSWebParts_LocalOne_CustomSearch : CMSAbstractWebPart
{
    #region "Properties"


   #endregion


   #region "Methods"

   /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


   /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            // Gets the custom table
            DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
            if (customTable != null)
            {
               // Gets the first custom table record whose value in the 'ItemName' field is equal to "SampleName"
                var item2 = CustomTableItemProvider.GetItems(customTableClassName);
                repItems.DataSource = item2;
				repItems.TransformationName = "customtable.RegisteredMemberJobs.MemberDirectory";
                repItems.DataBind();
            }
        }
    }

    private static string customTableClassName = "customtable.RegisteredMemberJobs";

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string customTableClassName = "customtable.RegisteredMemberJobs";
        // Gets the custom table
        DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
        if (customTable != null)
        {
            // Gets the first custom table record whose value in the 'ItemName' field is equal to "SampleName"
            var item2 = CustomTableItemProvider.GetItems(customTableClassName).Where(String.Format("Name like '%{0}%' OR Venue like '%{0}%' OR JobPosition like '%{0}%' OR Phone like '%{0}%'", txtSearchBox.Text));
            repItems.DataSource = item2;
            repItems.TransformationName = "customtable.RegisteredMemberJobs.MemberDirectory";
            repItems.DataBind();
        }
    }
    
   /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }

   #endregion
}

