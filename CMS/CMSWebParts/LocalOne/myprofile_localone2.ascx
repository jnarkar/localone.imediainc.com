﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="myprofile_localone2.ascx.cs" Inherits="CMSWebParts_LocalOne_myprofile_localone2" %>

<%@ Register Src="~/CMSModules/Membership/Controls/MyProfile.ascx" TagName="MyProfile" TagPrefix="cms" %>

<asp:Label ID="lblError" CssClass="ErrorLabel" runat="server" Visible="false" EnableViewState="false" />
<asp:PlaceHolder id="plcContent" runat="server">
    <cms:MyProfile ID="myProfile" runat="server" />
</asp:PlaceHolder>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_LocalOne_myprofile_localone"  CodeFile="~/CMSWebParts/LocalOne/myprofile_localone.ascx.cs" %>
<%@ Register Src="~/CMSModules/Membership/Controls/MyProfile.ascx" TagName="MyProfile" TagPrefix="cms" %>

<asp:Label ID="Label1" CssClass="ErrorLabel" runat="server" Visible="false" EnableViewState="false" />
<asp:PlaceHolder id="PlaceHolder1" runat="server">
    <cms:MyProfile ID="myProfile1" runat="server" />
</asp:PlaceHolder>
<!-- Job Modal -->
<div class="modal fade" id="jobSkillsModal" tabindex="-1" role="dialog" aria-labelledby="jobSkillsModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="jobSkillsModalLabel">Job Skills</h4>
            </div>
            <div class="modal-body">
                <div>
                    <asp:CheckBoxList ID="ccblSkills" runat="server" DataTextField="Text" DataValueField="Value"></asp:CheckBoxList>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="cbSaveJobSkills" runat="server" Text="Save" OnClick="cbSaveJobSkills_Click" CssClass="save-skill-btn" />
                <a href="#a" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>


<!-- Licenses Modal -->
<div class="modal fade" id="licensesModal" tabindex="-1" role="dialog" aria-labelledby="licensesModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="licensesModalLabel">Licenses & Certifications</h4>
            </div>
            <div class="modal-body">
                <div>
                    <asp:CheckBoxList ID="ccblLicenses" runat="server" DataTextField="Text" DataValueField="Value">
                    </asp:CheckBoxList>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="cbSaveLicenses" runat="server" Text="Save" OnClick="cbSaveLicenses_Click" CssClass="save-license-btn" />
                <a href="#a" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>


<!-- Job Experience Modal -->
<div class="modal fade" id="jobExpModal" tabindex="-1" role="dialog" aria-labelledby="jobExpModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <asp:UpdatePanel ID="cupJobExpModal" runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="jobExpModalLabel">Job Experience</h4>
                        <asp:HiddenField ID="chfJobExp" runat="server" />
                    </div>
                    <div class="modal-body">
                        <div>
                            <label>Venue Type: *</label><br />
                            <asp:RadioButtonList ID="crblVenueTypes" CssClass="rbl-venue-types" runat="server" DataTextField="Text" DataValueField="Value" RepeatDirection="Horizontal">
                            </asp:RadioButtonList>
                            <asp:Label ID="clVenueTypes" runat="server" CssClass="field-error"></asp:Label>
                        </div>
                        <div>
                            <div>
                                <label>Job Position: *</label>
                            </div>
                            <asp:Panel ID="cPanelJobPositions1" runat="server">
                                <div class="inline">
                                    <asp:DropDownList ID="cddlJobPositions" runat="server" CssClass="position-dropdown" DataTextField="Text" DataValueField="Value">
                                    </asp:DropDownList>
                                    <asp:Label ID="clJobPosition" runat="server" CssClass="field-error"></asp:Label>
                                </div>
                                <%--<div class="inline">
                                    <div class="small">
                                        Don't See Your Position?
                                    </div>
                                    <div>
                                        <asp:LinkButton ID="clbAddJobPosition" runat="server" CssClass="add-job-position" OnClick="clbAddJobPosition_Click" Text="Add A New Position"></asp:LinkButton>
                                    </div>
                                </div>--%>
                            </asp:Panel>
                            <asp:Panel ID="cPanelJobPositions2" runat="server" Visible="false">
                                <div class="inline">
                                    <asp:TextBox ID="ctbAddJobPosition" runat="server"></asp:TextBox>
                                </div>
                                <div class="inline">
                                    <asp:Button ID="cbAddCustomPosition" runat="server" Text="Save" OnClick="cbAddCustomPosition_Click" />
                                    <asp:Button ID="cbCancelCustomPosition" runat="server" Text="Cancel" OnClick="cbCancelCustomPosition_Click" />
                                </div>
                            </asp:Panel>
                        </div>
                        <div>
                            <div>
                                <label>Venue: *</label>
                                <asp:Panel ID="cPanelVenue1" runat="server">
                                    <div class="inline">
                                        <asp:DropDownList ID="cddlJobVenues" runat="server" CssClass="venue-dropdown" DataTextField="Text" DataValueField="Value">
                                        </asp:DropDownList>
                                        <asp:Label ID="clVenue" runat="server" CssClass="field-error"></asp:Label>
                                    </div>
                                    <%--<div class="inline">
                                        <div class="small">
                                            Don't See Your Venue?
                                        </div>
                                        <div>
                                            <asp:LinkButton ID="clbAddJobVenue" runat="server" CssClass="add-venue" OnClick="clbAddJobVenue_Click" Text="Add A New Venue"></asp:LinkButton>
                                        </div>
                                    </div>--%>
                                </asp:Panel>
                                <asp:Panel ID="cPanelVenue2" runat="server" Visible="false">
                                    <div class="inline">
                                        <asp:TextBox ID="ctbAddJobVenue" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="inline">
                                        <asp:Button ID="cbAddCustomVenue" runat="server" Text="Save" OnClick="cbAddCustomVenue_Click" />
                                        <asp:Button ID="cbCancelCustomVenue" runat="server" Text="Cancel" OnClick="cbCancelCustomVenue_Click" />
                                    </div>
                                </asp:Panel>
                            </div>                            
                        </div>
                        <div>
                            <div class="inline">
                                <label>Start Date: *</label><br />
                                <asp:TextBox ID="ctbJobStartDate" runat="server" CssClass="start-datepicker"></asp:TextBox>
                                <asp:Label ID="clJobStateDate" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>End Date:</label><br />
                                <div>
                                    <asp:TextBox ID="ctbJobEndDate" runat="server" CssClass="end-datepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="inline">
                                <asp:CheckBox ID="ccbCurrentPosition" runat="server" CssClass="current-position" Text="Current Position" />
                            </div>
                        </div>
                        <div>
                            <div>
                                Job Description:
                            </div>
                            <div>
                                <asp:TextBox ID="ctbJobDescription" runat="server" TextMode="MultiLine" Height="140"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <asp:Button ID="cbSaveJob" runat="server" Text="Save Job" OnClick="cbSaveJob_Click" CssClass="save-job-btn" />
                        <a href="#a" data-dismiss="modal">Cancel</a>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="crepJobsInitial" EventName="ItemCommand" />
                    <asp:AsyncPostBackTrigger ControlID="clbAddJobInitial" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="clbAddJobFinal" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>


<!--Form Start-->

 <asp:Panel ID="cPanelProfile" runat="server" Visible="true">
            <div id="create-profile">
                <div class="form">
                    <div class="form-section">
                        <h3>Create your profile</h3>
                        <p>Please enter your personal information about job skills, licenses, certifications or jobs.</p>
                    </div>
                    <div class="form-section">
                        <h3>Personal Information</h3>
                        <div>
                            <label>Name: *</label><br />
                            <div class="inline">
                                <cms:CMSTextBox ID="ctbFirstName" EnableEncoding="true" runat="server" MaxLength="100" CssClass="ctbFirstName" />
                                <label>First</label>
                                <asp:Label ID="clFirstName" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <cms:CMSTextBox ID="ctbLastName" EnableEncoding="true" runat="server" MaxLength="100" CssClass="ctbLastName" />
                                <label>Last</label>
                                <asp:Label ID="clLastName" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                        </div>
                        <div>
                            <label>Street Address: *</label><br />
                            <cms:CMSTextBox ID="ctbAddress" runat="server" CssClass="ctbAddress"></cms:CMSTextBox>
                            <asp:Label ID="clAddress" runat="server" CssClass="field-error"></asp:Label>
                        </div>
                        <div>
                            <div class="inline">
                                <label>City: *</label><br />
                                <cms:CMSTextBox ID="ctbCity" runat="server" CssClass="ctbCity"></cms:CMSTextBox>
                                <asp:Label ID="clCity" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>State: *</label><br />
                                <cms:CMSTextBox ID="ctbState" runat="server" CssClass="ctbState"></cms:CMSTextBox>
                                <asp:Label ID="clState" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>Zip Code: *</label><br />
                                <cms:CMSTextBox ID="ctbZipCode" runat="server" CssClass="ctbZipCode"></cms:CMSTextBox>
                                <asp:Label ID="clZipCode" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                        </div>
                        <div>
                            <div class="inline">
                                <label>Cell Number: *</label><br />
                                <cms:CMSTextBox ID="ctbCellNumber" runat="server" CssClass="ctbCellNumber"></cms:CMSTextBox>
                                <asp:Label ID="clCellNumber" runat="server" CssClass="field-error"></asp:Label>
                            </div>
                            <div class="inline">
                                <label>Home Number:</label>
                                <br />
                                <cms:CMSTextBox ID="ctbHomeNumber" runat="server"></cms:CMSTextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-section">
                        <h3>Job Skills:</h3>
                        <asp:Repeater ID="crepJobSkillsInitial" runat="server">
                            <ItemTemplate>
                                <p><%# Eval("Text") %></p>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:LinkButton ID="clbAddSkillsInitial" runat="server" Text="Add Job Skills" CssClass="add-skills-btn"></asp:LinkButton>
                    </div>
                    <div class="form-section">
                        <h3>Licenses & Certifications:</h3>
                        <asp:Repeater ID="crepLicensesInitial" runat="server">
                            <ItemTemplate>
                                <p><%# Eval("Text") %></p>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:LinkButton ID="clbAddLicensesInitial" runat="server" Text="Add Licenses & Certifications" CssClass="add-licenses-btn"></asp:LinkButton>
                    </div>
                    <div class="form-section">
                        <h3>Job Experience:</h3>
                        <asp:Repeater ID="crepJobsInitial" runat="server" OnItemCommand="crepJobsInitial_ItemCommand">
                            <HeaderTemplate>
                                <div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="item">
                                    <div class="delete-btn-container">
                                        <asp:ImageButton ID="cibDeleteJobInitial" runat="server" ImageUrl="~/EcommerceSite/images/fancy_close.png" CommandName="delete" CommandArgument='<%# Container.ItemIndex %>' OnClientClick="return confirm('Are you sure you want to delete this job?');" />
                                    </div>
                                    <p><%# Eval("Venue") %></p>
                                    <p><%# Eval("VenueType") %></p>
                                    <p><%# Eval("Position") %></p>
                                    <p><%# Eval("StartDate") %> - <%# Eval("Status")%></p>
                                    <asp:LinkButton ID="clbEditJobInitial" runat="server" CommandName="edit" CommandArgument='<%# Container.ItemIndex %>'>Edit</asp:LinkButton>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <%--<input type="button" id="add-job-btn" value="+ Add a Job" class="btn" data-toggle="modal" data-target="#jobExpModal" />--%>
                        <asp:LinkButton ID="clbAddJobInitial" runat="server" Text="Add a Job" CssClass="add-job-btn" OnClick="clbAddJobInitial_Click"></asp:LinkButton>
                    </div>
                    <div>
                        <cms:CMSButton ID="cbSaveProfile" runat="server" Text="Save & Continue" OnClick="cbSaveProfile_Click" CssClass="save-profile-btn" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="cPanelConfirmInfo" runat="server" Visible="false">
            <div id="confirm-info">
                <div class="form">
                    <div class="form-section">
                        <h3>Confirm Your Information</h3>
                        <p>Before we create your account, please confirm the information you have entered.</p>
                    </div>
                    <div class="form-section">
                        <div class="inline">
                            <h3>
                                <asp:Label ID="clName" runat="server"></asp:Label>
                            </h3>
                        </div>
                        <div class="inline">
                            <asp:LinkButton ID="clbEditInfoFinal" runat="server" Text="edit" OnClick="clbEditInfoFinal_Click" CssClass="edit-info-btn"></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="clCardNumber" runat="server"></asp:Label>
                            &nbsp;&nbsp; <b>Union Card Number</b><br />
                            <asp:Label ID="clStreetAddress" runat="server"></asp:Label><br />
                            <asp:Label ID="clCityStateZip" runat="server"></asp:Label><br />
                            <asp:Label ID="clCellPhone" runat="server"></asp:Label>&nbsp;&nbsp;<b>Cell</b><br />
                            <asp:Label ID="clHomePhone" runat="server"></asp:Label>&nbsp;&nbsp;<b>Home</b><br />
                            <asp:Label ID="clEmail" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form-section">
                        <div class="inline">
                            <h3>Job Skills</h3>
                        </div>
                        <div class="inline">
                            <asp:LinkButton ID="clbAddSkillsFinal" runat="server" Text="edit" CssClass="add-skills-btn"></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Repeater ID="crepJobSkillsFinal" runat="server">
                                <ItemTemplate>
                                    <p><%# Eval("Text") %></p>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="form-section">
                        <div class="inline">
                            <h3>Licenses & Certification</h3>
                        </div>
                        <div class="inline">
                            <asp:LinkButton ID="clbAddLicensesFinal" runat="server" Text="edit" CssClass="add-licenses-btn"></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Repeater ID="crepLicensesFinal" runat="server">
                                <ItemTemplate>
                                    <p><%# Eval("Text") %></p>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="form-section">
                        <h3>Job Experience</h3>
                        <asp:Repeater ID="crepJobsFinal" runat="server" OnItemCommand="crepJobsInitial_ItemCommand">
                            <HeaderTemplate>
                                <div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="item">
                                    <div class="delete-btn-container">
                                        <asp:ImageButton ID="cibDeleteJobFinal" runat="server" ImageUrl="~/EcommerceSite/images/fancy_close.png" CommandName="delete" CommandArgument='<%# Container.ItemIndex %>' OnClientClick="return confirm('Are you sure you want to delete this job?');" />
                                    </div>
                                    <p><%# Eval("Venue") %></p>
                                    <p><%# Eval("VenueType") %></p>
                                    <p><%# Eval("Position") %></p>
                                    <p><%# Eval("StartDate") %> - <%# Eval("Status")%></p>
                                    <asp:LinkButton ID="clbEditJobFinal" runat="server" CommandName="edit" CommandArgument='<%# Container.ItemIndex %>'>Edit</asp:LinkButton>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div>
                            <%--<cms:CMSButton ID="cbAddAnotherJob" runat="server" Text="+ Add Another Job" OnClick="cbAddAnotherJob_Click" />--%>
                            <asp:LinkButton ID="clbAddJobFinal" runat="server" Text="Add a Job" CssClass="add-job-btn" OnClick="clbAddJobFinal_Click"></asp:LinkButton>
                            <%--<input type="button" id="add-another-job-btn" value="+ Add Another Job" class="btn" data-toggle="modal" data-target="#jobExpModal" />--%>
                        </div>
                        <div>
                            <cms:CMSButton ID="cbCreateAccount" runat="server" Text="Save & Create Account" OnClick="cbCreateAccount_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

