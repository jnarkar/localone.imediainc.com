<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_LocalOne_StewardRegistration" CodeFile="~/CMSWebParts/LocalOne/StewardRegistration.ascx.cs" %>
<asp:Panel runat="server" ID="pnlError" Visible="false">
    <div>
    <asp:Literal runat="server" ID="ltrError"></asp:Literal>
    </div>
    <div>OR</div>
    <div>
        You can contact an administrator by <a href="mailto:jnarkar@imediainc.com">email</a>.
    </div>
</asp:Panel>
<asp:MultiView runat="server" ActiveViewIndex="0" ID="mvStewardRegistration">
    <asp:View ID="vRegistration" runat="server">
        <!-- Venue Type -->
        <div class="venue-top">
            <p>Venue Type: *</p>
            <div id="venue_types">
                <asp:Repeater runat="server" ID="rptVenueTypes">
                    <ItemTemplate>
                        <a href="#" class="venue-type"><%# Eval("VenueType") %></a>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <!-- Hidden Values -->
            <div style="display: none;">
                <asp:TextBox runat="server" CssClass="all-venues" ID="txtVenues"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="selected-venue-type" ID="txtSelectedVenueType"></asp:TextBox>
                <asp:TextBox runat="server" CssClass="selected-venue" ID="txtSelectedVenue"></asp:TextBox>
                <asp:Literal ID="ltrVenueTypes" runat="server" Visible="true" />
            </div>
        </div>
        <!-- Venue Drop down -->
        <div>
            <p>Venue: *</p>
            <div class="select-col">
                <!-- float left -->
                <div class="venue-list-wrapper">
                    <select id="select-venues">
                        <option value="-1">Select Your Venue</option>
                    </select>
                </div>
                <!-- float right -->
                <div>
                    <span>Don't See your Venue?</span>
                    <a href="mailto:jnarkar@imediainc.com">Request Administrator</a>
                </div>
            </div>
            <div style="display:none;">
                Result:
                <asp:Literal ID="ltrResult" runat="server" />
            </div>
            <!-- Send for Approval Button -->
            <div style="clear:both;">
                <asp:Button runat="server" ID="btnSendForApproval" CssClass="btn-steward-approval" Text="Send for Approval" OnClick="btnSendForApproval_Click" />
            </div>
        </div>
    </asp:View>
    <asp:View ID="vUnderApproval" runat="server">
        <div style="color:#ff6a00; font-weight: bold; padding-top: 25px;font-size:1.3em;padding:15px 0;">
            Your approval request is under process, the decision will be notified to you by email.
        </div>
    </asp:View>
    <asp:View ID="vStewardRoleWithIssues" runat="server">
        <div style="color:#ff6a00; font-weight: bold; padding-top: 25px;font-size:1.3em;padding:15px 0;">
            You are with Steward Role but you are not listed under our Approval Process. Kindly contact an adminsitrator with your query.
        </div>
    </asp:View>
    <asp:View ID="vAuthorizeError" runat="server">
        <div style="color:#ff6a00; font-weight: bold; padding-top: 25px;font-size:1.3em;padding:15px 0;">
            You are not authorize to view this page.
        </div>
    </asp:View>
    <asp:View ID="vGenericError" runat="server">
        <div style="color: red; font-weight: bold;">
            Sorry for inconvenience and we are workng this issue.
        </div>
    </asp:View>
    <asp:View ID="vSuccess" runat="server">
        <div style="color:#ff6a00; font-weight: bold; padding-top: 25px;font-size:1.3em;padding:15px 0;">
            Thanks for your approval request, the request has been sent to an Administrator. You will be notfied by email.
        </div>
    </asp:View>
</asp:MultiView>


