using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;
using CMS.Membership;
using CMS.DataEngine;
using CMS.CustomTables;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CMS.SiteProvider;
using CMS.EmailEngine;
using CMS.EventLog;

public partial class CMSWebParts_LocalOne_StewardRegistration : CMSAbstractWebPart
{
    #region "Properties"

    

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    private readonly string MemberRole = "RegisteredUser";
    private readonly string StewardRole = "LocalOneSteward";
    private readonly string ctClassVenues = "customtable.stewardjobvenues";
    private readonly string ctClassStewardApproval = "customtable.stewardregistration_approval";

    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            // get list from custom table
            CurrentUserInfo currentUser = MembershipContext.AuthenticatedUser;

            if (currentUser == null)
            {
                Response.Redirect("~/");
                return;
            }

            // check role : if the user is not Member throw them out
            if (!currentUser.IsInRole(MemberRole, SiteContext.CurrentSiteName))
            {
                mvStewardRegistration.SetActiveView(vAuthorizeError);
                return;
            }

            // check if the user is in steward role
            if (currentUser.IsInRole(StewardRole, SiteContext.CurrentSiteName))
            {
                // check if the user has approval
                DataClassInfo ctSteardApproval = DataClassInfoProvider.GetDataClassInfo(ctClassStewardApproval);
                if (ctSteardApproval != null)
                {
                    CustomTableItem ctiStewardApproval = CustomTableItemProvider.GetItems(ctClassStewardApproval)
                        .WhereEquals("UserName", currentUser.UserName)
                        .FirstObject;
                    if(ctiStewardApproval != null)
                    {
                        bool bApproved = ctiStewardApproval.GetBooleanValue("Approved", false);
                        if (bApproved)
                        {
                            // redirect to next page
                            Response.Redirect(CurrentDocument.Children[0].NodeAliasPath);
                        }
                        else
                        {
                            // show error view
                            mvStewardRegistration.SetActiveView(vUnderApproval);
                            return;
                        }
                    }
                }
                mvStewardRegistration.SetActiveView(vStewardRoleWithIssues);
                return;
            }

            // the user in MemberRole and looking forward to request for an approval
            DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(ctClassVenues);
            if (customTable != null)
            {
                // get entire list
                var lstVenues = CustomTableItemProvider.GetItems(ctClassVenues);

                if (lstVenues != null)
                {
                    var sortedVenues = lstVenues.Column("VenueType").Distinct(true).OrderByAscending("VenueType");

                    List<object> olstVenueTypes = new List<object>();
                    foreach (var item in sortedVenues)
                    {
                        olstVenueTypes.Add(new { VenueType = item.GetStringValue("VenueType", "Venue-not-found") });
                    }

                    rptVenueTypes.DataSource = olstVenueTypes;
                    rptVenueTypes.DataBind();

                    // Put JSON in Textbox
                    var lstVenuesNew = CustomTableItemProvider.GetItems(ctClassVenues)
                        .Columns(new string[] { "Venue","VenueType", "ItemGUID" });
                    txtVenues.Text = lstVenuesNew.ToJSON("venues", true);
                }
            }
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion

    protected void btnSendForApproval_Click(object sender, EventArgs e)
    {
        // get the selected value
        string selectedVenueGUID = txtSelectedVenue.Text;

        //string customTableClassNameVenues = "customtable.stewardjobvenues";

        // get Venue by GUID
        CustomTableItem selectedVenue = CustomTableItemProvider.GetItems(ctClassVenues)
            .WhereEquals("ItemGUID", selectedVenueGUID)
            .FirstObject;
        if (selectedVenue != null)
        {
            string sVenue = selectedVenue.GetStringValue("Venue", "NF");
            string sVenueType = selectedVenue.GetStringValue("VenueType", "NF");
            ltrResult.Text = sVenue;

            // check if anyone else have requested for steward for selected venue
            CustomTableItem ctiVenueAssignedToSteward = CustomTableItemProvider.GetItems(ctClassStewardApproval)
                .WhereEquals("VenueType", sVenueType)
                .WhereEquals("Venue", sVenue)
                .FirstObject;
            if (ctiVenueAssignedToSteward != null)
            {
                // show panel saying some steward already have assigned or sent approval for venue
                ltrError.Text = "Some user have already requested or working on this venue, please select another venue and try again";
                pnlError.Visible = true;
                return;
            }

            // create data row and close this panel

            CurrentUserInfo currentUser = MembershipContext.AuthenticatedUser;

            RoleInfo role = RoleInfoProvider.GetRoleInfo(StewardRole, SiteContext.CurrentSiteName);

            // check if user 
            UserRoleInfoProvider.AddUserToRole(currentUser.UserID, role.RoleID);

            // add row in custom table
            DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(ctClassStewardApproval);
            if (customTable != null)
            {
                // add new item
                CustomTableItem newSteardApproval = CustomTableItem.New(ctClassStewardApproval);
                // set values
                newSteardApproval.SetValue("UserName", currentUser.UserName);
                newSteardApproval.SetValue("VenueType", sVenueType);
                newSteardApproval.SetValue("Venue", sVenue);

                newSteardApproval.Insert();

                SendEmail();

                mvStewardRegistration.SetActiveView(vSuccess);
            }
        }else
        {
            // show error that Selected Venue is not in Database
            // show them error and ask user to reload page
        }
    }

    /// <summary>
    /// To reload the page
    /// </summary>
    private void ReloadPage()
    {
        Response.Redirect(Request.RawUrl, true);
    }

    protected void SendEmail()
    {
        string siteName = SiteContext.CurrentSiteName;

        CMS.EmailEngine.EmailMessage em = new CMS.EmailEngine.EmailMessage();
        em.EmailFormat = CMS.EmailEngine.EmailFormatEnum.Html;
        em.From = "noreply@imediainc.com";
        em.Recipients = "Abraunstein@iatse-local1.org; mcaffrey@iatse-local1.org";
        em.Subject = "Steward Approval";
        //string bodyText = "";
        em.Body = String.Format("Member {0}; {1}, is requesting steward approval.", CurrentUser.FullName, CurrentUser.UserName);

        CMS.EmailEngine.EmailSender.SendEmail(CurrentSiteName, em);

        /*
        EmailTemplateInfo template = null;
        string emailSubject = null;

        template = EmailTemplateProvider.GetEmailTemplate("", siteName);
        emailSubject = EmailHelper.GetSubject(template, "Request for an approval from Member.");

        if (template != null)
        {
            // Email message
            EmailMessage email = new EmailMessage();
            email.EmailFormat = EmailFormatEnum.Default;
            email.Recipients = "jnarkar@imediainc.com";
            email.From = SettingsKeyInfoProvider.GetValue(siteName + ".CMSNoreplyEmailAddress");
            email.Subject = emailSubject;

            try
            {
                //var resolver = MembershipResolvers.GetMembershipRegistrationResolver(ui, AuthenticationHelper.GetRegistrationApprovalUrl(ApprovalPage, ui.UserGUID, siteName, NotifyAdministrator));
                //EmailSender.SendEmailWithTemplateText(siteName, email, template, new string[,] { "a":"b","d":"c" });
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("E", "Steward RegistrationForm - SendEmail", ex);
                //error = true;
            }
        }
        */
    }
}



