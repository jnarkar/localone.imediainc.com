<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_Localone_StewardReportApproval" CodeFile="~/CMSWebParts/Localone/StewardReportApproval.ascx.cs" %>

<asp:Panel runat="server" ID="pnlUnderApprovalStatus">
    <h4>The report is currently ready for Approval.</h4>
</asp:Panel>

<asp:Panel runat="server" ID="pnlAprovedStatus">
    <h4>The report is Approved. </h4>
</asp:Panel>

<asp:Panel runat="server" ID="pnlDeniedStatus">
    <h4>The report is currently Denied. </h4>
</asp:Panel>


<asp:Panel runat="server" ID="pnlStewardDetails">
    <div>
        <h3>
            <asp:Literal runat="server" ID="ltrVenue"></asp:Literal>
        </h3>
        <div class="steward-info">
            <div class="items">
                <span class="labels">Steward:</span>
                <span class="content">
                    <asp:Literal ID="ltrStewardName" runat="server" /></span><br />
            </div>
            <div class="items">
                <span class="labels">Week Ending: </span>
                <span class="content">
                    <asp:Literal ID="ltrWeekEnding" runat="server"></asp:Literal></span>
            </div>
            <div class="items">
                <span class="labels">Report Status: </span>
                <span class="content">
                    <asp:Literal ID="ltrReportStatus" runat="server"></asp:Literal></span>
            </div>
        </div>
    </div>

</asp:Panel>

<asp:Panel runat="server" CssClass="empty-report" ID="pnlEmptyReport" Visible="false">
    <h3>There aren't any reports at this time.</h3>
</asp:Panel>

<asp:PlaceHolder ID="ReportTable" runat="server">
    <div class="jobs-wrapper">
        <div id="jobs-union-wrapper">
            <h4>Union</h4>
            <table class="tabledata union-jobs" id="data-table-union" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Name</td>
                        <td>Local #</td>
                        <td>Union Card Number</td>
                        <td>Position</td>
                        <td>Hours</td>
                        <td>Shows</td>
                    </tr>
                </thead>
                <tbody>
                    <%if (StewardReportDetailsList.Where(x => x.JobType == "Union").Count() > 0)
                        { %>

                    <% foreach (var jobListing in StewardReportDetailsList.Where(x => x.JobType == "Union"))
                        { %>
                    <tr>
                        <%# Convert.ToDateTime(Eval("StartDate").ToString()).ToShortDateString() %>
                        <td><%= jobListing.Name %></td>
                        <td><%= jobListing.LocalOneUnionCardNumber %></td>
                        <td><%= jobListing.UnionCardNumber %></td>
                        <td><%= jobListing.Position %></td>
                        <td><%= jobListing.Hours %></td>
                        <td><%= jobListing.Shows %></td>

                    </tr>
                    <% } %>
                    <%} %>
                    <%else
                        {%>
                    <tr>
                        <td colspan="8" class="empty-table">There are no jobs added to Union!</td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </div>
        <div id="jobs-production-wrapper">
            <h4>Production</h4>
            <table class="tabledata production-jobs" id="data-table-production" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Name</td>
                        <td>Local #</td>
                        <td>Union Card Number</td>
                        <td>Position</td>
                        <td>Hours</td>
                        <td>Shows</td>
                    </tr>
                </thead>
                <tbody>
                    <%if (StewardReportDetailsList.Where(x => x.JobType == "Production").Count() > 0)
                        { %>

                    <% foreach (var jobListing in StewardReportDetailsList.Where(x => x.JobType == "Production"))
                        { %>
                    <tr>
                        <td><%= jobListing.Name %></td>
                        <td><%= jobListing.LocalOneUnionCardNumber %></td>
                        <td><%= jobListing.UnionCardNumber %></td>
                        <td><%= jobListing.Position %></td>
                        <td><%= jobListing.Hours %></td>
                        <td><%= jobListing.Shows %></td>

                    </tr>
                    <% } %>
                    <%} %>
                    <%else
                        {%>
                    <tr>
                        <td colspan="8" class="empty-table">There are no jobs added to Production!</td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </div>
    </div>
</asp:PlaceHolder>
<asp:Panel runat="server" ID="pnlApproveReport">
    <div style="clear: both;">
        <asp:Button type="button" runat="server" OnClick="ApproveReport_Click" class="imedia-modal-popup btn-send-report" Text="Approve Report" />
        <asp:Button type="button" runat="server" OnClick="DenyReport_Click" class="imedia-modal-popup btn-send-report" Text="Deny Report" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Panel>

<asp:Panel runat="server" ID="pnlEmptyReportReturn" Visible="false">
    <asp:Button type="button" runat="server" OnClick="ReturnHome_Click" class="imedia-modal-popup btn-send-report" Text="Return Home" />
</asp:Panel>

