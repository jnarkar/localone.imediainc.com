
$(function($) {
    
    
    $("#job-skill-data input[checked='checked']").each(function(index){
        
        var labelObj = $(this).next();
        
        var labelText =  $(labelObj).html();
        
        var jobListElement = "<li>" + labelText + "</li>"
        
        $("#user-job-skill-listing").append(jobListElement);    

    });   
  
    
    $("#lic-cert-data input[checked='checked']").each(function(index){
        
        var labelObj = $(this).next();
        
        var labelText =  $(labelObj).html();
        
        var jobListElement = "<li>" + labelText + "</li>"
        
        $("#user-lic-cert-listing").append(jobListElement);        
        
        
    });  
  
      
    $("#job-skill-data input").click(function(){
        
        var labelObj = $(this).next();       
     
        
        if($(this).attr("checked") == "checked")
        {        
          $(this).removeAttr("checked");
        }
        else if($(this).attr("checked") != "checked")
        {
            $(this).attr("checked","checked");   
        }       
              
    });
  
   $("#lic-cert-data  input").click(function(){
        
        var labelObj = $(this).next();       

        
        if($(this).attr("checked") == "checked")
        {        
          $(this).removeAttr("checked");
        }
        else if($(this).attr("checked") != "checked")
        {
            $(this).attr("checked","checked");   
        }      
             
    });
  

  
  $.fn.UpdateCredListing = function(){
    
    var listdata  = $(this).data("listing") + " input[checked='checked']";
    
    var listingElement = $(this).data("display");
    $(listingElement + " li").empty();
    
    console.log(listdata);
    console.log(listingElement);       

    
    $(listdata).each(function(index){ 
           
        var labelObj = $(this).next();
        
        var labelText =  $(labelObj).html();
        
        var jobListElement = "<li>" + labelText + "</li>"       
            
        $(listingElement).append(jobListElement);   
        
        
    });
  
    
    }
  
})
