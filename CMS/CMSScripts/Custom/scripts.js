/// <reference path="//code.jquery.com/jquery-2.2.4.min.js" />



$('.add-skills-btn').click(function () {
    $('#jobSkillsModal').modal('show');
});

$('.add-licenses-btn').click(function () {
    $('#licensesModal').modal('show');
});
$('.add-job-btn').click(function () {
    $('#jobExpModal').modal('show');
});
/*Availability Cookie*/
$(function ($) {
    $("a.avail-btn span#avail").click(function () {
        var cookie = $.cookie('span#avail');
        /* //Test (Expire 2 Min)//
           var date = new Date();
           var minutes = 2;
           date.setTime(date.getTime() + (minutes * 60 * 1000));
        */
        if (cookie == 1) {
            $.removeCookie('span#avail')
        } else {
            $.cookie('span#avail', 1, {
                expires: 3 /* date */
            });
        }
        $(this).toggleClass('added');
        $(this).html($(this).text() == 'NO' ? 'YES' : 'NO');
    }).each(function () {
        if ($.cookie('span#avail') == 1) {
            $(this).addClass('added').text('NO')
        }
    })
})
/*END Availability Cookie*/

// Added by Jagdish
// namespace addded for some init code
var LocalOne = {
    Common: {
        Init: function (e) {
            // if you wanna initialize something from init
            LocalOne.Common.InitSomething();
        },
        InitSomething: function (e) {
            
        },
        InitModalPopup: function (e) {
            $('.imedia-modal-popup').click(function () {
                // get modal id to show
                $($(this).data("modalid")).modal("show").on('hidden.bs.modal', function (e) {
                    ModalReset();
                });
            });
        },
        FormValidation: function (e) {
            $('.form-validation').click(function () {
                // parent id
                var parentIDofForm = $(this).data("parentid");
                if ($(parentIDofForm).length > 0) {
                    // write for text required
                    var inputRequiredFields = $(parentIDofForm + ' input.text-required');
                    var formValid = true;
                    $.each(inputRequiredFields, function () {
                        
                        if ($(this).attr("type") === "text") {
                            if ($(this).val() === "") {
                                $(this).next().removeClass('hide');
                                formValid = false;
                            } else {
                                $(this).next().addClass('hide');
                            }
                        }
                    });

                    inputRequiredFields = $(parentIDofForm + ' select.select-required');
                    $.each(inputRequiredFields, function () {
                        if ($(this).val() === "-1") {
                            $(this).next().removeClass('hide');
                            formValid = false;
                        } else {
                            $(this).next().addClass('hide');
                        }
                    });
                    if (!formValid)
                        return false;
                }
            });
        }
    },
    AnotherClassHere: {
        Init: function (e) {
            // if you wanna initialize something from here
        },
        MoreFunction: function (e) {
            // keep adding your function for this class
        }
    },
    StewardRegistration: {
        InitVenues: function (objectToParse) {
            $allvenues = objectToParse;
        },
        InitJobs: function (objectToParse) {
            $alljobs = objectToParse;
        }
    },
    StewardReport: {
        Init: function (e) {
            // all the code related to StewardReport goes here
            //if ($('.select-week-jobs-exist').val() == "False") {
            //    //$('input#copy_from_prev_week').css("display", "inline-block");
            //    //$('input.btn-send-report').css("display", "none");
            //} else {
            //    //Controls display of yes button of copy modal although display of button to show modal is being handle
            //    //$('input#copy_from_prev_week').css("display", "inline-block");
            //    $('input.btn-send-report').css("display", "inline-block");
            //}
        }
    }
    //$('').siblings
};

// following variable is used for Steward Registration process
var $allvenues;
var $alljobs;

var StewardRegistration = function () {
    // parse data from textfield
    var JSONvenues = JSON.parse("{" + $('.all-venues').val() + "}");

    // stores JSON in $allvenues variable
    LocalOne.StewardRegistration.InitVenues(JSONvenues);

    /* Steward Registration Venue Selection */
    $('#venue_types a').click(function (e) {
        //event.stopPropagation();
        var $filterBy = $(this).text();

        // add/remvoe active class
        $(this).siblings().removeClass('active').end()
            .addClass("active");
        $('.selected-venue').val("");// remove selected venue
        $('.selected-venue-type').val($filterBy);

        var $filteredVenues = $allvenues.venues.filter(function (v) {
            return v.VenueType === $filterBy;
        });
        // clear all options and add select
        $('#select-venues').html("").append($("<option>", {
            value: "-1",
            text: "Select Your Venue"
        }));

        // add an options for select
        $.each($filteredVenues, function (index) {
            $('#select-venues').append($("<option>", {
                text: this.Venue,
                value: this.ItemGUID
            }));
        });

        return false;
    });

    // now on select change
    $('#select-venues').change(function () {
        $('.selected-venue').val($(this).val());
    });
}


    
var StewardReports = function (txtClass, tableId, jsonData) {
    $.fn.dataTable.ext.errMode = 'none';
    // .all-jobs-union
    // #data-table-union
    var disableReport = false;
    if ($('.report-disable').length > 0) {
        disableReport = true;
    }

    var $job_edit = '<span class="edit-btn-job"><input type="button" title="Edit" value="Edit" data-action="edit" data-modalid="#addNewJobModal" class="modal-popup job-button-action job-edit" /></span>';
    var $job_remove = '<span class="edit-btn-rem"><input type="button" title="Remove" value="Remove" data-action="remove" data-modalid="#removeJobModal" class="modal-popup job-button-action job-remove" /></span>';

    var dtTable;
    if (tableId == "#data-table-union") {
        dtTable = $(tableId).DataTable({
            "paging": false,
            data: jsonData.data,
            language: {
                emptyTable: "There are no jobs added to Union!"
            },
            columns: [
                { "data": "Name" },
                { "data": "LocalOneUnionCardNumber" },
                { "data": "UnionCardNumber" },
                { "data": "Position" },
                { "data": "Hours" },
                { "data": "Shows" },
                {

                    "render": function (data) {
                        //if ($('.current-week-report').val() == "True" && !disableReport) {
                        //    return $job_edit;
                        //} else {
                        //    return "<span>NA</span>"
                        //}
                        if ($('.selected-week-editable').val() == "True" && !disableReport) {
                            return $job_edit;
                        } else {
                            return "<span>NA</span>"
                        }
                    }
                },
                {
                    "render": function (data, type, row) {
                        //return $job_remove;
                        //if ($('.current-week-report').val() == "True" && !disableReport) {
                        //    return $job_remove;
                        //} else {
                        //    return "<span>NA</span>"
                        //}
                        if ($('.selected-week-editable').val() == "True" && !disableReport) {
                            return $job_remove;
                        } else {
                            return "<span>NA</span>"
                        }
                    }
                }
            ]
        });
    }

    if (tableId == "#data-table-production") {
        dtTable = $(tableId).DataTable({
            "paging": false,
            data: jsonData.data,
            language: {
                emptyTable: "There are no jobs added to Production!"
            },
            columns: [
                { "data": "Name" },
                { "data": "LocalOneUnionCardNumber" },
                { "data": "UnionCardNumber" },
                { "data": "Position" },
                { "data": "Hours" },
                { "data": "Shows" },
                {
                    "render": function (data) {
                        if ($('.selected-week-editable').val() == "True" && !disableReport) {
                            return $job_edit;
                        } else {
                            return "<span>NA</span>"
                        }
                    }
                },
                {
                    "render": function (data, type, row) {
                        if ($('.selected-week-editable').val() == "True" && !disableReport) {
                            return $job_remove;
                        } else {
                            return "<span>NA</span>"
                        }
                    }
                }
            ]
        });
    }

    // onclick button for datatable
    $(tableId).on('click', '.job-button-action', function (event) {
        event.preventDefault();
        var dataRow = dtTable.row($(this).parents('tr')).data();
        

        // get brnaction and modal div id to open
        var btnaction = $(this).data("action"),
            modalid = $(this).data("modalid");

        $(modalid + ' .item-guid').val(dataRow.ItemGUID);

        if (btnaction === "edit") {
            // set all the values
            var jobType = dataRow.JobType;
            switch (jobType) {
                case "Union":
                    $('#add_job_tabs li:eq(0) a').tab('show');  // 0-indexed
                    //$('#add_job_tabs li:eq(1)').addClass('hide');
                    $('#addNewJobModal ul#add_job_tabs').addClass('hide');
                    $('h4#addNewJobModalLabel').html("Edit Job - Union");
                    $(".autocomplete-search-union").val(dataRow.Name);
                    $('.job_union.txt_card_number').val(dataRow.UnionCardNumber);
                    $('.job_union.txt_local_union_number').val(dataRow.LocalOneUnionCardNumber);
                    $('.job_union.ddl_position').val(dataRow.Position);
                    $('.job_union.txt_hours').val(dataRow.Hours);
                    $('.job_union.txt_shows').val(dataRow.Shows);
                    break;
                case "Production":
                    $('#add_job_tabs li:eq(1) a').tab('show');
                    //$('#add_job_tabs li:eq(0)').addClass('hide');
                    $('#addNewJobModal ul#add_job_tabs').addClass('hide');
                    $('h4#addNewJobModalLabel').html("Edit Job - Production");
                    $(".autocomplete-search-production").val(dataRow.Name);
                    $('.job_prod.txt_card_number').val(dataRow.UnionCardNumber);
                    $('.job_prod.txt_local_union_number').val(dataRow.LocalOneUnionCardNumber);
                    $('.job_prod.ddl_position').val(dataRow.Position);
                    $('.job_prod.txt_hours').val(dataRow.Hours);
                    $('.job_prod.txt_shows').val(dataRow.Shows);
                    break;
            }
        }

        // show modal popup for edit and remove
        $(modalid).modal('show').on('hidden.bs.modal', function (e) {
            
            $(modalid + ' .item-guid').val("");
            ModalReset();
        });

        /* CustomTable Properties 
        Hours,        ItemCreatedBy,        ItemCreatedWhen,        ItemGUID,        ItemID
        ItemModifiedBy,        ItemModifiedWhen,        ItemOrder,        JobType,        "Union"
        LocalOneUnionCardNumber,        Name,        Position,        Shows,        UnionCardNumber,        UserName
        */
        /*switch (btnaction) {
            case "edit":
                // set id here
                break;
            case "remove":
                break;

        }*/
    });
}

function ModalReset() {
    $('#add_new_job_modal input.form-control').val("");
    $('#add_new_job_modal select.job_prod, #add_new_job_modal select.job_union').val("-1");
    // add hide class to all error labels
    $('#add_new_job_modal span.error-label').addClass('hide');
    $('#addNewJobModal ul#add_job_tabs').removeClass('hide');
    $('h4#addNewJobModalLabel').html("Add Job");
}

$(document).ready(function () {
    // calling init
    //LocalOne.Common.Init();
    // .form-validation class exist and someone click on it
    LocalOne.Common.FormValidation();

    // bind click events for modals
    // Jagdish: this one could be used for every where else to dynamically populate modals
    LocalOne.Common.InitModalPopup();
    LocalOne.StewardReport.Init();

    /* ######### Steward Venues code ##########*/
    if ($('.all-venues').length > 0) {
        StewardRegistration();
    }
    /*#### EndOf: StewardRegistration process*/
    /*### Start of Steward Reports */
    // Initializing jobs for Union
    if ($('.all-jobs-union').length > 0) {
        var jsonJobsUnion = JSON.parse("{" + $('.all-jobs-union').val() + "}");
        //LocalOne.StewardRegistration.InitJobs(JSONjobs);

        StewardReports('.all-jobs-union', '#data-table-union', jsonJobsUnion);
    }
    // Initializing jobs for Production
    if ($('.all-jobs-production').length > 0) {
        var jsonJobsProduction = JSON.parse("{" + $('.all-jobs-production').val() + "}");
        //LocalOne.StewardRegistration.InitJobs(JSONjobs);

        StewardReports('.all-jobs-production', '#data-table-production', jsonJobsProduction);
    }
    /*### EndOf: Steward Report*/
});

Sys.WebForms.PageRequestManager.getInstance().add_endRequest(registerJobSkillModalEvent);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(registerLicCertModalEvent);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(registerAddJobModalEvent);

function registerJobSkillModalEvent(sender, args) {

    $('.add-skills-btn').click(function () {
        $('#jobSkillsModal').modal('show');

    });

}

function registerLicCertModalEvent(sender, args) {

    $('.add-licenses-btn').click(function () {
        $('#licensesModal').modal('show');
    });
}

function registerAddJobModalEvent(sender, args) {

    $('.add-job-btn').click(function () {
        $('#jobExpModal').modal('show');
    });
}


function openJobsInitial() {
    $('#jobExpModal').modal();
}
function validateEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function clearValidation() {
    $('label, input').removeClass("error");
    $('input').removeClass("validated");
    $('.field-error').hide();
}

// Member Registration
//   1st screen validation
$('.register-btn').on("click", function (e) {
    clearValidation();
    var error = false;
    var email1 = $('.ctbEmail').val();
    var email2 = $('.ctbEmail2').val();

    // check if user exists
    $.ajax({
        type: "POST",
        url: "MemberRegistration.ashx",
        data: { email: $('.ctbEmail').val() },
        dataType: "json",
        async: false,
        success: function (data) {
            if (data) {
                error = true;
                showValidation($('.ctbEmail'), "This user already exists.");
            }
        },
        failure: function (data) {
            // o well
        }
    });

    // check if a valid email
    if (!validateEmail(email1)) {
        error = true;
        showValidation($('.ctbEmail'), "Please enter a valid email address.");
    }
    else {
        $('.ctbEmail').addClass("validated");
    }
    if (validateEmail(email1) && (email1 == email2)) {
        $('.ctbEmail').addClass("validated");
        $('.ctbEmail2').addClass("validated");
    }
    else {
        error = true;
        showValidation($('.ctbEmail2'), "The email addresses must match.");
    }
    // make sure there is a card number
    if ($('.ctbCardNumber').val() == "") {
        error = true;
        showValidation($('.ctbCardNumber'), "Union Card Number is required.");
    }
    else {
        $('.ctbCardNumber').addClass("validated");
    }

    if (error)
        return false;
});

function showValidation(selector, errorText) {
    $(selector).parent().find('label').addClass("error");
    $(selector).addClass("error");
    $(selector).parent().find('.field-error').html("<p>" + errorText + "</p>").show();
}

//Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(function () {

//});

Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {

    $('.field-error').hide();
    $(".start-datepicker").datepicker();
    $('.end-datepicker').datepicker();

    // 2nd screen validation
    $('.create-password-btn').on("click", function (e) {
        clearValidation();
        var errorLabel = $('.ctbPassword').parent().find('.field-error');
        var errorLabel2 = $('.ctbPassword2').parent().find('.field-error');
        var error = false;

        if ($('.ctbPassword').val() == "") {
            error = true;
            showValidation($('.ctbPassword'), "Password is required.");
        }
        else {
            $('.ctbPassword').addClass("validated");
        }

        if ($('.ctbPassword').val() != $('.ctbPassword2').val()) {
            error = true;
            showValidation($('.ctbPassword2'), "Password fields must match.");
        }
        else {
            $('.ctbPassword2').addClass("validated");
        }

        if (error)
            return false;
    });

    // 3rd screen validation
    $('.save-profile-btn').on("click", function () {
        clearValidation();
        var error = false;

        if ($('.ctbFirstName').val() == "") {
            error = true;
            $('.ctbFirstName').parent().parent().find('label').first().addClass("error");
            showValidation($('.ctbFirstName'), "First name is required.");
        }
        if ($('.ctbLastName').val() == "") {
            error = true;
            $('.ctbLastName').parent().parent().find('label').first().addClass("error");
            showValidation($('.ctbLastName'), "Last name is required.");
        }
        if ($('.ctbAddress').val() == "") {
            error = true;
            showValidation($('.ctbAddress'), "Address is required.");
        }
        if ($('.ctbCity').val() == "") {
            error = true;
            showValidation($('.ctbCity'), "City is required.");
        }
        if ($('.ctbState').val() == "") {
            error = true;
            showValidation($('.ctbState'), "State is required.");
        }
        if ($('.ctbZipCode').val() == "") {
            error = true;
            showValidation($('.ctbZipCode'), "Zip Code is required.");
        }
        if ($('.ctbCellNumber').val() == "") {
            error = true;
            showValidation($('.ctbCellNumber'), "Cell number is required.");
        }

        if (error)
            return false;
    });

    $('.add-job-btn').click(function () {
        $('#jobExpModal').modal('show');
    });
    $('.edit-info-btn').click(function () {
        $('#editInfoModal').modal('show');
    });

    // Job Experience Modal validation
    $('.save-job-btn').click(function (e) {
        $('.modal input, label').removeClass("error");
        $('.modal .field-error').hide();
        var error = false;
        $('.job-modal-error').text("");

        var rblVenueType = $('.rbl-venue-types').find('input:checked').val();
        if (rblVenueType === undefined) {
            error = true;
            $('.rbl-venue-types').parent().find('label').first().addClass("error");
            //$('.rbl-venue-types').addClass("error");
            $('.rbl-venue-types').parent().find('.field-error').html("<p>Venue type is required.</p>").show();
        }
        if ($('.position-dropdown').val() == "") {
            error = true;
            $('.position-dropdown').parent().parent().parent().find('label').addClass("error");
            //$('.position-dropdown').addClass("error");
            $('.position-dropdown').parent().find('.field-error').html("<p>Job position is required.</p>").show();
        }
        if ($('.venue-dropdown').val() == "") {
            error = true;
            $('.venue-dropdown').parent().parent().parent().find('label').addClass("error");
            //$('.venue-dropdown').addClass("error");
            $('.venue-dropdown').parent().find('.field-error').html("<p>Venue is required.</p>").show();
        }
        if ($('.start-datepicker').val() == "") {
            error = true;
            $('.start-datepicker').parent().find('label').addClass("error");
            //$('.start-datepicker').addClass("error");
            $('.start-datepicker').parent().find('.field-error').html("<p>Start date is required.</p>").show();
        }

        if (error) {
            return false;
        }
        else {
            $('#jobExpModal').modal('hide');
        }

    });
    $('.save-license-btn').click(function () {
        $('#licensesModal').modal('hide');
    });
    $('.save-skill-btn').click(function () {
        $('#jobSkillsModal').modal('hide');
    });

    // Information modal validation
    $('.save-info-btn').click(function (e) {
        var error = false;

        if ($('.first-name-modal').val() == "") {
            error = true;
            showValidation($('.first-name-modal'), "First name is required.");
        }
        if ($('.last-name-modal').val() == "") {
            error = true;
            showValidation($('.last-name-modal'), "Last name is required.");
        }
        if ($('.card-number-modal').val() == "") {
            error = true;
            showValidation($('.card-number-modal'), "Card number is required.");
        }
        if ($('.address-modal').val() == "") {
            error = true;
            showValidation($('.address-modal'), "Address is required.");
        }
        if ($('.city-modal').val() == "") {
            error = true;
            showValidation($('.city-modal'), "City is required.");
        }
        if ($('.state-modal').val() == "") {
            error = true;
            showValidation($('.state-modal'), "State is required.");
        }
        if ($('.zip-modal').val() == "") {
            error = true;
            showValidation($('.zip-modal'), "Zip Code is required.");
        }
        if ($('.cell-modal').val() == "") {
            error = true;
            showValidation($('.cell-modal'), "Cell Number is required.");
        }

        if (error) {
            return false;
        }
        else {
            $('#editInfoModal').modal('hide');
        }
    });
});

/*Slick Slider*/
$(document).ready(function () {
    $('#hero .carousel-inner').slick({
        slidesToShow: 1,
        slidesToScro11: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
    });
    $('.home-announce .carousel-inner').slick({
        slidesToShow: 1,
        slidesToScro11: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
    });
    $('.slider-common').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        centerMode: true,
        fade: true,
        asNavFor: '.slider-common-nav'
    });
    $('.slider-common-nav').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        asNavFor: '.slider-common',
        dots: false,
        //centerMode: true,
        focusOnSelect: true
    });
});

$(document).ready(function() {
    var away = false;
    $('#btntop').click(function() {
        $("html, body").animate({scrollTop: 0}, 500);
    });
  //Stacktable for 768px and below for Department Heads
  if($(window).width() < 768 ) {
    $("table.member").cardtable();
  }
  $("a[type=submit]").click(function(){ $(".navbar-form.navbar-right.membersearch").toggleClass("open"); });
}); 

  //Alphabetical Department Heads
var $tbody = $('.departmentHeads tbody');
  $tbody.find('tr').sort(function (a, b) {
      var tda = $(a).find('td:eq(0)').text();
      var tdb = $(b).find('td:eq(0)').text();
      return tda > tdb ? 1
       : tda < tdb ? -1   
       : 0;
  }).appendTo($tbody);


 //Alphabetical Members

$(function () {

    $('.member-row .col-25 p .member-expand').html(function (i, html) {
        return html.replace(/(\w+)$/, '<span class="sort-by">$1</span>');
    });

   
});
 $('.member .member-row .col-25 p .member-expand .sort-by').sort(function(a, b){
     return $('a', a).text() > $('a', b).text()
}).appendTo('.member');


//Steward Report Job Modal Removing Certain Positions
$( document ).ready(function() {
  var selectOption = document.querySelectorAll('main.stewards .modal-body .venue-list-wrapper-modal select.job_union option');
  for (var i = 14; i < selectOption.length; i++) {
    selectOption[i].remove();
  }
  selectOption = document.querySelectorAll('main.stewards .modal-body .venue-list-wrapper-modal select.job_prod option');
  for (var i = 0; i < 14; i++) {
    selectOption[i].remove();
  }
});
