﻿/* Less.js compiler for Windows Script Host
 * http://blog.dotsmart.net/
 * Modified by Van Kichline (vank), Microsoft. 10/2012:
 * Emit errors in imported files.
 * Copyright (c) 2010, Duncan Smart
 * Licensed under the Apache 2.0 License.
 */

// Stub out globals
var window = this;
var location = window.location = {
    port: 0,
    href: '',
    protocol: 'http:',      // vank: added to support development env setting
    hostname: 'localhost'   // vank: added to support development env setting
};

var input = null;

// XMLHttpRequest that just gets local files. Used when processing "@import"
function XMLHttpRequest() { }
XMLHttpRequest.prototype = {
    open: function (method, url, async) {
        this.url = url;
    },
    send: function () {
        try {
            this.status = 200;
            var response = StylesheetLoader.getStylesheetCode(this.url);

            if (response == null) {
                this.status = 404;
            }
            else {
                if (response.length === 0) {
                    response = ' ';
                }
                else {
                    this.responseText = response;
                }
            }
        }
        catch (e) {
            this.status = 404;
            this.responseText = e.description;
        }
    },
    setRequestHeader: function () { },
    getResponseHeader: function () { return ''; }
};

// Fake document
var document = {
    _dummyElement: {
        childNodes: [],
        appendChild: function () { },
        style: {}
    },
    getElementsByTagName: function () { return []; },
    getElementById: function () { return this._dummyElement; },
    createElement: function () { return this._dummyElement; },
    createTextNode: function () { return this._dummyElement; },
    // vank: added 'body' to capture errors in @imported files
    body: {
        replaceChild: function (elem) {
            if (!elem.innerHTML.match(/null or not an object/i)) {
                document._dummyElement.error = elem.innerHTML;
            }
        }
    }
};

// vank: added 'setInterval' and 'clearInterval' to support development env setting
function setInterval(f, int) {
    f();
    return 'x';
}
function clearInterval(t) { }


// Returns parsed CSS code as string, error object or error message in special format string (starting with 'err|')
// string lessCode: Less input text
function GetCss(lessCode) {
    var lessParser = new less.Parser();
    var returnObject;

    // Don't parse empty strings
    if ((typeof lessCode == 'undefined') || (lessCode == null) || (lessCode.length == 0))
    {
        returnObject = '';
    }
    else {
        try {
            lessParser.parse(lessCode, function (error, root) {
                if (error) {
                    returnObject = error;
                }
                else {
                    returnObject = root.toCSS();
                }
            });
        }
        catch (ex) {
            returnObject = 'err|' + ex.message;
        }
    }

    return returnObject;
}

