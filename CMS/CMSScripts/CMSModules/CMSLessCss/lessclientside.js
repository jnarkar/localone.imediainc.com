﻿if (typeof xhrModified == 'undefined') {
    var originalFunction = XMLHttpRequest.prototype.open;

    function modifiedOpen(method, url, async) {
        var res = url.match(/(?:.*\/)(\w+)\.less$/i);
        if (res) {
            url = urlBase + res[1];
        }

        originalFunction.call(this, method, url, async);
    }

    XMLHttpRequest.prototype.open = modifiedOpen;
    xhrModified = true;
}


// Returns parsed CSS code as string, error object or error message in special format string (starting with 'err|')
// string lessCode: Less input text
function GetCss(lessCode) {
    var lessParser = new less.Parser();
    var returnObject;

    // Don't parse empty strings
    if (!lessCode || (lessCode.length == 0)) {
        return '';
    }

    try {
        lessParser.parse(lessCode, function (error, root) {
            if (error) {
                // If imported stylesheet file was not found then ensure that file name doesn't contain whole path
                if (error.type.toLowerCase() == 'file') {
                    var re = /'.*\/(.+\.less)'(.+)$/;
                    if (error.message.match(re)) {
                        error.message = error.message.replace(re, "'$1'" + "$2");
                    }
                }

                var errorDescription = ['err', error.line, error.message, error.type, error.index, error.column];
                returnObject = errorDescription.join('|');
            }
            else {
                returnObject = root.toCSS();
            }
        });
    }
    catch (ex) {
        if (ex.message && ex.type && ex.column && ex.line && ex.index) {
            returnObject = ['err', ex.line, ex.message, ex.type, ex.index, ex.column].join('|');
        }
        else {
            returnObject = 'err|' + ex.message;
        }
    }

    return returnObject;
}
