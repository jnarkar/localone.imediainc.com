﻿<%@ WebHandler Language="C#" Class="MemberRegistration" %>

using System;
using System.Web;
using CMS.Membership;
using System.Web.Script.Serialization;
using CMS.SiteProvider;

public class MemberRegistration : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        //context.Response.Write("Hello World");

        string email = context.Request.Form["email"];
        bool exists = DoesUserExist(email);
        //context.Response.ContentType = "application/json";
        string json = new JavaScriptSerializer().Serialize(exists);
        context.Response.Write(json);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

    public bool DoesUserExist(string email)
    {
        UserInfo ui = UserInfoProvider.GetUserInfo(email);
        SiteInfo si = SiteContext.CurrentSite;
        UserInfo siteui = UserInfoProvider.GetUserInfo(UserInfoProvider.EnsureSitePrefixUserName(email, si));
        if (ui != null || siteui != null)
            return true;
        else
            return false;
    }

}